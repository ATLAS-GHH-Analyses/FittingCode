# FittingCode

# setup HistFitter

twiki: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HistFitterTutorial#Part_0_Setting_up_HistFitter
* git  clone ssh://git@gitlab.cern.ch:7999/HistFitter/HistFitter.git
* cd HistFitter && git checkout v0.65.0 -b v0.65.0-user
* In python/configManager.py, change "self.forceNorm = True" to "self.forceNorm = False", in order to use the normalisation histograms stored in CacheFile
* source userSetup.sh
* cd src && make

# Fitting code

* the configuration file with systematics settings is under systematics folder

* Input histograms with systematics is in
```
 /eos/user/y/yuxu/Histograms/r04-02/GHH_SS2l_r04-02_sys_20220214.root (no WWW SF applied, sherpa2.2.2 ssWW, signal PS, decorrelate, all signals)
```

* For drawing other variables (jet pt, lep pt, mll, MET, fatjet pt), one can run HiistFitter with GHH_2WZNF.py with "SRvars", "VRvars" or "CRvars" in mode. Other variables for all background are stored and also for four benchmark signal: GHH300fW0fWW4600, GHH600fW1200fWW2400, GHH600fW1350fWW0, GHH900fW1350fWW0. Note that HistFitter will take more than 6 hours to plot (-D option) with so many variables. To speed up, one can create workspace wiht "-w -f- F" firstly, then use "-F excl -D "before,after,corrMatrix" ". Jet1Pt can not be drawn properly with systematics, may be due to some bins having negative yields(need to check). The input is:
```
 /eos/user/y/yuxu/Histograms/r04-02/GHH_SS2l_r04-02_sys_20220416.root
```
To disable signal in all regions, use "disableSignal = True".
# run the Fitting code

* modify sys.path.insert in GHH.py with your path of systematic_objects.py

* put input histograms into data folder and change the input root file name to backupCacheFile_GHH_SS2l_r04-02_sys_scaleWWWandRenameInclusiveFF.root

* Signal used in the fit is defined here:
```
suffix = 'GHH6f650f0'
```

* Exclusion fit -- both CRs and SRs are used in the fit:

use:
```
mode = 'excl_Inclusive_sys'
```
in GHH.py

run the code, GHH6f650f0 represents the signal sample name:

```
HistFitter.py -w -f -F excl -D "before,after,corrMatrix" -c "mode='excl_Inclusive_sys'; suffix='GHH6f650f0';"  path-to-code/GHH.py
```

* Exclusion fit with asimov data:

```
HistFitter.py -w -f -a -F excl -c "mode='exclAsimov_Inclusive_sys'; suffix='GHH6f650f0';" path-to-code/GHH.py
```

* Bkg-only fit -- only the CRs are used to constrain the fit parameters:

```
HistFitter.py -w -f -F bkg -D "before,after,corrMatrix" -c "mode='bkg_Inclusive_sys'; suffix='GHH6f650f0';" path-to-code/GHH.py
```

* Bkg-only fit with asimov data:

```
HistFitter.py -w -f -a -F bkg -D "corrMatrix" -c "mode='bkgAsimov_Inclusive_sys'; suffix='GHH6f650f0';" path-to-code/GHH.py
```

* Ranking with real data in CRs and asimov data(= bkg+sig) in SRs:

```
HistFitter.py -w -f -F excl -c "mode='Ranking_Inclusive_sys'; suffix='GHH6f650f0';"  path-to-code/GHH.py

mkdir  plotsMP_ranking

SystRankingPlotMP.py  -w results/GHH_Ranking_Inclusive_sys_GHH6f650f0/Ranking_combined_NormalMeasurement_model_afterFit.root 
-f SRSS2lBoostedInc_Meff,SRSS2lResolvedInc_Meff,ssWWResolvedInc_Meff,CR3lBoostedInc_Meff,CR3lResolvedInc_Meff
-p "mu_SIG"
--lumi 139
--maxCores -1
--sysOnly -o "plotsMP_ranking/"
```

* Ranking with asimov data in CRs and SRs:

```
HistFitter.py -w -f -a -F excl -c "mode='exclAsimov_Inclusive_sys'; suffix='GHH6f650f0';"  path-to-code/GHH.py

mkdir  plotsMP_exclAsimov

SystRankingPlotMP.py  -w results/GHH_exclAsimov_Inclusive_sys_GHH6f650f0/Exclusion_combined_NormalMeasurement_model_afterFit_asimovData.root 
-f SRSS2lBoostedInc_Meff,SRSS2lResolvedInc_Meff,ssWWResolvedInc_Meff,CR3lBoostedInc_Meff,CR3lResolvedInc_Meff
-p "mu_SIG"
--lumi 139
--maxCores -1
--dataSet "asimovData"
--sysOnly -o "plotsMP_exclAsimov/"
```

* get upperlimit:

```
HistFitter.py -w -f -l -F excl -c "mode='excl_Inclusive_sys'; suffix='GHH6f650f0';" path-to-code/GHH.py
```
Input histograms with systematics is in

```
/afs/cern.ch/work/y/yuxu/public/SysForGHH/
```

# Submitting over all signal samples

To calculate the upper limit for all signal samples and create exclusion plots, the HTCondor script is used (see below).

### run the HTCondor Script
* move condor_submitHF.sh, condorTEMPLATE.sub, samplelist_4thSig.txt, and run_HF.sh to your HistFitter directory
* replace paths in each file as necessary
* create a "batch" directory in your HistFitter directory
* modify run_HF.sh to run the appropriate HistFitter analysis
* set up the HistFitter environment as you would normally (this environment will be captured by the getenv = True statement in the condorTEMPLATE.sub file)
* submit the job with the following command
```
bash /HF_PATH/condor_submitHF.sh
```

### Upper Limits and Mass Exclusion

Uncomment the following line of code in run_HF.sh: 
```
HistFitter.py -w -f -l -F excl -c "mode='excl_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
```
The output log files of these runs will be in the batch directory. The scripts make_limits.sh and read_limits.py can read the upper limit results from the logfiles into a table of results (note this will use samplelist_4thSig.txt to check if any signals are missing). Move these to your HistFitter directory or modify the paths in each appropriately and run:
```
source make_limits.sh
python read_limits.py
```
The resulting signal_limits.pkl file can be used to create the mass exclusion plots by running:
```
lsetup "views LCG_98python3_ATLAS_1 x86_64-centos7-gcc8-opt"

python plotMassExclusion.py
```
Again you may need to move this from the plotting directory to your HistFitter directory, or modify the paths inside it.

### Contour Plots (fW,fWW)

Uncomment the following line of code in run_HF.sh:
```
HistFitter.py -p -c "mode='excl_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
``` 
If the workspaces have not been previously created add the `-w` flag to this line of code.

The results are stored in the results directory (\*hypotest.root). The script plotting/contourplots.sh can be run to create the contour plots. This makes use of several other scripts within the plotting directory. It will also create a number of directorys to store output json files, debug plots, etc. Some comments are added in this script to explain each step. 
Modify the paths within this file or move the entire plotting/ directory into your HistFitter directory and run:
```
rm -f scripts/contourPlotter.py

cp plotting/contourPlotter.py scripts/

source plotting/contourplots.sh
```
Run tips: separate out contourplots.sh into two separate scripts. One that runs everything before the "lsetup "views LCG_98python3_ATLAS_1 x86_64-centos7-gcc8-opt" line and the second script runs only the lines after that.

The resulting contour plots will be in the contourplots directory.

# change name of HypoTestInverterResult

replace the src/ConfigMgr.cxx with ModifyHF/ConfigMgr.cxx and do make again so that the name of HypoTestInverterResult will be hypo_GHH9f0f-5000 instead of hypo_GHH9f0fm5000

# Reformatting of Pull Plots
Originally, the pull plot is stored in results/output-path/fit_parameters.root, but it's hard to read. Need  to reformat the plots.

Replace src/Utils.cxx with ModifyHF/Utils.cxx and do make again. Then when HistFitter.py is run, this additionally saves testSavePrims.root and corrMatrix.root to the results.

The macro replotpulls.py can be used to reformat the pull plots. Modify line 17 to point to the results directory. It prints a reformatted plot containg all n.p. to the results directory. Several arguments exist to modify behaviour (more details in the script itself):

* `python replotpulls.py` simply reformats the plot. Extending it to fit all n.p. and fixing the labels.
* `python replotpulls.py --multiple --n_plots N` to split the pull plot into N plots. If `--n_plots` isn't defined it defaults to 4.
* `python replotpulls.py --reduce` to reduce the size of the pull plot. This will plot only the top 20 largest nuisance parameter pulls.
* To remove the gamma_* parameters use the `--no_gammas` flag. This works with standard, multiple or reduce plotting.
* Use the `--no_blue` flag to plot Lumi and gamma parameters in black
* Use `--extentsion png` to plot as png, or any other type of image. Default is pdf.
* Use `--group <str>` to plot select groups of n.p. which contain the substring <str>. For example `--group FT_EFF` plots the b-tag systematics. Any regex expression should work, e.g. `--group MUON\|EL` will plot muon and electron systematics.

For the grouped n.p. pull plots, the script grouped_pulls.sh in plotting has been created. This can be used or modified to create different groupings.

N.B. if used on lxplus you may need to lsetup root (e.g. root 6.20) as the default root/python 2 can have bugs.

The macro replotcorr.py can be used to reformat the correlation matrix plot. Again the option --reduce can be used to reduce the size of the matrix. This works by removing any nusiance parameters which dont have at least one correlation grater than a threshold value. The threshold value can be modified using --threshold.

# Comparing Pulls
The macro `comparepulls.py` can plot the systematic pulls from two different fits on the same axes. This is currently only implemented for the grouped pulls option. To do this we need the testSavePrims.root from each run. An example script is available in `plotting/compare_grouped_pulls.sh` with the various commands needed to compare different groups of pulls.

Various commands are available (in addition to the commands available for replotpulls.py):
* `--dirr1`, `--dirr2`, `--leg1` and `--leg2` are used to point to the results directory for fits 1 and 2, and create the label in the legend.
* `--scaleX` and `--scaleY` can be used to extend the x and y axis if the plots contain too many n.p. to fit
* `--lower_limit` will increase the lower limit on the x-axis. Use this in the event that the printed values are pushed off the edge of the plot (this occasionally happens due to ROOT bug)

# Reformatting pre- and post-fit plots
```
git clone ssh://git@gitlab.cern.ch:7999/atlas-publications-committee/atlasrootstyle.git
```
```
mv atlasrootstyle macros
```

Use the plotGHH.py (in plotting/). This reformats the outputted pre- and post-fit region plots (after running `HistFitter.py -f -F bkg -D "before,after,corrMatrix" ...`).

The script doPlotGHH.sh shows the necessary commands to reformat the VR, CR and SR plots for a bkg-only and excl fit.

# Contour Plots
See the scrips (in plotting/): contourplots.sh and ploMassExclusion.py. Python3 is needed for both scripts, simply run:
```
lsetup "views LCG_98python3_ATLAS_1 x86_64-centos7-gcc8-opt"
```

# get yields table

yield table for validation region with bkg-only fit
:
```
YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPromptEl,NonPromptMu,ChargeFlip,PhotonConversion -c VRSS2lBoostedInc_Meff,VRSS2lResolvedInc_Meff -w results/<output-path>/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_VRInc_sys.tex -b
```
yield table for control regions with exclusion fit

```
YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPromptEl,NonPromptMu,ChargeFlip,PhotonConversion -c CR3lBoostedInc_Meff,CR3lResolvedInc_Meff -w results/<output-path>/Exclusion_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_CR3lIncExcl_sys.tex -b
```

```
YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPromptEl,NonPromptMu,ChargeFlip,PhotonConversion -c ssWWResolvedInc_Meff -w results/<output-path>/Exclusion_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_ssWWIncExcl_sys.tex -b
```

yield table for signal regions with exclusion fit

```
YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPromptEl,NonPromptMu,ChargeFlip,PhotonConversion -c SRSS2lBoostedInc_Meff,SRSS2lResolvedInc_Meff -w results/<output-path>/Exclusion_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_SR_Excl.tex -b
```

# get systematic table

(https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/HistFitterTutorial#Systematics_table)

Replace HistFitter/scripts/SysTable.py with FittingCode/ModifyHF/SysTable.py, you can group/modify the NPs start from line 539.

Get signal regions table:
```
SysTable.py -w results/GHH_excl_Inclusive_GHH600fW1200fWW2400/Exclusion_combined_NormalMeasurement_model_afterFit.root -c SRSS2lResolvedInc,SRSS2lBoostedInc -%
```

Get control regions table:

```
SysTable.py -w results/GHH_excl_Inclusive_GHH600fW1200fWW2400/Exclusion_combined_NormalMeasurement_model_afterFit.root -c CR3lResolvedInc,CR3lBoostedInc, ssWWResolvedInc -%
```
