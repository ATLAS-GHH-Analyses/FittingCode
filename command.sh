#HistFitter.py -w -f -F excl -D "before,after,corrMatrix" -i MineTest/GHH_ShapeFitTest.py
#HistFitter.py -w -f -D "before,after" -i MineTest/GHH.py #bkg only
#HistFitter.py -F excl -p -l MineTest/GHH_ShapeFitTest.py
#HistFitter.py -F excl -l MineTest/GHH_ShapeFitTest.py # return upperlimit

#get  yield table

#YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPrompt,ChargeFlip,Vgamma -c SRSS2lBoostedee_Meff,SRSS2lBoostedemu_Meff,SRSS2lBoostedmue_Meff,SRSS2lBoostedmumu_Meff -w results/GHH_excl_NLO_DiLepton_GHH6f650f0/Exclusion_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_SRBoosted_ll.tex

#YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPrompt,ChargeFlip,Vgamma -c SRSS2lResolvedee_Meff,SRSS2lResolvedemu_Meff,SRSS2lResolvedmue_Meff,SRSS2lResolvedmumu_Meff -w results/GHH_excl_NLO_DiLepton_GHH6f650f0/Exclusion_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_SRResolved_ll.tex

#YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPrompt,ChargeFlip,Vgamma -c VRSS2lBoostedee_Meff,VRSS2lBoostedemu_Meff,VRSS2lBoostedmue_Meff,VRSS2lBooostedmumu_Meff  -w results/GHH_bkg_NLO_DiLepton_GHH6f650f0/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_VRBoosted_ll_sig.tex

#YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPrompt,ChargeFlip,Vgamma -c VRSS2lResolvedee_Meff,VRSS2lResolvedemu_Meff,VRSS2lResolvedmue_Meff,VRSS2lResolvedmumu_Meff -w results/GHH_bkg_NLO_DiLepton_GHH6f650f0/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_VRResolved_ll_sig.tex

#YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPrompt,ChargeFlip,Vgamma -c VRSS2lBoostedee_Meff,VRSS2lBoostedemu_Meff,VRSS2lBoostedmue_Meff,VRSS2lBoostedmumu_Meff -w results/GHH_bkg_NLO_DiLepton_AddFlatWtaggerSFMjj200bin4_GHH6f650f0/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_VRBoosted_ll.tex

#YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPrompt,ChargeFlip,Vgamma -c VRSS2lResolvedee_Meff,VRSS2lResolvedemu_Meff,VRSS2lResolvedmue_Meff,VRSS2lResolvedmumu_Meff -w results/GHH_bkg_NLO_DiLepton_AddFlatWtaggerSFMjj200bin4_GHH6f650f0/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_VRResolved_ll.tex

YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPrompt,ChargeFlip,Vgamma -c SRSS2lBoostedInc_Meff,SRSS2lResolvedInc_Meff -w results/GHH_excl_NLO_Inclusive_GHH6f650f0/Exclusion_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_SRInc.tex

YieldsTable.py -s GHH6f650f0,TopX,WWW,ssWW,VVV,ZZ,WZ,NonPrompt,ChargeFlip,Vgamma -c VRSS2lBoostedInc_Meff,VRSS2lResolvedInc_Meff -w results/GHH_bkg_NLO_Inclusive_GHH6f650f0/Template_BkgOnly_combined_NormalMeasurement_model_afterFit.root -o YieldsTable_VRInc.tex

