#!/bin/bash
# set -x

function submit() #parameters = condor_tag
{
        condor_submit $1
}


condor_tag="HF_unblinded_UL_v1_"

my_dir=$PWD
HF_PATH=/afs/cern.ch/work/g/grbarbou/HeavyHiggs/HFUpdated

channel=$1
type=$2
channel_label=""
targets=(targettest)

#for signal in 'GHH600fW1350fWW0'
#for signal in 'GHH300fW150fWW2250' 'GHH300fWm140fWWm2100' 'GHH600fWm500fWW1000' 'GHH600fW500fWW1000'
for signal in $(cat samplelist_4thSig.txt)
do
for target in "${targets[@]}"
do
	name=${channel}_${type}_${target}_${signal}
	echo $name

	cd ${HF_PATH}/batch
	mkdir $name
	cd $name

	jobName=${condor_tag}${name}.sub
	echo $jobName

	scriptName=${condor_tag}${name}.sh
	echo $scriptName

	condorTemplate=${HF_PATH}/condorTEMPLATE.sub

	cp $condorTemplate $jobName

	scriptString=${HF_PATH}/batch/${name}/${scriptName}
	sed -i "s!@SCRIPT@!${scriptString}!g" ${jobName}

	condor_err=${HF_PATH}/batch/${name}/${condor_tag}${name}.err
	sed -i "s!@STDOUTERR@!${condor_err}!g" ${jobName}

	condor_out=${HF_PATH}/batch/${name}/${condor_tag}${name}.out
	sed -i "s!@STDOUT@!${condor_out}!g" ${jobName}

	condorLog=${HF_PATH}/batch/${name}/${condor_tag}${name}.log
	sed -i "s!@LOG@!${condorLog}!g" ${jobName}

	condor_queue="tomorrow"
	sed -i "s!@QUEUE@!${condor_queue}!g" ${jobName}



	cat > $scriptName << EOF
#!/bin/bash
set -x
ps -p \$$
echo $PWD

cd ${HF_PATH}
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
lsetup cmake "root 6.20.06-x86_64-centos7-gcc8-opt" #"root 6.18.04-x86_64-centos7-gcc8-opt" #

export RUNDIR='${HF_PATH}'
cd \$RUNDIR

source setup.sh

export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:${HF_PATH}}/lib"

source run_HF.sh $signal $channel_label $type $target

EOF

	chmod 755 $scriptName

	submit $jobName

done
done

cd $my_dir
