from configManager import configMgr
from configWriter import Sample
from logger import INFO, Logger
from systematic import Systematic

#---------------------------------------
# Analysis setup
#---------------------------------------
if 'mode' not in dir():
    mode = 'DiLepton_sys_excl'

if 'suffix' not in dir():
    suffix = 'default'

if 'variable' not in dir():
    variable = 'htmet'
    variable2 = 'mtlep'

if "TriLepton" in mode:
    analysis_name = 'SeeSaw_' + str(mode) + '_' + str(suffix) + '_' + str(variable2)
else:
    analysis_name = 'SeeSaw_' + str(mode) + '_' + str(suffix) + '_' + str(variable)
print analysis_name

sysType = 'overallNormHisto'
toys = 'toys' in mode or 'Toys' in mode
bkgOnly = 'bkg' in mode or 'BKG' in mode
asimov = 'asimov' in mode or 'Asimov' in mode
exclMode = 'excl' in mode or 'EXCL' in mode
limitMode = 'lim' in mode or 'LIM' in mode
rankingMode = "Ranking" in mode 
disableSignal = bkgOnly

other_bkg_tmp = 'other_'
if "DiLepton" in mode:
    other_bkg = other_bkg_tmp + '2l'
elif "TriLepton" in mode or "ZRegion" in mode or "JetVeto" in mode or "ZVeto" in mode:
    other_bkg = other_bkg_tmp + '3l'
elif "FourLepton" in mode:
    other_bkg = other_bkg_tmp + '4l'
#---------------------------------------
# Logger
#---------------------------------------

log = Logger(analysis_name)
log.setLevel(INFO)  # should have no effect if -L is used

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
useStat = True  # statistics variation of samples

#-------------------------------
# Config manager basics
#-------------------------------

# Setting the parameters of the hypothesis test
configMgr.doExclusion = True  # True=exclusion, False=discovery
if toys:
    configMgr.nTOYs=5000
    configMgr.calculatorType = 0
else:
    configMgr.calculatorType = 2
configMgr.testStatType = 3
configMgr.nPoints = 20
# configMgr.scanRange = (0, 1)

configMgr.writeXML = True

configMgr.analysisName = analysis_name
configMgr.histCacheFile = 'data/' + configMgr.analysisName + '.root'
configMgr.outputFileName = 'results/' + configMgr.analysisName + '_Output.root'

# activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = False  # enable the fallback to trees
# enable the use of an alternate data file
configMgr.useHistBackupCacheFile = True
# histogram templates - the data file of your previous fit, backup cache
configMgr.histBackupCacheFile = 'data/backupCacheFile_' + analysis_name + '.root'

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 138.96516  # Luminosity of input TTree after weighting
configMgr.outputLumi = 138.96516  # Luminosity required for output histograms
configMgr.setLumiUnits('fb-1')

# blind
configMgr.blindSR = True
if asimov:
    print('Using Asimov Dataset!')
    configMgr.useAsimovSet=True

if rankingMode:
    print('INJECTING SIGNAL!')
    configMgr.useSignalInBlindedData = True


#-------------------------------------
# Now we start to build the data model
#-------------------------------------

# Systematics
if 'sys' in mode:
    sysNamesCommon = [
        'FT_EFF_B_systematics',
        'FT_EFF_C_systematics',
        'FT_EFF_extrapolation_from_charm',
        'FT_EFF_extrapolation',
        'FT_EFF_Light_systematics',
        'JET_BJES_Response',
        'JET_EffectiveNP_1',
        'JET_EffectiveNP_2',
        'JET_EffectiveNP_3',
        'JET_EffectiveNP_4',
        'JET_EffectiveNP_5',
        'JET_EffectiveNP_6',
        'JET_EffectiveNP_7',
        'JET_EffectiveNP_8restTerm',
        'JET_EtaIntercalibration_Modelling',
        'JET_EtaIntercalibration_NonClosure_highE',
        'JET_EtaIntercalibration_NonClosure_negEta',
        'JET_EtaIntercalibration_NonClosure_posEta',
        'JET_EtaIntercalibration_TotalStat',
        'JET_Flavor_Composition',
        'JET_Flavor_Response',
        'JET_JvtEfficiency',
        'JET_Pileup_OffsetMu',
        'JET_Pileup_OffsetNPV',
        'JET_Pileup_PtTerm',
        'JET_Pileup_RhoTopology',
        'JET_SingleParticle_HighPt',
        'MET_SoftTrk_Scale',
        'PRW_DATASF',
    ]
    sysNamesCommonFullSim = [
        'JET_PunchThrough_MC16',
    ]
    sysNamesCommonFastSim = [
        'JET_PunchThrough_AFII',
        'JET_RelativeNonClosure_AFII',
    ]

    sysNamesCommonOneSidedSym = [
        'JET_JER_EffectiveNP_1',
        'JET_JER_EffectiveNP_2',
        'JET_JER_EffectiveNP_3',
        'JET_JER_EffectiveNP_4',
        'JET_JER_EffectiveNP_5',
        'JET_JER_EffectiveNP_6',
        'JET_JER_EffectiveNP_7restTerm',
        'MET_SoftTrk_ResoPara',
        'MET_SoftTrk_ResoPerp',
    ]
    sysNamesCommonOneSidedFullSim = [
        'JET_JER_DataVsMC_MC16',
    ]
    sysNamesCommonOneSidedFastSim = [
        'JET_JER_DataVsMC_AFII',
    ]

    sysNamesElectron = [
        'EG_RESOLUTION_ALL',
        'EG_SCALE_ALL',
        'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_CHARGEID_SYStotal',
        'EL_CHARGEID_STAT',
    ]
    sysNamesElectronFastSim = [
        'EG_SCALE_AF2',
    ]
    sysNamesMuon = [
        'MUON_ID',
        'MUON_MS',
        'MUON_SAGITTA_RESBIAS',
        'MUON_SAGITTA_RHO',
        'MUON_SCALE',
        'MUON_EFF_RECO_STAT',
        'MUON_EFF_RECO_SYS',
        'MUON_EFF_RECO_STAT_LOWPT',
        'MUON_EFF_RECO_SYS_LOWPT',
        'MUON_EFF_ISO_STAT',
        'MUON_EFF_ISO_SYS',
        'MUON_EFF_TrigStatUncertainty',
        'MUON_EFF_TrigSystUncertainty',
        'MUON_EFF_TTVA_STAT',
        'MUON_EFF_TTVA_SYS',
        'MUON_EFF_BADMUON_SYS',
    ]

    sysNamesTTbarTheory = [
        'THEORY_PDF_CHOICE_ttbar',
        'THEORY_PDF_VARIATION_ttbar',
        'THEORY_SCALE_ttbar',
        'THEORY_ISR_ttbar',
    ]
    sysNamesTTbarTheoryOneSidedSym = [
        'THEORY_GENERATOR_ttbar',
        'THEORY_SHOWERING_ttbar',
        'THEORY_FSR_ttbar',
    ]
    sysNamesDibosonTheory = [
        'THEORY_PDF_CHOICE_diboson',
        'THEORY_PDF_VARIATION_diboson',
        'THEORY_SCALE_diboson',
    ]
else:
    sysNamesCommon = []
    sysNamesCommonFullSim = []
    sysNamesCommonFastSim = []
    sysNamesCommonOneSidedSym = []
    sysNamesCommonOneSidedFullSim = []
    sysNamesCommonOneSidedFastSim = []
    sysNamesElectron = []
    sysNamesElectronFastSim = []
    sysNamesMuon = []
    sysNamesTTbarTheory = []
    sysNamesTTbarTheoryOneSidedSym = []
    sysNamesDibosonTheory = []

# Dictionnary of cuts defining channels/regions (for Tree->hist)
if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
    configMgr.cutsDict["Top_OS_ee"] = "1."
    configMgr.cutsDict["Top_OS_em"] = "1."
    configMgr.cutsDict["Top_OS_mm"] = "1."

    configMgr.cutsDict["WCR_SS_ee"] = "1."
    configMgr.cutsDict["WCR_SS_em"] = "1."
    configMgr.cutsDict["WCR_SS_mm"] = "1."

    configMgr.cutsDict["SR_2l2j_OS_ee"] = "1."
    configMgr.cutsDict["SR_2l2j_OS_em"] = "1."
    configMgr.cutsDict["SR_2l2j_OS_mm"] = "1."
    configMgr.cutsDict["SR_2l2j_SS_ee"] = "1."
    configMgr.cutsDict["SR_2l2j_SS_em"] = "1."
    configMgr.cutsDict["SR_2l2j_SS_mm"] = "1."

if "JetVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    configMgr.cutsDict["CR_3l0j_AS1_lll"] = "1."
    configMgr.cutsDict["SR_3l0j_AS1_lll"] = "1."

if "ZRegion" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    configMgr.cutsDict["CR_3l2j_2VB_AS1_lll"] = "1."
    configMgr.cutsDict["SR_3l2j_2VB_AS1_lll"] = "1."

if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    configMgr.cutsDict["SR_3l2j_VB_AS1_lll"] = "1."

if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
    configMgr.cutsDict["LowMass_4l_HalfHalf_llll"] = "1."
    configMgr.cutsDict["LowTop_4l_HalfHalf_llll"] = "1."

    configMgr.cutsDict["SR_4l_HalfHalf_llll"] = "1."
    configMgr.cutsDict["SR_4l_OneDiff_llll"] = "1."

if bkgOnly:
    if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
        configMgr.cutsDict["WSidebands_OS_ee"] = "1."
        configMgr.cutsDict["WSidebands_OS_em"] = "1."
        configMgr.cutsDict["WSidebands_OS_mm"] = "1."
        configMgr.cutsDict["WSidebands_SS_ee"] = "1."
        configMgr.cutsDict["WSidebands_SS_em"] = "1."
        configMgr.cutsDict["WSidebands_SS_mm"] = "1."

    if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
        configMgr.cutsDict["CR_3l2j_VB_AS1_lll"] = "1."
        # configMgr.cutsDict["ZVRSmT_AS1_lll"] = "1."
        # configMgr.cutsDict["JLowVRLmT_AS1_lll"] = "1."
    # if "TriLepton" in mode or "AllChannel" in mode:
    #     configMgr.cutsDict["VR_3l2j_AS1_lll"] = "1."

    if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
        configMgr.cutsDict["LowMass_4l_OneDiff_OneDiff_llll"] = "1."

# Define weights - dummy here
configMgr.weights = '1.'

# nominal name of the histograms with systematic variation
configMgr.nomName = 'Nom_'

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------

from ROOT import kGreen, kYellow, kViolet, kBlack, kBlue, kRed, kCyan


if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
    ttbarSample = Sample('ttbar', kGreen - 6)
    ttbarSample.setNormByTheory()  # scales with lumi
    ttbarSample.setStatConfig(useStat)
    ttbarSample.setNormFactor('mu_ttbar', 1., 0., 5.)
    ttbarSample.setNormRegions([('Top_OS_ee', variable), ('Top_OS_mm', variable), ('Top_OS_em', variable)])
    for sysName in sysNamesCommon:
        ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonFullSim:
        ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonOneSidedSym:
        ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFullSim:
        ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    for sysName in sysNamesTTbarTheory:
        ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesTTbarTheoryOneSidedSym:
        ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
    raretopSample = Sample("raretop", kCyan + 3)
    raretopSample.setNormByTheory()  # scales with lumi
    raretopSample.setStatConfig(useStat)
    raretopSample.setNormFactor("mu_raretop", 1., 0., 5.)
    raretopSample.setNormRegions([("LowTop_4l_HalfHalf_llll", variable)])
    for sysName in sysNamesCommon:
        raretopSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonFullSim:
        raretopSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonOneSidedSym:
        raretopSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFullSim:
        raretopSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

if not "AllChannel" in mode and not "TriChannel" in mode:
    otherSample = Sample(other_bkg, kYellow - 9)
    otherSample.setNormByTheory()  # scales with lumi
    otherSample.setStatConfig(useStat)
    for sysName in sysNamesCommon:
        otherSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesCommonFullSim:
        otherSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesCommonOneSidedSym:
        otherSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFullSim:
        otherSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
if "AllChannel" in mode or "DiTriChannel" in mode:
    otherSample2l = Sample("other_2l", kYellow - 9)
    otherSample2l.setNormByTheory()  # scales with lumi
    otherSample2l.setStatConfig(useStat)
    for sysName in sysNamesCommon:
        otherSample2l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesCommonFullSim:
        otherSample2l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesCommonOneSidedSym:
        otherSample2l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFullSim:
        otherSample2l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
if "AllChannel" in mode or "TriChannel" in mode:
    otherSample3l = Sample("other_3l", kYellow - 9)
    otherSample3l.setNormByTheory()  # scales with lumi
    otherSample3l.setStatConfig(useStat)
    for sysName in sysNamesCommon:
        otherSample3l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesCommonFullSim:
        otherSample3l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesCommonOneSidedSym:
        otherSample3l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFullSim:
        otherSample3l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
if "AllChannel" in mode or "FourTriChannel" in mode:
    otherSample4l = Sample("other_4l", kYellow - 9)
    otherSample4l.setNormByTheory()  # scales with lumi
    otherSample4l.setStatConfig(useStat)
    for sysName in sysNamesCommon:
        otherSample4l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesCommonFullSim:
        otherSample4l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesCommonOneSidedSym:
        otherSample4l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFullSim:
        otherSample4l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))

dibosonSample = Sample('diboson', kBlue - 1)
dibosonSample.setNormByTheory()
dibosonSample.setStatConfig(useStat)
if not "ZVeto" in mode:
    dibosonSample.setNormFactor('mu_DB', 1., 0., 5.)
if "DiLepton" in mode:
    dibosonSample.setNormRegions([("WCR_SS_ee", variable), ("WCR_SS_mm", variable), ("WCR_SS_em", variable)])
if "TriLepton" in mode:
    dibosonSample.setNormRegions([("CR_3l0j_AS1_lll", variable2), ("CR_3l2j_2VB_AS1_lll", variable2)])
if "JetVeto" in mode:
    dibosonSample.setNormRegions([("CR_3l0j_AS1_lll", variable2)])
if "ZRegion" in mode: 
    dibosonSample.setNormRegions([("CR_3l2j_2VB_AS1_lll", variable2)])
if "FourLepton" in mode:
    dibosonSample.setNormRegions([("LowMass_4l_HalfHalf_llll", variable)])
if "DiTriChannel" in mode:
    dibosonSample.setNormRegions([("WCR_SS_ee", variable), ("WCR_SS_em", variable), ("WCR_SS_mm", variable), ("CR_3l0j_AS1_lll", variable2), ("CR_3l2j_2VB_AS1_lll", variable2)])
if "FourTriChannel" in mode:
    dibosonSample.setNormRegions([("LowMass_4l_HalfHalf_llll", variable), ("CR_3l0j_AS1_lll", variable2), ("CR_3l2j_2VB_AS1_lll", variable2)])
if "AllChannel" in mode:
    dibosonSample.setNormRegions([("WCR_SS_ee", variable), ("WCR_SS_em", variable), ("WCR_SS_mm", variable), ("LowMass_4l_HalfHalf_llll", variable), ("CR_3l0j_AS1_lll", variable2), ("CR_3l2j_2VB_AS1_lll", variable2)])
for sysName in sysNamesCommon:
    dibosonSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonFullSim:
    dibosonSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonOneSidedSym:
    dibosonSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
    dibosonSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
for sysName in sysNamesDibosonTheory:
    dibosonSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))

fakesSample = Sample('fakes', kRed - 6)
fakesSample.setStatConfig(True) # separate gamma parameters
# if 'sys' in mode:
    # fakesSample.addSystematic(Systematic('StatFakes', 'Nom_', 'High_', 'Low_', 'tree', 'shapeStat')) # upper limit on fakes


# Signal samples
if not disableSignal:
    sigName = suffix
    if suffix == 'default':
        sigName = 'sig700'

    # sysNamesSigTheory = [
    #     # 'THEORY_PDF_CHOICE_' + sigName,
    #     'THEORY_PDF_VARIATION_' + sigName,
    #     'THEORY_SCALE_' + sigName,
    # ]

    sigSample = Sample(sigName, kViolet)
    sigSample.setNormByTheory()
    sigSample.setStatConfig(useStat)
    sigSample.setNormFactor('mu_SIG', 1., 0., 5.)
    for sysName in sysNamesCommon:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonFastSim:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonOneSidedSym:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFastSim:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    # for sysName in sysNamesSigTheory:
    #     sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
            
# data
dataSample = Sample('data', kBlack)
dataSample.setData()


#**************
# fit
#**************
commonSamples = [dataSample, fakesSample, dibosonSample]

# Bkg-only template
if not "AllChannel" in mode and not "TriChannel" in mode:
    commonSamples += [otherSample]
elif "AllChannel" in mode:
    commonSamples += [otherSample2l, otherSample3l, otherSample4l]
elif "DiTriChannel" in mode:
    commonSamples += [otherSample2l, otherSample3l]
elif "FourTriChannel" in mode:
    commonSamples += [otherSample3l, otherSample4l]
if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
    commonSamples += [ttbarSample]
if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
    commonSamples += [raretopSample]
if not disableSignal:
    commonSamples += [sigSample]

# Parameters of the Measurement
measurementName = 'NormalMeasurement'
measurementLumi = 1.
if 'sys' in mode:
    measurementLumiError = 0.017  # 2015+16+17+18
else:
    measurementLumiError = 0.0001

if bkgOnly:
    fitConfig = configMgr.addFitConfig('Template_BkgOnly')
elif exclMode:
    fitConfig = configMgr.addFitConfig('Exclusion')
elif rankingMode:
    fitConfig = configMgr.addFitConfig('Ranking')

if useStat:
    fitConfig.statErrThreshold = 0.01  # values above this will be considered in the fit
else:
    fitConfig.statErrThreshold = None
fitConfig.addSamples(commonSamples)
measurement = fitConfig.addMeasurement(measurementName, measurementLumi, measurementLumiError)

if not disableSignal:
    fitConfig.setSignalSample(sigSample)
measurement.addPOI('mu_SIG')

# Regions
# region.hasB = False
# region.hasBQCD = False
# region.useOverflowBin = False

if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
    region_Top_OS_ee = fitConfig.addChannel(variable, ['Top_OS_ee'], 2, 0, 2)
    region_Top_OS_em = fitConfig.addChannel(variable, ['Top_OS_em'], 2, 0, 2)
    region_Top_OS_mm = fitConfig.addChannel(variable, ['Top_OS_mm'], 2, 0, 2)

    region_WCR_SS_ee = fitConfig.addChannel(variable, ['WCR_SS_ee'], 2, 0, 2)
    region_WCR_SS_em = fitConfig.addChannel(variable, ['WCR_SS_em'], 2, 0, 2)
    region_WCR_SS_mm = fitConfig.addChannel(variable, ['WCR_SS_mm'], 2, 0, 2)

    region_SR_2l2j_OS_ee = fitConfig.addChannel(variable, ['SR_2l2j_OS_ee'], 6, 0, 6)
    region_SR_2l2j_OS_em = fitConfig.addChannel(variable, ['SR_2l2j_OS_em'], 6, 0, 6)
    region_SR_2l2j_OS_mm = fitConfig.addChannel(variable, ['SR_2l2j_OS_mm'], 6, 0, 6)
    region_SR_2l2j_SS_ee = fitConfig.addChannel(variable, ['SR_2l2j_SS_ee'], 6, 0, 6)
    region_SR_2l2j_SS_em = fitConfig.addChannel(variable, ['SR_2l2j_SS_em'], 6, 0, 6)
    region_SR_2l2j_SS_mm = fitConfig.addChannel(variable, ['SR_2l2j_SS_mm'], 6, 0, 6)

if "JetVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    region_CR_3l0j_AS1_lll = fitConfig.addChannel(variable2, ['CR_3l0j_AS1_lll'], 2, 0, 2)
    region_SR_3l0j_AS1_lll = fitConfig.addChannel(variable2, ['SR_3l0j_AS1_lll'], 6, 0, 6)
if "ZRegion" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    region_CR_3l2j_2VB_AS1_lll = fitConfig.addChannel(variable2, ['CR_3l2j_2VB_AS1_lll'], 2, 0, 2)
    region_SR_3l2j_2VB_AS1_lll = fitConfig.addChannel(variable2, ['SR_3l2j_2VB_AS1_lll'], 6, 0, 6)
if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    region_SR_3l2j_VB_AS1_lll = fitConfig.addChannel(variable2, ['SR_3l2j_VB_AS1_lll'], 6, 0, 6)

if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
    region_LowMass_4l_HalfHalf_llll = fitConfig.addChannel(variable, ['LowMass_4l_HalfHalf_llll'], 2, 0, 2)
    region_LowTop_4l_HalfHalf_llll = fitConfig.addChannel(variable, ['LowTop_4l_HalfHalf_llll'], 1, 0, 1)

    region_SR_4l_HalfHalf_llll = fitConfig.addChannel(variable, ['SR_4l_HalfHalf_llll'], 2, 0, 2)
    region_SR_4l_OneDiff_llll = fitConfig.addChannel(variable, ['SR_4l_OneDiff_llll'], 3, 0, 3)

if "bkg" in mode:
    if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
        region_WSidebands_OS_ee = fitConfig.addChannel(variable, ['WSidebands_OS_ee'], 4, 0, 4)
        region_WSidebands_OS_em = fitConfig.addChannel(variable, ['WSidebands_OS_em'], 4, 0, 4)
        region_WSidebands_OS_mm = fitConfig.addChannel(variable, ['WSidebands_OS_mm'], 4, 0, 4)
        region_WSidebands_SS_ee = fitConfig.addChannel(variable, ['WSidebands_SS_ee'], 3, 0, 3)
        region_WSidebands_SS_em = fitConfig.addChannel(variable, ['WSidebands_SS_em'], 3, 0, 3)
        region_WSidebands_SS_mm = fitConfig.addChannel(variable, ['WSidebands_SS_mm'], 3, 0, 3)

    if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
        region_CR_3l2j_VB_AS1_lll = fitConfig.addChannel(variable2, ['CR_3l2j_VB_AS1_lll'], 2, 0, 2)
        # region_ZVRDr_AS1_lll = fitConfig.addChannel(variable2, ['ZVRSmT_AS1_lll'], 3, 0, 3)
        # region_JLowDr_AS1_lll = fitConfig.addChannel(variable2, ['JLowVRLmT_AS1_lll'], 4, 0, 4)
    # if "TriLepton" in mode or "AllChannel" in mode:
    #     region_VR_3l2j = fitConfig.addChannel(variable, ['VR_3l2j_AS1_lll'], 6, 0, 6)

    if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
        region_LowMass_4l_OneDiff_llll = fitConfig.addChannel(variable, ['LowMass_4l_OneDiff_OneDiff_llll'], 1, 0, 1)


has_electrons = []

if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
    has_electrons += [
        region_Top_OS_ee,
        region_Top_OS_em,
        region_WCR_SS_ee,
        region_WCR_SS_em,
        region_SR_2l2j_OS_ee,
        region_SR_2l2j_OS_em,
        region_SR_2l2j_SS_ee,
        region_SR_2l2j_SS_em,
    ]

if "JetVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    has_electrons += [
        region_CR_3l0j_AS1_lll,
        region_SR_3l0j_AS1_lll,
    ]

if "ZRegion" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    has_electrons += [
        region_CR_3l2j_2VB_AS1_lll,
        region_SR_3l2j_2VB_AS1_lll,
    ]

if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    has_electrons += [
        region_SR_3l2j_VB_AS1_lll,
    ]

if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
    has_electrons += [
        region_LowMass_4l_HalfHalf_llll,
        region_LowTop_4l_HalfHalf_llll,
        region_SR_4l_HalfHalf_llll,
        region_SR_4l_OneDiff_llll,
    ]

if "bkg" in mode:
    if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
        has_electrons += [
            region_WSidebands_OS_ee,
            region_WSidebands_OS_em,
            region_WSidebands_SS_ee,
            region_WSidebands_SS_em,
        ]

    if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
        has_electrons += [
            region_CR_3l2j_VB_AS1_lll,
            # region_ZVRDr_AS1_lll,
            # region_JLowDr_AS1_lll
        ]
    # if "TriLepton" in mode or "AllChannel" in mode:
    #     has_electrons += [       
    #         region_VR_3l2j,
    #     ]

    if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
        has_electrons += [
            region_LowMass_4l_OneDiff_llll
        ]


for region in has_electrons:
    for sysName in sysNamesElectron:
        region.getSample('diboson').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        if "DiLepton" in mode or "DiTriChannel" in mode:
            region.getSample('ttbar').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
            region.getSample('other_2l').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if "TriLepton" in mode or "TriChannel" in mode:
            region.getSample('other_3l').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if "FourLepton" in mode or "FourTriChannel" in mode:
            region.getSample('raretop').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
            region.getSample('other_4l').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesElectronFastSim:
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    if 'sys' in mode:
            region.getSample('fakes').addSystematic(Systematic('FAKES_Electron', 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))

has_muons = []

if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
    has_muons += [
        region_Top_OS_mm,
        region_Top_OS_em,
        region_WCR_SS_mm,
        region_WCR_SS_em,
        region_SR_2l2j_OS_mm,
        region_SR_2l2j_OS_em,
        region_SR_2l2j_SS_mm,
        region_SR_2l2j_SS_em,
    ]

if "JetVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    has_muons += [
        region_CR_3l0j_AS1_lll,
        region_SR_3l0j_AS1_lll,
    ]

if "ZRegion" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    has_muons += [
        region_CR_3l2j_2VB_AS1_lll,
        region_SR_3l2j_2VB_AS1_lll,
    ]

if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    has_muons += [
        region_SR_3l2j_VB_AS1_lll,
    ]

if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
    has_muons += [
        region_LowMass_4l_HalfHalf_llll,
        region_LowTop_4l_HalfHalf_llll,
        region_SR_4l_HalfHalf_llll,
        region_SR_4l_OneDiff_llll,
    ]

if "bkg" in mode:
    if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
        has_muons += [
            region_WSidebands_OS_mm,
            region_WSidebands_OS_em,
            region_WSidebands_SS_mm,
            region_WSidebands_SS_em,
        ]
        
    if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
        has_muons += [
            region_CR_3l2j_VB_AS1_lll,
            # region_ZVRDr_AS1_lll,
            # region_JLowDr_AS1_lll
        ]
    # if "TriLepton" in mode or "AllChannel" in mode:
    #     has_muons += [       
    #         region_VR_3l2j,
    #     ]

    if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
        has_muons += [
            region_LowMass_4l_OneDiff_llll
        ]


for region in has_muons:
    for sysName in sysNamesMuon:
        region.getSample('diboson').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        if "DiLepton" in mode or "DiTriChannel" in mode:
            region.getSample('ttbar').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
            region.getSample('other_2l').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if "TriLepton" in mode or "TriChannel" in mode:
            region.getSample('other_3l').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if "FourLepton" in mode or "FourTriChannel" in mode:
            region.getSample('raretop').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
            region.getSample('other_4l').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    if 'sys' in mode:
        region.getSample('fakes').addSystematic(Systematic('FAKES_Muon', 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))

CRs = []
if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
    CRs +=[region_Top_OS_ee, region_Top_OS_em, region_Top_OS_mm, region_WCR_SS_ee, region_WCR_SS_em, region_WCR_SS_mm]
if "JetVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    CRs +=[region_CR_3l0j_AS1_lll]
if "ZRegion" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    CRs +=[region_CR_3l2j_2VB_AS1_lll]
if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
    CRs += [region_LowMass_4l_HalfHalf_llll, region_LowTop_4l_HalfHalf_llll]

SRs = []
if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
    SRs += [region_SR_2l2j_OS_ee, region_SR_2l2j_OS_em, region_SR_2l2j_OS_mm, region_SR_2l2j_SS_ee, region_SR_2l2j_SS_em, region_SR_2l2j_SS_mm]
if "JetVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    SRs +=[region_SR_3l0j_AS1_lll]
if "ZRegion" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    SRs +=[region_SR_3l2j_2VB_AS1_lll]
if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
    SRs +=[region_SR_3l2j_VB_AS1_lll]
if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
    SRs += [region_SR_4l_HalfHalf_llll, region_SR_4l_OneDiff_llll]

if "bkg" in mode:
    VRs = []
    if "DiLepton" in mode or "AllChannel" in mode or "DiTriChannel" in mode:
        VRs += [region_WSidebands_OS_ee, region_WSidebands_OS_em, region_WSidebands_OS_mm, region_WSidebands_SS_ee, region_WSidebands_SS_em, region_WSidebands_SS_mm]
    if "ZVeto" in mode or "TriLepton" in mode or "AllChannel" in mode or "TriChannel" in mode:
        VRs +=[region_CR_3l2j_VB_AS1_lll]
    # if "TriLepton" in mode or "AllChannel" in mode:
    #     VRs += [region_VR_3l2j]
    if "FourLepton" in mode or "AllChannel" in mode or "FourTriChannel" in mode:
        VRs +=[region_LowMass_4l_OneDiff_llll]

if not disableSignal:
    for r in CRs:
        r.removeSample(sigName)

# CRs:
fitConfig.addBkgConstrainChannels(CRs)
# VRs:
if bkgOnly:
    fitConfig.addValidationChannels(VRs)
# SRs:
fitConfig.addSignalChannels(SRs)

