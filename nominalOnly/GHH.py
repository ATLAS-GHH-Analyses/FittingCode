from configManager import configMgr
from configWriter import Sample
from logger import INFO, Logger
from systematic import Systematic

#---------------------------------------
# Analysis setup
#---------------------------------------
if 'mode' not in dir():
    mode = 'excl_NLO_Inclusive'
    #mode = 'bkg_NLO_DiLepton_sig'
    #mode = 'bkg_NLO_Inclusive'

if 'suffix' not in dir():
    #suffix = 'GHH9f0fm5000'
    suffix = 'GHH6f650f0'

if 'variable' not in dir():
    variable = 'Meff'

analysis_name = 'GHH_' + str(mode) + '_' + str(suffix)
# analysis_name = 'GHH_' + str(mode) + '_' + str(suffix) + '_' + str(variable)
print analysis_name
RootName = 'ForFitter_newSFVg' #NLO, data-driven, ssWW
NbinsBoosted=3
NbinsResolved=5


Nbins = 4
sysType = 'overallNormHisto'
toys = 'toys' in mode or 'Toys' in mode
bkgOnly = 'bkg' in mode or 'BKG' in mode
asimov = 'asimov' in mode or 'Asimov' in mode
exclMode = 'excl' in mode or 'EXCL' in mode
limitMode = 'lim' in mode or 'LIM' in mode
rankingMode = "Ranking" in mode 
disableSignal = bkgOnly and not 'sig' in mode

# other_bkg_tmp = 'other_'
other_bkg = 'Others'
#---------------------------------------
# Logger
#---------------------------------------

log = Logger(analysis_name)
log.setLevel(INFO)  # should have no effect if -L is used

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
useStat = True  # statistics variation of samples

#-------------------------------
# Config manager basics
#-------------------------------

# Setting the parameters of the hypothesis test
configMgr.doExclusion = True  # True=exclusion, False=discovery
if toys:
    configMgr.nTOYs=5000
    configMgr.calculatorType = 0
else:
    configMgr.calculatorType = 2
configMgr.testStatType = 3
configMgr.nPoints = 20
# configMgr.scanRange = (0, 1)

configMgr.writeXML = True
configMgr.analysisName = analysis_name
configMgr.histCacheFile = 'data/SS2l'+RootName+'.root'
configMgr.outputFileName = 'results/'+analysis_name+'.root'

# activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = False  # enable the fallback to trees
# enable the use of an alternate data file
configMgr.useHistBackupCacheFile = True
# histogram templates - the data file of your previous fit, backup cache
configMgr.histBackupCacheFile = 'data/backupCacheFile_SS2l'+RootName+'.root'

# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 138.96516  # Luminosity of input TTree after weighting
configMgr.outputLumi = 138.96516  # Luminosity required for output histograms
configMgr.setLumiUnits('fb-1')

# blind
configMgr.blindSR = True
if asimov:
    print('Using Asimov Dataset!')
    configMgr.useAsimovSet=True

if rankingMode:
    print('INJECTING SIGNAL!')
    configMgr.useSignalInBlindedData = True


#-------------------------------------
# Now we start to build the data model
#-------------------------------------

# Systematics
if 'sys' in mode:
    sysNamesCommon = [
        'FT_EFF_B_systematics',
        'FT_EFF_C_systematics',
        'FT_EFF_extrapolation_from_charm',
        'FT_EFF_extrapolation',
        'FT_EFF_Light_systematics',
        'JET_BJES_Response',
        'JET_EffectiveNP_1',
        'JET_EffectiveNP_2',
        'JET_EffectiveNP_3',
        'JET_EffectiveNP_4',
        'JET_EffectiveNP_5',
        'JET_EffectiveNP_6',
        'JET_EffectiveNP_7',
        'JET_EffectiveNP_8restTerm',
        'JET_EtaIntercalibration_Modelling',
        'JET_EtaIntercalibration_NonClosure_highE',
        'JET_EtaIntercalibration_NonClosure_negEta',
        'JET_EtaIntercalibration_NonClosure_posEta',
        'JET_EtaIntercalibration_TotalStat',
        'JET_Flavor_Composition',
        'JET_Flavor_Response',
        'JET_JvtEfficiency',
        'JET_Pileup_OffsetMu',
        'JET_Pileup_OffsetNPV',
        'JET_Pileup_PtTerm',
        'JET_Pileup_RhoTopology',
        'JET_SingleParticle_HighPt',
        'MET_SoftTrk_Scale',
        'PRW_DATASF',
    ]
    sysNamesCommonFullSim = [
        'JET_PunchThrough_MC16',
    ]
    sysNamesCommonFastSim = [
        'JET_PunchThrough_AFII',
        'JET_RelativeNonClosure_AFII',
    ]

    sysNamesCommonOneSidedSym = [
        'JET_JER_EffectiveNP_1',
        'JET_JER_EffectiveNP_2',
        'JET_JER_EffectiveNP_3',
        'JET_JER_EffectiveNP_4',
        'JET_JER_EffectiveNP_5',
        'JET_JER_EffectiveNP_6',
        'JET_JER_EffectiveNP_7restTerm',
        'MET_SoftTrk_ResoPara',
        'MET_SoftTrk_ResoPerp',
    ]
    sysNamesCommonOneSidedFullSim = [
        'JET_JER_DataVsMC_MC16',
    ]
    sysNamesCommonOneSidedFastSim = [
        'JET_JER_DataVsMC_AFII',
    ]

    sysNamesElectron = [
        'EG_RESOLUTION_ALL',
        'EG_SCALE_ALL',
        'EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR',
        'EL_CHARGEID_SYStotal',
        'EL_CHARGEID_STAT',
    ]
    sysNamesElectronFastSim = [
        'EG_SCALE_AF2',
    ]
    sysNamesMuon = [
        'MUON_ID',
        'MUON_MS',
        'MUON_SAGITTA_RESBIAS',
        'MUON_SAGITTA_RHO',
        'MUON_SCALE',
        'MUON_EFF_RECO_STAT',
        'MUON_EFF_RECO_SYS',
        'MUON_EFF_RECO_STAT_LOWPT',
        'MUON_EFF_RECO_SYS_LOWPT',
        'MUON_EFF_ISO_STAT',
        'MUON_EFF_ISO_SYS',
        'MUON_EFF_TrigStatUncertainty',
        'MUON_EFF_TrigSystUncertainty',
        'MUON_EFF_TTVA_STAT',
        'MUON_EFF_TTVA_SYS',
        'MUON_EFF_BADMUON_SYS',
    ]

    sysNamesTTbarTheory = [
        'THEORY_PDF_CHOICE_ttbar',
        'THEORY_PDF_VARIATION_ttbar',
        'THEORY_SCALE_ttbar',
        'THEORY_ISR_ttbar',
    ]
    sysNamesTTbarTheoryOneSidedSym = [
        'THEORY_GENERATOR_ttbar',
        'THEORY_SHOWERING_ttbar',
        'THEORY_FSR_ttbar',
    ]
    sysNamesDibosonTheory = [
        'THEORY_PDF_CHOICE_diboson',
        'THEORY_PDF_VARIATION_diboson',
        'THEORY_SCALE_diboson',
    ]
else:
    sysNamesCommon = []
    sysNamesCommonFullSim = []
    sysNamesCommonFastSim = []
    sysNamesCommonOneSidedSym = []
    sysNamesCommonOneSidedFullSim = []
    sysNamesCommonOneSidedFastSim = []
    sysNamesElectron = []
    sysNamesElectronFastSim = []
    sysNamesMuon = []
    sysNamesTTbarTheory = []
    sysNamesTTbarTheoryOneSidedSym = []
    sysNamesDibosonTheory = []

# Dictionnary of cuts defining channels/regions (for Tree->hist)
if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
    configMgr.cutsDict["CR3lBoostedInc"] = "1."

    configMgr.cutsDict["SRSS2lBoostedee"] = "1."
    configMgr.cutsDict["SRSS2lBoostedemu"] = "1."
    configMgr.cutsDict["SRSS2lBoostedmue"] = "1."
    configMgr.cutsDict["SRSS2lBoostedmumu"] = "1."

if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
    configMgr.cutsDict["CR3lResolvedInc"] = "1."

    configMgr.cutsDict["SRSS2lResolvedee"] = "1."
    configMgr.cutsDict["SRSS2lResolvedemu"] = "1."
    configMgr.cutsDict["SRSS2lResolvedmue"] = "1."
    configMgr.cutsDict["SRSS2lResolvedmumu"] = "1."
if "Inclusive" in mode:
    configMgr.cutsDict["CR3lBoostedInc"] = "1."
    configMgr.cutsDict["CR3lResolvedInc"] = "1."
    configMgr.cutsDict["CR3lInc"] = "1."
    configMgr.cutsDict["ssWWResolvedInc"] = "1."
    configMgr.cutsDict["VRSS2lBoostedInc"] = "1."
    configMgr.cutsDict["VRSS2lResolvedInc"] = "1."
    configMgr.cutsDict["SRSS2lBoostedInc"] = "1."
    configMgr.cutsDict["SRSS2lResolvedInc"] = "1."

if bkgOnly:
    if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
        configMgr.cutsDict["VRSS2lBoostedee"] = "1."
        configMgr.cutsDict["VRSS2lBoostedemu"] = "1."
        configMgr.cutsDict["VRSS2lBoostedmue"] = "1."
        configMgr.cutsDict["VRSS2lBoostedmumu"] = "1."
    if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
        configMgr.cutsDict["VRSS2lResolvedee"] = "1."
        configMgr.cutsDict["VRSS2lResolvedemu"] = "1."
        configMgr.cutsDict["VRSS2lResolvedmue"] = "1."
        configMgr.cutsDict["VRSS2lResolvedmumu"] = "1."
    if "Inclusive" in mode:
        configMgr.cutsDict["VRSS2lBoostedInc"] = "1."
        configMgr.cutsDict["VRSS2lResolvedInc"] = "1."

# Define weights - dummy here
configMgr.weights = '1.'

# nominal name of the histograms with systematic variation
configMgr.nomName = 'Nom_'

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------

from ROOT import kGreen, kYellow, kViolet, kBlack, kBlue, kRed, kCyan, kAzure, kOrange, kMagenta


# if "DiLepton" in mode or "AllChannel" in mode in mode:
#     ttbarSample = Sample('ttbar', kGreen - 6)
#     ttbarSample.setNormByTheory()  # scales with lumi
#     ttbarSample.setStatConfig(useStat)
#     ttbarSample.setNormFactor('mu_ttbar', 1., 0., 5.)
#     # ttbarSample.setNormRegions([('Top_OS_ee', variable), ('Top_OS_mm', variable), ('Top_OS_em', variable)])
#     for sysName in sysNamesCommon:
#         ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#     for sysName in sysNamesCommonFullSim:
#         ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#     for sysName in sysNamesCommonOneSidedSym:
#         ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
#     for sysName in sysNamesCommonOneSidedFullSim:
#         ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
#     for sysName in sysNamesTTbarTheory:
#         ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#     for sysName in sysNamesTTbarTheoryOneSidedSym:
#         ttbarSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

VVVSample2l = Sample("VVV", kOrange-9)
VVVSample2l.setNormByTheory()  # scales with lumi
VVVSample2l.setStatConfig(useStat)
  

WZSample = Sample("WZ", kMagenta-7)
WZSample.setNormByTheory()
WZSample.setStatConfig(useStat)
WZSample.setNormFactor('mu_WZ', 1., 0., 5.)
if "DiLepton" in mode:
    WZSample.setNormRegions([("CR3lBoostedInc", variable), ("CR3lResolvedInc", variable)])
if "Inclusive" in mode:
    #WZSample.setNormRegions([("CR3lInc", variable)])
    WZSample.setNormRegions([("CR3lBoostedInc", variable), ("CR3lResolvedInc", variable)])

for sysName in sysNamesCommon:
    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonFullSim:
    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonOneSidedSym:
    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
for sysName in sysNamesDibosonTheory:
    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))


TopXSample = Sample("TopX",ROOT.kBlue-9)
TopXSample.setNormByTheory()
TopXSample.setStatConfig(True)

NonPromptSample = Sample("NonPrompt",ROOT.kAzure+10)
NonPromptSample.setNormByTheory()
NonPromptSample.setStatConfig(True)

ChargeFlipSample = Sample("ChargeFlip",ROOT.kGreen-7)
ChargeFlipSample.setNormByTheory()
ChargeFlipSample.setStatConfig(True)

VgammaSample = Sample("Vgamma",ROOT.kOrange+1)
VgammaSample.setNormByTheory()
VgammaSample.setStatConfig(True)

WWWSample = Sample("WWW",ROOT.kYellow)
WWWSample.setNormByTheory()
WWWSample.setStatConfig(True)

ssWWSample = Sample("ssWW",ROOT.kOrange-7)
ssWWSample.setNormByTheory()
ssWWSample.setStatConfig(True)
ssWWSample.setNormFactor('mu_ssWW', 1., 0., 10.)
if "DiLepton" in mode:
    ssWWSample.setNormRegions([("ssWWBoostedInc", variable), ("ssWWResolvedInc", variable)])
if "Inclusive" in mode:
    ssWWSample.setNormRegions([("ssWWResolvedInc", variable)])

ZZSample = Sample("ZZ",ROOT.kMagenta-10)
ZZSample.setNormByTheory()
ZZSample.setStatConfig(True)

# Signal samples
if not disableSignal:
    sigName = suffix
    if suffix == 'default':
        sigName = 'GHH9f0fm5000'

    sigSample = Sample(sigName, kRed-4)
    sigSample.setNormByTheory()
    sigSample.setStatConfig(useStat)
    sigSample.setNormFactor('mu_SIG', 1., 0., 20.)
   # sigSample.addSampleSpecificWeight("1.3")

    for sysName in sysNamesCommon:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonFastSim:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonOneSidedSym:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFastSim:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    # for sysName in sysNamesSigTheory:
    #     sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
            
# data
dataSample = Sample('data', kBlack)
dataSample.setData()


#**************
# fit
#**************
commonSamples = [dataSample, WZSample, WWWSample, VVVSample2l, ZZSample, TopXSample, ChargeFlipSample, NonPromptSample, VgammaSample, ssWWSample]

# Bkg-only template
# if not "AllChannel" in mode and not "TriChannel" in mode:
#     commonSamples += [otherSample]
# elif "AllChannel" in mode:
#     commonSamples += [otherSample2l, otherSample3l, otherSample4l]
# elif "DiTriChannel" in mode:
#     commonSamples += [otherSample2l, otherSample3l]
# elif "FourTriChannel" in mode:
#     commonSamples += [otherSample3l, otherSample4l]
if not disableSignal:
    commonSamples += [sigSample]

# Parameters of the Measurement
measurementName = 'NormalMeasurement'
measurementLumi = 1.
if 'sys' in mode:
    measurementLumiError = 0.017  # 2015+16+17+18
else:
    measurementLumiError = 0.0001

if bkgOnly:
    fitConfig = configMgr.addFitConfig('Template_BkgOnly')
elif exclMode:
    fitConfig = configMgr.addFitConfig('Exclusion')
elif rankingMode:
    fitConfig = configMgr.addFitConfig('Ranking')

if useStat:
    fitConfig.statErrThreshold = 0.01  # values above this will be considered in the fit
else:
    fitConfig.statErrThreshold = None
fitConfig.addSamples(commonSamples)
measurement = fitConfig.addMeasurement(measurementName, measurementLumi, measurementLumiError)

if not disableSignal:
    fitConfig.setSignalSample(sigSample)
measurement.addPOI('mu_SIG')

# Regions
# region.hasB = False
# region.hasBQCD = False
# region.useOverflowBin = False

if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
    region_3lBoostedCR_Inc = fitConfig.addChannel(variable, ['CR3lBoostedInc'], 1, 0, 1)

    region_SS2lBoostedSR_ee = fitConfig.addChannel(variable, ['SRSS2lBoostedee'], NbinsBoosted, 0, NbinsBoosted)
    region_SS2lBoostedSR_em = fitConfig.addChannel(variable, ['SRSS2lBoostedemu'], NbinsBoosted, 0, NbinsBoosted)
    region_SS2lBoostedSR_me = fitConfig.addChannel(variable, ['SRSS2lBoostedmue'], NbinsBoosted, 0, NbinsBoosted)
    region_SS2lBoostedSR_mm = fitConfig.addChannel(variable, ['SRSS2lBoostedmumu'], NbinsBoosted, 0, NbinsBoosted)

if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
    region_3lResolvedCR_Inc = fitConfig.addChannel(variable, ['CR3lResolvedInc'], 1, 0, 1)

    region_SS2lResolvedSR_ee = fitConfig.addChannel(variable, ['SRSS2lResolvedee'], NbinsResolved, 0, NbinsResolved)
    region_SS2lResolvedSR_em = fitConfig.addChannel(variable, ['SRSS2lResolvedemu'], NbinsResolved, 0, NbinsResolved)
    region_SS2lResolvedSR_me = fitConfig.addChannel(variable, ['SRSS2lResolvedmue'], NbinsResolved, 0, NbinsResolved)
    region_SS2lResolvedSR_mm = fitConfig.addChannel(variable, ['SRSS2lResolvedmumu'], NbinsResolved, 0, NbinsResolved)

if "Inclusive" in mode:
    region_3lBoostedCR_Inc = fitConfig.addChannel(variable, ['CR3lBoostedInc'], 1, 200, 3000)
    region_3lResolvedCR_Inc = fitConfig.addChannel(variable, ['CR3lResolvedInc'], 1, 200, 3000)
    #region_CR3l_Inc = fitConfig.addChannel(variable, ['CR3lInc'], 1, 0.5, 1.5)
    region_CRssWW_Inc = fitConfig.addChannel(variable, ['ssWWResolvedInc'], 1, 200, 1000)
    region_SS2lBoostedSR_Inc = fitConfig.addChannel(variable, ['SRSS2lBoostedInc'], NbinsBoosted, 0, NbinsBoosted)
    #region_SS2lBoostedSR_Inc.useOverflowBin=True
    #region_SS2lBoostedSR_Inc.useUnderflowBin=True
    region_SS2lResolvedSR_Inc = fitConfig.addChannel(variable, ['SRSS2lResolvedInc'], NbinsResolved, 0, NbinsResolved)
    #region_SS2lResolvedSR_Inc.useOverflowBin=True
    #region_SS2lResolvedSR_Inc.useUnderflowBin=True

if "bkg" in mode:
    if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
        region_SS2lBoostedVR_ee = fitConfig.addChannel(variable, ['VRSS2lBoostedee'], Nbins, 400, 2000)
        region_SS2lBoostedVR_em = fitConfig.addChannel(variable, ['VRSS2lBoostedemu'], Nbins, 400, 2000)
        region_SS2lBoostedVR_me = fitConfig.addChannel(variable, ['VRSS2lBoostedmue'], Nbins, 400, 2000)
        region_SS2lBoostedVR_mm = fitConfig.addChannel(variable, ['VRSS2lBoostedmumu'], Nbins, 400, 2000)
    if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
        region_SS2lResolvedVR_ee = fitConfig.addChannel(variable, ['VRSS2lResolvedee'], Nbins, 200, 1000)
        region_SS2lResolvedVR_em = fitConfig.addChannel(variable, ['VRSS2lResolvedemu'], Nbins, 200, 1000)
        region_SS2lResolvedVR_me = fitConfig.addChannel(variable, ['VRSS2lResolvedmue'], Nbins, 200, 1000)
        region_SS2lResolvedVR_mm = fitConfig.addChannel(variable, ['VRSS2lResolvedmumu'], Nbins, 200, 1000)
    if "Inclusive" in mode:
        region_SS2lBoostedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lBoostedInc'], Nbins, 400, 2000)
        #region_SS2lBoostedVR_Inc.useOverflowBin=True
        #region_SS2lBoostedVR_Inc.useUnderflowBin=True
        region_SS2lResolvedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lResolvedInc'], Nbins, 200, 1000)
        #region_SS2lResolvedVR_Inc.useOverflowBin=True
        #region_SS2lResolvedVR_Inc.useUnderflowBin=True


has_electrons = []

if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
    has_electrons += [
        # region_3lBoostedCR_eee,
        # region_3lBoostedCR_eem,
        # region_3lBoostedCR_mme,
	  region_3lBoostedCR_Inc,
        region_SS2lBoostedSR_ee,
        region_SS2lBoostedSR_em,
        region_SS2lBoostedSR_me,
    ]
if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
    has_electrons += [
        # region_3lResolvedCR_eee,
        # region_3lResolvedCR_eem,
        # region_3lResolvedCR_mme,
	  region_3lResolvedCR_Inc,
        region_SS2lResolvedSR_ee,
        region_SS2lResolvedSR_em,
        region_SS2lResolvedSR_me,
    ]
if "Inclusive" in mode:
    has_electrons += [
        region_3lBoostedCR_Inc,
        region_3lResolvedCR_Inc,
        #region_CR3l_Inc,
        region_CRssWW_Inc,
        #region_SS2lBoostedVR_Inc,
        #region_SS2lResolvedVR_Inc,
        region_SS2lBoostedSR_Inc,
        region_SS2lResolvedSR_Inc,
    ]


if "bkg" in mode:
    if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
        has_electrons += [
            region_SS2lBoostedVR_ee,
            region_SS2lBoostedVR_em,
            region_SS2lBoostedVR_me,
        ]
    if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
        has_electrons += [
            region_SS2lResolvedVR_ee,
            region_SS2lResolvedVR_em,
            region_SS2lResolvedVR_me,
        ]
    if "Inclusive" in mode:
        has_electrons += [
            region_SS2lBoostedVR_Inc,
            region_SS2lResolvedVR_Inc,
        ]


# for region in has_electrons:
#     for sysName in sysNamesElectron:
#         region.getSample('diboson').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#         if "AllChannel" in mode or "DiLepton" in mode or "Inclusive" in mode or "Boosted" in mode or "Resolved" in mode:
#             # region.getSample('ttbar').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#             region.getSample('Others').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
#         # if "TriLepton" in mode or "TriChannel" in mode:
#         #     region.getSample('other_3l').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
#         # if "FourLepton" in mode or "FourTriChannel" in mode:
#         #     region.getSample('raretop').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#         #     region.getSample('other_4l').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
#         if not disableSignal:
#             region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#     for sysName in sysNamesElectronFastSim:
#         if not disableSignal:
#             region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#     if 'sys' in mode:
#             region.getSample('fakes').addSystematic(Systematic('FAKES_Electron', 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))

has_muons = []

if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
    has_muons += [
        # region_3lBoostedCR_mmm,
        # region_3lBoostedCR_eem,
        # region_3lBoostedCR_mme,
	  region_3lBoostedCR_Inc,
        region_SS2lBoostedSR_mm,
        region_SS2lBoostedSR_em,
        region_SS2lBoostedSR_me,
    ]
if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
    has_muons += [
        # region_3lResolvedCR_mmm,
        # region_3lResolvedCR_eem,
        # region_3lResolvedCR_mme,
	  region_3lResolvedCR_Inc,
        region_SS2lResolvedSR_mm,
        region_SS2lResolvedSR_em,
        region_SS2lResolvedSR_me,
    ]
if "Inclusive" in mode:
    has_muons += [
        region_3lBoostedCR_Inc,
        region_3lResolvedCR_Inc,
        #region_CR3l_Inc,
        region_CRssWW_Inc,
        region_SS2lBoostedSR_Inc,
        region_SS2lResolvedSR_Inc,
    ]

if "bkg" in mode:
    if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
        has_muons += [
            region_SS2lBoostedVR_mm,
            region_SS2lBoostedVR_em,
            region_SS2lBoostedVR_me,
        ]
    if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
        has_muons += [
            region_SS2lResolvedVR_mm,
            region_SS2lResolvedVR_em,
            region_SS2lResolvedVR_me,
        ]
    if "Inclusive" in mode:
        has_muons += [
            region_SS2lBoostedVR_Inc,
            region_SS2lResolvedVR_Inc,
        ]


# for region in has_muons:
#     for sysName in sysNamesMuon:
#         region.getSample('diboson').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#         if "AllChannel" in mode or "DiLepton" in mode or "Inclusive" in mode or "Boosted" in mode or "Resolved" in mode:
#             # region.getSample('ttbar').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#             region.getSample('Others').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
#         if not disableSignal:
#             region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#     # if 'sys' in mode:
#     #     region.getSample('fakes').addSystematic(Systematic('FAKES_Muon', 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))

CRs = []
if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
   # CRs +=[region_3lBoostedCR_eee, region_3lBoostedCR_eem, region_3lBoostedCR_mme, region_3lBoostedCR_mmm]
   CRs +=[region_3lBoostedCR_Inc]
if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
   # CRs +=[region_3lResolvedCR_eee, region_3lResolvedCR_eem, region_3lResolvedCR_mme, region_3lResolvedCR_mmm]
   CRs +=[region_3lResolvedCR_Inc]
if "Inclusive" in mode:
   CRs +=[region_3lBoostedCR_Inc, region_3lResolvedCR_Inc, region_CRssWW_Inc]
   #CRs +=[region_CR3l_Inc]

SRs = []
if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
    SRs += [region_SS2lBoostedSR_ee, region_SS2lBoostedSR_em, region_SS2lBoostedSR_me, region_SS2lBoostedSR_mm]
if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
    SRs += [region_SS2lResolvedSR_ee, region_SS2lResolvedSR_em, region_SS2lResolvedSR_me, region_SS2lResolvedSR_mm]
if "Inclusive" in mode:
    SRs +=[region_SS2lBoostedSR_Inc, region_SS2lResolvedSR_Inc]
    # SRs +=[region_SS2lBoostedSR_Inc]
    # SRs +=[region_SS2lResolvedSR_Inc]

if "bkg" in mode:
    VRs = []
    if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
        VRs += [region_SS2lBoostedVR_ee, region_SS2lBoostedVR_em, region_SS2lBoostedVR_me, region_SS2lBoostedVR_mm]
    if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
        VRs += [region_SS2lResolvedVR_ee, region_SS2lResolvedVR_em, region_SS2lResolvedVR_me, region_SS2lResolvedVR_mm]
    if "Inclusive" in mode:
        VRs +=[region_SS2lBoostedVR_Inc, region_SS2lResolvedVR_Inc]


if not disableSignal:
    for r in CRs:
        r.removeSample(sigName)

#CRs:
fitConfig.addBkgConstrainChannels(CRs)
# VRs:
if bkgOnly:
    fitConfig.addValidationChannels(VRs)
# SRs:
fitConfig.addSignalChannels(SRs)

