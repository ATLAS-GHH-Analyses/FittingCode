import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--inputFile","-i",  type = str, help="input harvest file", default = "test.json")
args = parser.parse_args()

def cleanUpJSON():
    import json
    import glob
    for file in glob.glob("./*json"):
        print(">>> Making file human readable: %s"%file)
        data = json.load(open(file))
        with open(file, 'w') as f:
            f.write( json.dumps(data, indent=4) )
    return

filename = args.inputFile

with open(filename) as inputJSONFile:
    inputJSON = json.load(inputJSONFile)

with open("./XSections_13TeV.txt") as f:
    xsecs = f.readlines()
    xsecs = [x.strip() for x in xsecs if 'HHVV_0_6200_ML2' in x or 'HHVV_1350_0_ML2' in x]

xsecs_dict={}
for x in xsecs:
    xsecs_dict[x.split()[4]] = float(x.split()[1])

for signal in inputJSON:
    mass = int(signal['mass'])
    fW = int(signal['fW'])
    fWW = int(signal['fWW'])
    identifier = '{}_HHVV_{}_{}_ML2'.format(mass,fW,fWW)
    signal['xsec'] = xsecs_dict[identifier]*1000 # switch to fb units

with open(filename,"w") as f:
    f.write( json.dumps(inputJSON) )

cleanUpJSON()


