python comparepulls.py --extension png --group MUON\|EL --output compare_lepton_pulls \
  --dirr1 results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ \
  --dirr2 results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --leg1 excl --leg2 bkg

python comparepulls.py --extension png  --group THEORY\|Zjet\|TopX\|VVV\|ZZ\|WWWNorm --output compare_theory_pulls \
  --dirr1 results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ \
  --dirr2 results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --leg1 excl --leg2 bkg \
  --scaleY 1.3

python comparepulls.py --extension png --group MET\|Rate\|Lumi\|#mu\|#gamma\|EG\|InclusiveFF\|ZMass\|MCSubtraction\|PRW\|FAKES --output compare_other_pulls \
  --dirr1 results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ \
  --dirr2 results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --leg1 excl --leg2 bkg \
  --scaleY 1.2 --no_blue --lower_limit -4

python comparepulls.py --extension png --group FATJET --output compare_fatjet_pulls \
  --dirr1 results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ \
  --dirr2 results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --leg1 excl --leg2 bkg \
  --scaleY 2

python comparepulls.py --extension png  --group \^#alpha_JET --output compare_jet_pulls \
  --dirr1 results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ \
  --dirr2 results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --leg1 excl --leg2 bkg \
  --scaleY 1.3

python comparepulls.py --extension png --group FT_EFF --output compare_btag_pulls \
  --dirr1 results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ \
  --dirr2 results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --leg1 excl --leg2 bkg
