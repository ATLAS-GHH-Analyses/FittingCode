import ROOT
import os
import argparse
import numpy as np
from ROOT import *
import re

parser = argparse.ArgumentParser()
parser.add_argument("--multiple",action="store_true",default=False,help="split pulls over multiple plots, incompatible with --reduce, modify nps dictionary in script")
parser.add_argument("--reduce",action="store_true",default=False,help="keep only top n systematic pulls")
parser.add_argument("--no_gammas",action="store_true",default=False,help="Remove gamma n.p. from pull plot")
parser.add_argument("--nps",type=dict,default=None,help="manually define number of nps per canvas for --multiple plotting")
parser.add_argument("--n_plots",type=int,default=4,help="define n of canvasses in --multiple plotting")
parser.add_argument("--no_blue",action="store_true",default=False,help="Lumi and gamma_* will show up as blue")
parser.add_argument("--group",type=str,default=None,help="Plot n.p. starting with a given string. For example: '--group FT_EFF_' plots only b-tagging n.p.")
parser.add_argument("--extension",type=str,default="pdf",help="output plot file type: [pdf,png,eps,...]")
parser.add_argument("--output",type=str,default=None,help="manually set the output filename and directory")
parser.add_argument("--dirr1",type=str,default="GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/",help="directory of results for input")
parser.add_argument("--dirr2",type=str,default="GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/",help="directory of results for input")
parser.add_argument("--leg1",type=str,default="leg1",help="legend entry for results in dirr1")
parser.add_argument("--leg2",type=str,default="leg2",help="legend entry for results in dirr2")
parser.add_argument("--scaleX",type=float,default=1.0,help="Stretch the plot in the x-direction (formatting)")
parser.add_argument("--scaleY",type=float,default=1.0,help="Stretch the plot in the y-direction (formatting)")
parser.add_argument("--lower_limit",type=float,default=-3.2,help="Manually set the x-axis lower limit to give more space for the values (formatting)")

opt = parser.parse_args()
if opt.reduce and opt.multiple: raise NotImplementedError("cannot reduce and plot on multiple canvasses")


ROOT.gROOT.SetBatch(True)
# dirr = "./results/GHH_excl_Inclusive_sys_new_GHH6f650f0/"
dirr="/afs/cern.ch/work/g/grbarbou/HeavyHiggs/HFUpdated/"+opt.dirr1
dirr2="/afs/cern.ch/work/g/grbarbou/HeavyHiggs/HFUpdated/"+opt.dirr2
leg_entry_1 = opt.leg1
leg_entry_2 = opt.leg2
f = ROOT.TFile(dirr+'testSavePrims.root')
f2 = ROOT.TFile(dirr2+'testSavePrims.root')

def deduce_nps(g, n_plots):
    nbins = g.GetHistogram().GetYaxis().GetNbins()
    x = nbins // n_plots
    remainder = nbins % n_plots
    nps={}
    for i in range(n_plots):
        nps[i] = x+1 if i<remainder else x
    return nps

def reduced_reformat(g,axis1=None):
    g.GetHistogram().GetYaxis().SetLabelSize(0.023)#.023
    g.GetHistogram().GetXaxis().SetLabelSize(0.015)
    g.GetHistogram().GetXaxis().SetLabelOffset(0)
    g.GetHistogram().GetXaxis().SetTitleOffset(0.5)
    remove_alpha_prefix(g)
    if axis1 is not None:
        axis1.SetLabelSize(0.015)
        axis1.SetLabelOffset(-0.01)
        axis1.SetTitleSize(0.02)
        axis1.SetTitleOffset(.9)

def get_2band(count):
    sig2band = TGraph()
    sig2band.SetPoint(0, -2, 0)
    sig2band.SetPoint(1, -2, count + 2)
    sig2band.SetPoint(2, 2, count + 2)
    sig2band.SetPoint(3, 2, 0)
    sig2band.SetFillColor(kYellow)
    sig2band.SetFillStyle(1001)
    sig2band.Draw("Fsame")
    return sig2band

def get_axis(p_np):
    axis2 = TGaxis(p_np.GetUxmin(), p_np.GetUymax(), p_np.GetUxmax(), p_np.GetUymax(), p_np.GetUxmin() + 1,
                   p_np.GetUxmax() + 1, 510, "-L")
    axis2.SetLineColor(kBlue)
    axis2.SetLabelColor(kBlue)
    if opt.no_blue: axis2.SetTitle("#gamma / #mu")
    else: axis2.SetTitle("#mu")
    axis2.SetTitleColor(kBlue)
    axis2.SetLabelSize(0.015)
    axis2.SetLabelOffset(-0.01)
    axis2.SetTitleSize(0.02)
    axis2.SetTitleOffset(.9)
    return axis2

def draw_values(g, size=0.015):
    tmpText = ROOT.TLatex()
    color=kBlack
    alpha=1
    tmpText.SetTextSize(size)
    tmpText.SetTextColorAlpha(color, alpha)
    x, y, n = g.GetX(), g.GetY(), g.GetN()
    for i in range(n):
        parVal = g.GetPointX(i)
        parLowError=g.GetErrorXlow(i)
        parHighErrors=g.GetErrorXhigh(i)
        if opt.multiple:
            if i+1 < g.GetYaxis().GetFirst() or i+1 > g.GetYaxis().GetLast():
                continue
        if parLowError != parHighErrors:
            tmpText.DrawLatex(-3,y[i],"%.2f"%parVal+"\pm^{{%.2f}}_{{%.2f}}"%(parLowError,parHighErrors))
        else:
            if "#mu" in g.GetHistogram().GetYaxis().GetBinLabel(i+1):
                parVal+=1
            tmpText.DrawLatex(-3,y[i]-.3,"%.2f"%parVal+" \pm %.2f"%parLowError)

# Reduce to top 20 alpha pulls and plot on single canvas
def plot_reduced(g,_g,axis1,yMCstatBand,lMCstat,dirr,count):
    reduced_reformat(g,axis1)
    myC = ROOT.TCanvas("reformat_fit_params", "reformat_fit_params",1760,1400)
    myC.cd()
    p_np = ROOT.TPad("p_np","p_np",0,1,1,0.,0)
    p_np.SetLeftMargin(0.36) #.3
    p_np.Draw()
    p_np.cd()
    g.GetXaxis().SetLimits(opt.lower_limit,3) #4.2
    g.Draw("AP")
    myC.Update()
    # axis1.SetY1(p_np.GetUymax())
    # axis1.SetY2(p_np.GetUymax())
    # axis1.SetX1(p_np.GetUxmin())
    # axis1.SetX2(p_np.GetUxmax())
    sig2band = get_2band(count)
    yMCstatBand.SetFillColor(kGreen)
    yMCstatBand.SetFillStyle(1001)
    sig2band.Draw("Fsame")
    yMCstatBand.Draw("Fsame")
    lMCstat.Draw("same")
    axis2 = get_axis(p_np)
    axis2.Draw("same")
    draw_values(g)
    # axis1.Draw("same")
    g.Draw("Psame")
    _g.Draw("Psame")
    gPad.RedrawAxis()
    myC.Print(dirr+"reduced_pulls."+opt.extension)

def offset_g(g2):
    g2.SetMarkerColor(kRed)
    dy = 0.75
    ys = g2.GetY()
    for i in range(len(ys)):
        ys[i] = ys[i]-dy

def offset__g(_g2):
    _g2.SetMarkerColor(kCyan)
    dy = 0.75
    ys = _g2.GetY()
    for i in range(len(ys)):
        ys[i] = ys[i]-dy

def match_g_to_g2(g,g2):
    n = g.GetN()
    n2 = g2.GetN()
    for i in range(n):
        name = g.GetHistogram().GetYaxis().GetBinLabel(i+1)
        y1 = g.GetPointY(i)
        y2s = g2.GetY()
        for j in range(len(y2s)):
            name2 = g2.GetHistogram().GetYaxis().GetBinLabel(j+1)
            if name == name2:
                dy=y2s[j]-y1
                y2s[j] = y2s[j] - dy

def plot_group(g,_g,g2,_g2,axis1,yMCstatBand,lMCstat,dirr,count):
    reduced_reformat(g,axis1)
    reduced_reformat(g2)
    match_g_to_g2(g,g2)
    match_g_to_g2(_g,_g2)
    offset_g(g2)
    offset__g(_g2)
    myC = ROOT.TCanvas("reformat_fit_params", "reformat_fit_params",int(1760*opt.scaleX),int(1400*opt.scaleY)) #1600
    myC.cd()
    p_np = ROOT.TPad("p_np","p_np",0,1,1,0.,0)
    p_np.SetLeftMargin(0.4) #.3
    p_np.Draw()
    p_np.cd()
    g.GetXaxis().SetLimits(opt.lower_limit,3)
    g.Draw("AP")
    myC.Update()
    axis1.SetY1(p_np.GetUymax())
    axis1.SetY2(p_np.GetUymax())
    # axis1.Draw("same")
    sig2band = get_2band(count)
    yMCstatBand.SetFillColor(kGreen)
    yMCstatBand.SetFillStyle(1001)
    sig2band.Draw("Fsame")
    yMCstatBand.Draw("Fsame")
    lMCstat.Draw("same")
    if "#mu" in opt.group:
        axis2 = get_axis(p_np)
        axis2.Draw("same")
    draw_values(g)
    draw_values(g2)
    g.Draw("Psame")
    g2.Draw("Psame")
    if "#mu" in opt.group:
        _g.Draw("Psame")
        _g2.Draw("Psame")
    gPad.RedrawAxis()
    leg = ROOT.TLegend(0.91, 0.8, 1, 0.875)
    leg.AddEntry(g, leg_entry_1)
    if "#mu" in opt.group: leg.AddEntry(_g, leg_entry_1 + " #mu")
    leg.AddEntry(g2, leg_entry_2)
    if "#mu" in opt.group: leg.AddEntry(_g2, leg_entry_2 + " #mu")
    leg.SetTextSize(0.015)
    leg.SetBorderSize(0)
    leg.Draw()

    if opt.output is not None:
        myC.Print("pull_plots/"+opt.output+"."+opt.extension)
    else:
        myC.Print(dirr+opt.group+"_compared_pulls."+opt.extension)


def remove_alpha_prefix(g):
    nbins = g.GetHistogram().GetYaxis().GetNbins()
    for i in range(nbins):
        title = g.GetHistogram().GetYaxis().GetBinLabel(i + 1)
        if "#alpha" in title:
            g.GetHistogram().GetYaxis().SetBinLabel(i + 1, title[7:])


def indexes_to_keep(g, n=20):
    """
    Returns list of indices to keep, includes all mu and gamma np, and top N alpha np
    :param g: the TGraph
    :param n: keep top n systematic pulls
    """
    n_no_blue=0
    nbins = g.GetHistogram().GetYaxis().GetNbins()
    values=[]
    nps=[]
    keep_idx=[]
    for i in range(nbins):
        title = g.GetHistogram().GetYaxis().GetBinLabel(i + 1)
        if ("#gamma") in title or ("Lumi") in title:
            n_no_blue+=1
        if ("#mu" in title) or ("#gamma") in title or ("Lumi") in title:
            if opt.no_gammas and ("#gamma") in title:
                continue
            keep_idx.append(i)
            continue
        values.append(abs(g.GetPointX(i)))
        nps.append(i)
    inds = np.array(values).argsort()
    ranking = np.array(nps)[inds]
    # print(ranking)
    top_n = ranking[-n:].tolist() # gives the largest n, because ranking goes smallest to largest
    if n == 0: #dont rank if we keep all n.p.
        top_n = nps
    return top_n+keep_idx, n_no_blue

def group_indexes(g, group, n=0):
    """
    Returns list of indices to keep, includes all mu and gamma np, and top N alpha np
    :param g: the TGraph
    :param n: keep top n systematic pulls
    """
    nbins = g.GetHistogram().GetYaxis().GetNbins()
    values=[]
    nps=[]
    keep_idx=[]
    regex = re.compile(group)
    for i in range(nbins):
        title = g.GetHistogram().GetYaxis().GetBinLabel(i + 1)
        # if group in title:
        if regex.search(title):
            values.append(abs(g.GetPointX(i)))
            nps.append(i)

    if n == 0: #dont rank if we keep all n.p.
        return nps
    else:
        inds = np.array(values).argsort()
        ranking = np.array(nps)[inds]
        top_n = ranking[-n:].tolist()
        return top_n

def extract_values_to_keep(g, n=20, group=None):
    if group is not None:
        keep_idx = group_indexes(g, group)
    else:
        keep_idx, n_no_blue = indexes_to_keep(g, n)
    N = (2. * np.arange(1, len(keep_idx) + 1))
    errN = np.zeros(len(keep_idx))
    parNames = []
    parVals = []
    parLowErrors = []
    parHighErrors = []
    for i in keep_idx:
        parNames.append(g.GetHistogram().GetYaxis().GetBinLabel(i + 1))
        title = g.GetHistogram().GetYaxis().GetBinLabel(i + 1)
        if opt.no_blue and ("Lumi" in title or "gamma" in title):
            parVals.append(g.GetPointX(i)+1)
            parLowErrors.append(g.GetErrorXlow(i))
            parHighErrors.append(g.GetErrorXhigh(i))
        else:
            parVals.append(g.GetPointX(i))
            parLowErrors.append(g.GetErrorXlow(i))
            parHighErrors.append(g.GetErrorXhigh(i))
    parVals = np.array(parVals)
    parLowErrors = np.array(parLowErrors)
    parHighErrors = np.array(parHighErrors)
    #ToDo: Greg the -4 and -3 are hard-coded here (mu_SIG, mu_ssWW, mu_WZ, Lumi), change to calc automatically
    if opt.group:
        ctr=0
        for parName in parNames:
            if "#mu" in parName:
                ctr+=1
        idx=-1*ctr
    else:
        idx = n+n_no_blue if opt.no_blue else n
    _N = N[idx:]
    _parVals = parVals[idx:]
    _parNames2=parNames[idx:]
    return  N, errN, parNames, parVals, parLowErrors, parHighErrors, _N, _parVals, _parNames2


def reduce_g(N, errN, parNames, parVals, parLowErrors, parHighErrors):
    """
    Reduces g by keeping only top n alpha params
    """
    red_g = TGraphAsymmErrors(len(N), parVals, N, parLowErrors, parHighErrors, errN, errN)
    count = int(N[-1]) # i believe this is correct, so count/2 is total number of bins
    red_g.GetHistogram().GetYaxis().Set(int(count / 2), 1, count + 1)
    for i in range(len(N)):
        red_g.GetHistogram().GetYaxis().SetBinLabel(i+1, parNames[i])
    red_g.GetHistogram().GetYaxis().SetRangeUser(1, count + 1)
    red_g.GetHistogram().GetYaxis().SetTickLength(0.)
    red_g.GetHistogram().GetYaxis().SetLabelSize(0.035)
    red_g.GetHistogram().GetXaxis().SetRangeUser(-1.5, 1.5)
    red_g.GetHistogram().GetXaxis().SetTitle("#alpha")
    red_g.SetName("fit_results")
    red_g.SetTitle("")
    red_g.SetMarkerStyle(8)
    red_g.SetMarkerSize(2)
    red_g.SetLineWidth(1)

    return red_g


def reduce__g(_N, _paramVals, _parNames):
    red__g = TGraph(len(_N),_paramVals,_N)
    count = int(_N[-1])  # i believe this is correct, so count/2 is total number of bins
    red__g.GetHistogram().GetYaxis().Set(count / 2, 1, count + 1)
    for i in range(len(_N)):
        red__g.GetHistogram().GetYaxis().SetBinLabel(i+1, _parNames[i])
    red__g.SetName("_fit_results")
    red__g.SetTitle("")
    red__g.SetMarkerStyle(8)
    red__g.SetMarkerColor(kBlue)
    red__g.SetMarkerSize(2)
    return red__g

#######################################################################################################
# Main
g=f.Get("fit_results")
_g=f.Get("_fit_results")
axis1 = f.Get("axis1")
yMCstatBand = f.Get("yMCstatBand")
lMCstat = f.Get("lMCstat")

g2=f2.Get("fit_results")
_g2=f2.Get("_fit_results")

nbins_g1 = g.GetHistogram().GetYaxis().GetNbins()
nbins_g2 = g2.GetHistogram().GetYaxis().GetNbins()
if nbins_g1 < nbins_g2:
    print("Warning: the second fit contains more n.p. than the first. Will manually reverse the order to ensure all n.p. are shown")
    print("If comparing bkg only and excl fit, it is better to put the exclusion fit first (i.e. as --dirr1)")
    g = f2.Get("fit_results")
    _g = f2.Get("_fit_results")
    g2 = f.Get("fit_results")
    _g2 = f.Get("_fit_results")
    leg_entry_1 = opt.leg2
    leg_entry_2 = opt.leg1

if opt.group is not None:
    N, errN, parNames, parVals, parLowErrors, parHighErrors, _N, _parVals, _parNames = extract_values_to_keep(g, group=opt.group)
    red_g = reduce_g(N, errN, parNames, parVals, parLowErrors, parHighErrors)
    red__g = reduce__g(_N, _parVals, _parNames)
    count = int(N[-1]) # i believe this is correct, so count/2 is total number of bins
    lMCstat_new = TLine(0, 1, 0, int(N[-1]) + 1)
    lMCstat_new.SetLineStyle(8)

    #now do the same for the second fit
    N2, errN2, parNames2, parVals2, parLowErrors2, parHighErrors2, _N2, _parVals2, _parNames2 = extract_values_to_keep(g2, group=opt.group)
    red_g2 = reduce_g(N2, errN2, parNames2, parVals2, parLowErrors2, parHighErrors2)
    red__g2 = reduce__g(_N2, _parVals2, _parNames2)

    plot_group(red_g, red__g, red_g2, red__g2, axis1, yMCstatBand, lMCstat_new, dirr, count)

elif opt.reduce:
    raise NotImplementedError("work in progress")
    # N, errN, parNames, parVals, parLowErrors, parHighErrors, _N, _parVals = extract_values_to_keep(g, n=20)
    # red_g = reduce_g(N, errN, parNames, parVals, parLowErrors, parHighErrors)
    # red__g = reduce__g(_N, _parVals)
    # count = int(N[-1]) # i believe this is correct, so count/2 is total number of bins
    # lMCstat_new = TLine(0, 1, 0, int(N[-1])+1)
    # lMCstat_new.SetLineStyle(8)
    # plot_reduced(red_g, red__g, axis1, yMCstatBand, lMCstat_new, dirr, count)
else:
    raise NotImplementedError("only group or reduced")


########################################################################################################

########################################################################################################
