version="v1"
name="unblinded"
tag=final
source userSetup.sh

mkdir inputHypoTestResults_${tag}
mkdir inputjsons_${tag}
mkdir contourplots_${tag}
mkdir debug_surfaces_${tag}
mkdir outputGraphs_${tag}

# this first step creates the json files from the histfitter run results in results directory
for mass in 300 600 900
do
  hadd inputHypoTestResults_${tag}/GHH${mass}_SS2l_r04-02_sys_${name}_hypotest_"$version".root ../unblind_final/hypotest_contour/*GHH${mass}*hypotest*
  GenerateJSONOutput.py -i inputHypoTestResults_${tag}/GHH${mass}_SS2l_r04-02_sys_${name}_hypotest_"$version".root -f hypo_GHH%ffW%ffWW%f -p "mass:fW:fWW" -a '{"fW":"x","fWW":"y"}'
  mv *.json inputjsons_${tag}/
done

# the next step interpolates the exclusion contours from the json files
# python 3 is needed to use the scipy interpolate, and thus the harvestToContoursPython3.py script is needed
# setup python 3
lsetup "views LCG_98python3_ATLAS_1 x86_64-centos7-gcc8-opt"

interpolation='multiquadric'
for mass in 900 600 300
do
 mkdir debug_surfaces_${tag}/"$mass"
 if [ ${mass} == 900 ]
 then
   # briefly extend the x domain to draw full contours in the mass 900 case
   python plotting/harvestToContoursPython3.py -b -d -i inputjsons_${tag}/GHH"$mass"_SS2l_r04-02_sys_${name}_hypotest_"$version"__1_harvest_list.json -x fW -y fWW -l None --interpolationScheme rbf --interpolation $interpolation --sigmax 10 -o outputGraphs_${tag}/outputGraphs"$mass".root \
     --xMin -5000 --xMax 5000
 else
   python plotting/harvestToContoursPython3.py -b -d -i inputjsons_${tag}/GHH"$mass"_SS2l_r04-02_sys_${name}_hypotest_"$version"__1_harvest_list.json -x fW -y fWW -l None --interpolationScheme rbf --interpolation $interpolation --sigmax 10 -o outputGraphs_${tag}/outputGraphs"$mass".root
 fi

 #saving some debug plots
 mv DebugFinalCurves.pdf debug_surfaces_${tag}/"$mass"/DebugFinalCurves"$mass".pdf
 mv scipy_debug_surface_clsd1s.png debug_surfaces_${tag}/"$mass"/scipy_debug_surface_clsd1s_rbf_${interpolation}_"$mass"_python3_${name}.png
 mv scipy_debug_surface_clsu1s.png debug_surfaces_${tag}/"$mass"/scipy_debug_surface_clsu1s_rbf_${interpolation}_"$mass"_python3_${name}.png
 mv scipy_debug_surface_clsd2s.png debug_surfaces_${tag}/"$mass"/scipy_debug_surface_clsd2s_rbf_${interpolation}_"$mass"_python3_${name}.png
 mv scipy_debug_surface_clsu2s.png debug_surfaces_${tag}/"$mass"/scipy_debug_surface_clsu2s_rbf_${interpolation}_"$mass"_python3_${name}.png
 mv scipy_debug_surface.png debug_surfaces_${tag}/"$mass"/scipy_debug_surface_rbf_${interpolation}_"$mass"_python3_${name}.png

#this script does the final plotting: need python2 !!!!!!!
 rm -f contourplots_${tag}/GHH"$mass"_contour_plot_rbf__${interpolation}_python3_${name}_scale.*
  python plotting/plotContourGHH.py --mass $mass --label rbf__${interpolation}_python3_${name}_scale
  mv GHH"$mass"_contour_plot_rbf__${interpolation}_python3_${name}_scale.* contourplots_${tag}/
done
#
## need python 3 !!!
for fWfWW in 'fW1350fWW0' 'fW0fWW6200'
do
   rm -f contourplots_${tag}/GHH_mass_scan_${fWfWW}.*
   python plotting/plotContourGHH.py --fWfWW $fWfWW --label python3_mass_${fWfWW}
   mv GHH_mass_scan_${fWfWW}.* contourplots_${tag}/
    #python injection_WWWNorm/plotContourGHH_injection.py --fWfWW $fWfWW --toy
done
