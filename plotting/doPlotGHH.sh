python plotGHH.py --plot VRSS2lBoostedInc_Meff -m --info "SS2l VR Boosted" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --doBefore --atlas_left
python plotGHH.py --plot VRSS2lBoostedInc_Meff -m --info "SS2l VR Boosted" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left
python plotGHH.py --plot VRSS2lResolvedInc_Meff -m --info "SS2l VR Resolved" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --doBefore
python plotGHH.py --plot VRSS2lResolvedInc_Meff -m --info "SS2l VR Resolved" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotGHH.py --plot SRSS2lResolvedInc_Meff -m --info "SS2l SR Resolved" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotGHH.py --plot SRSS2lResolvedInc_Meff -m --info "SS2l SR Resolved" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --doBefore
python plotGHH.py --plot SRSS2lBoostedInc_Meff -m --info "SS2l SR Boosted" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --doBefore --atlas_left
python plotGHH.py --plot SRSS2lBoostedInc_Meff -m --info "SS2l SR Boosted" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left

python plotGHH.py --plot CR3lBoostedInc_Meff -m --info "WZ CR Boosted" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left
python plotGHH.py --plot CR3lBoostedInc_Meff -m --info "WZ CR Boosted" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left --doBefore
python plotGHH.py --plot CR3lResolvedInc_Meff -m --info "WZ CR Resolved" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left --doBefore
python plotGHH.py --plot CR3lResolvedInc_Meff -m --info "WZ CR Resolved" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left
python plotGHH.py --plot ssWWResolvedInc_Meff -m --info "ssWW CR Resolved" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left
python plotGHH.py --plot ssWWResolvedInc_Meff -m --info "ssWW CR Resolved" -F results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left --doBefore
python plotGHH.py --plot CR3lBoostedInc_Meff -m --info "WZ CR Boosted" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left
python plotGHH.py --plot CR3lBoostedInc_Meff -m --info "WZ CR Boosted" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left --doBefore
python plotGHH.py --plot CR3lResolvedInc_Meff -m --info "WZ CR Resolved" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left --doBefore
python plotGHH.py --plot CR3lResolvedInc_Meff -m --info "WZ CR Resolved" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left
python plotGHH.py --plot ssWWResolvedInc_Meff -m --info "ssWW CR Resolved" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left
python plotGHH.py --plot ssWWResolvedInc_Meff -m --info "ssWW CR Resolved" -F results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --atlas_left --doBefore
