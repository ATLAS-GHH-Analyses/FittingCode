########### Meff and SR plots 
path=/afs/cern.ch/work/y/yuxu/public/HeavyHiggs/GHH_FW/HistFitterTutorial/unblind_inclusive/
result=GHH_bkgwithSR_vars_Inclusive_sys_GHH6f650f0

#for var in Meff fatjet1Pt Lep1Pt Lep2Pt MET mll jet1Pt jet2Pt
for var in Meff mll jet1Pt fatjet1Pt MET Lep1Pt Lep2Pt jet2Pt
#for var in MET
do
   if [[ $var = Meff ]]; then
	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/
	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --doBefore
	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --doBefore --atlas_left
	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --atlas_left

	python plotting/plotGHH_vars.py --plot CR3lBoostedInc_${var} -m --info "WZ CR Boosted" -F ${path}/${result}/ --atlas_left
	python plotting/plotGHH_vars.py --plot CR3lBoostedInc_${var} -m --info "WZ CR Boosted" -F ${path}/${result}/ --atlas_left --doBefore
	python plotting/plotGHH_vars.py --plot CR3lResolvedInc_${var} -m --info "WZ CR Resolved" -F ${path}/${result}/ --atlas_left --doBefore
	python plotting/plotGHH_vars.py --plot CR3lResolvedInc_${var} -m --info "WZ CR Resolved" -F ${path}/${result}/ --atlas_left
	python plotting/plotGHH_vars.py --plot ssWWResolvedInc_${var} -m --info "ssWW CR Resolved" -F ${path}/${result}/ --atlas_left
	python plotting/plotGHH_vars.py --plot ssWWResolvedInc_${var} -m --info "ssWW CR Resolved" -F ${path}/${result}/ --atlas_left --doBefore
   elif [[ $var = Lep1Pt ]] || [[ $var = Lep2Pt ]] || [[ $var = MET ]] || [[ $var = mll ]]; then
	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --setLogy
	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --doBefore --setLogy
	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --setLogy
	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --atlas_left --setLogy
   elif [[ $var = fatjet1Pt ]]; then
	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --setLogy
	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --atlas_left --setLogy

   elif [[ $var = jet1Pt ]] || [[ $var = jet2Pt ]]; then
	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --setLogy
	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --doBefore --setLogy
   else
	echo "var not definied"
   fi
done
######### VR plots
path=results/
result=GHH_bkg_Inclusive_test_SRvars_VRvars_CRvars_GHH600fW1350fWW0
for var in mll jet1Pt fatjet1Pt MET Lep1Pt Lep2Pt jet2Pt
do
   if [[ $var = Lep1Pt ]] || [[ $var = Lep2Pt ]] || [[ $var = MET ]] || [[ $var = mll ]]; then
	python plotting/plotGHH_vars.py --plot VRSS2lResolvedInc_${var} -m --info "SS2l VR Resolved" -F ${path}/${result}/ --setLogy
	python plotting/plotGHH_vars.py --plot VRSS2lResolvedInc_${var} -m --info "SS2l VR Resolved" -F ${path}/${result}/ --doBefore --setLogy
	python plotting/plotGHH_vars.py --plot VRSS2lBoostedInc_${var} -m --info "SS2l VR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --setLogy
	python plotting/plotGHH_vars.py --plot VRSS2lBoostedInc_${var} -m --info "SS2l VR Boosted" -F ${path}/${result}/ --atlas_left --setLogy
   elif [[ $var = fatjet1Pt ]]; then
	python plotting/plotGHH_vars.py --plot VRSS2lBoostedInc_${var} -m --info "SS2l VR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --setLogy
	python plotting/plotGHH_vars.py --plot VRSS2lBoostedInc_${var} -m --info "SS2l VR Boosted" -F ${path}/${result}/ --atlas_left --setLogy

   elif [[ $var = jet1Pt ]] || [[ $var = jet2Pt ]]; then
	python plotting/plotGHH_vars.py --plot VRSS2lResolvedInc_${var} -m --info "SS2l VR Resolved" -F ${path}/${result}/ --setLogy
	python plotting/plotGHH_vars.py --plot VRSS2lResolvedInc_${var} -m --info "SS2l VR Resolved" -F ${path}/${result}/ --doBefore --setLogy
   else
	echo "var not definied"
   fi
done
####### CR plots
path=results/
result=GHH_excl_Inclusive_sys_nosig_CRvars_2_GHH600fW1350fWW0
for var in MET mlll jet1Pt Lep1Pt
do
   if [[ $var = MET ]] || [[ $var = mlll ]]; then
	python plotting/plotGHH_vars.py --plot CR3lResolvedInc_${var} -m --info "WZ CR Resolved" -F ${path}/${result}/ --setLogy
	python plotting/plotGHH_vars.py --plot CR3lResolvedInc_${var} -m --info "WZ CR Resolved" -F ${path}/${result}/ --doBefore --setLogy
	python plotting/plotGHH_vars.py --plot CR3lBoostedInc_${var} -m --info "WZ CR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --setLogy
	python plotting/plotGHH_vars.py --plot CR3lBoostedInc_${var} -m --info "WZ CR Boosted" -F ${path}/${result}/ --atlas_left --setLogy

   elif [[ $var = jet1Pt ]] || [[ $var = Lep1Pt ]]; then
	python plotting/plotGHH_vars.py --plot ssWWResolvedInc_${var} -m --info "ssWW CR Resolved" -F ${path}/${result}/ --setLogy
	python plotting/plotGHH_vars.py --plot ssWWResolvedInc_${var} -m --info "ssWW CR Resolved" -F ${path}/${result}/ --doBefore --setLogy
   else
	echo "var not definied"
   fi
done
####### SR Meff with two signals + bkg-only fit with CRs and SRs
#path=/afs/cern.ch/work/y/yuxu/public/HeavyHiggs/GHH_FW/HistFitterTutorial/unblind_inclusive/
#result=GHH_bkgwithSR_vars_Inclusive_sys_GHH6f650f0
#for var in Meff mll jet1Pt fatjet1Pt MET Lep1Pt Lep2Pt jet2Pt
#do
#   if [[ $var = Meff ]]; then
#     python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --plotTwoSignals
#     python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --doBefore --plotTwoSignals
#     python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --plotTwoSignals
#     python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --atlas_left --plotTwoSignals
#
#     python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --plotTwoSignals --setLogy
#     python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --doBefore --plotTwoSignals --setLogy
#     python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --plotTwoSignals --setLogy
#     python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --atlas_left --plotTwoSignals --setLogy
#   elif [[ $var = Lep1Pt ]] || [[ $var = Lep2Pt ]] || [[ $var = MET ]] || [[ $var = mll ]]; then
#	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --setLogy --plotTwoSignals
#	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --doBefore --setLogy --plotTwoSignals
#	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --setLogy --plotTwoSignals
#	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --atlas_left --setLogy --plotTwoSignals
#   elif [[ $var = fatjet1Pt ]]; then
#	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --doBefore --atlas_left --setLogy --plotTwoSignals
#	python plotting/plotGHH_vars.py --plot SRSS2lBoostedInc_${var} -m --info "SS2l SR Boosted" -F ${path}/${result}/ --atlas_left --setLogy --plotTwoSignals
#
#   elif [[ $var = jet1Pt ]] || [[ $var = jet2Pt ]]; then
#	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --setLogy --plotTwoSignals
#	python plotting/plotGHH_vars.py --plot SRSS2lResolvedInc_${var} -m --info "SS2l SR Resolved" -F ${path}/${result}/ --doBefore --setLogy --plotTwoSignals
#   else
#	echo "var not definied"
#  fi
#done
