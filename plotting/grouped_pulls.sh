python plotting/replotpulls.py --extension pdf --group THEORY\|Zjet\|TopX\|VVV\|ZZ\|WWWNorm --output theory_pulls_excl --dirr GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group THEORY\|Zjet\|TopX\|VVV\|ZZ\|WWWNorm --output theory_pulls_bkg --dirr GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group FATJET --output fatjet_pulls_excl --dirr GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group FATJET --output fatjet_pulls_bkg --dirr GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group \^#alpha_JET --output jet_pulls_excl --dirr GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group \^#alpha_JET --output jet_pulls_bkg --dirr GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group FT_EFF --output btag_pulls_excl --dirr GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group FT_EFF --output btag_pulls_bkg --dirr GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group MUON\|EL --output lepton_pulls_excl --dirr GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group MUON\|EL --output lepton_pulls_bkg --dirr GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/
python plotting/replotpulls.py --extension pdf --group MET\|Rate\|Lumi\|#mu\|#gamma\|EG\|InclusiveFF\|ZMass\|MCSubtraction\|PRW\|FAKES --output other_pulls_excl --dirr GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --lower_limit -3.8 --no_blue
python plotting/replotpulls.py --extension pdf --group MET\|Rate\|Lumi\|#mu\|#gamma\|EG\|InclusiveFF\|ZMass\|MCSubtraction\|PRW\|FAKES --output other_pulls_bkg --dirr GHH_bkg_Inclusive_sys_r0402_new_GHH600fW1350fWW0/ --no_blue
