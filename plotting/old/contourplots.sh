version="v1"
name="unblinded"

source resetup.sh

mkdir inputHypoTestResultsr04
mkdir inputjsons
mkdir contourplots
mkdir debug_surfaces
mkdir outputGraphs

# this first step creates the json files from the histfitter run results in results directory
for mass in 300 600 900
do
  hadd inputHypoTestResultsr04/GHH${mass}_SS2l_r04-02_sys_${name}_hypotest_"$version".root results/*GHH${mass}*hypotest*
  GenerateJSONOutput.py -i inputHypoTestResultsr04/GHH${mass}_SS2l_r04-02_sys_${name}_hypotest_"$version".root -f hypo_GHH%ffW%ffWW%f -p "mass:fW:fWW" -a '{"fW":"x","fWW":"y"}'
  mv *.json inputjsons/
done

# the next step interpolates the exclusion contours from the json files
# python 3 is needed to use the scipy interpolate, and thus the harvestToContoursPython3.py script is needed
lsetup "views LCG_98python3_ATLAS_1 x86_64-centos7-gcc8-opt"

interpolation='multiquadric'
for mass in 900 600 300
do
  mkdir debug_surfaces/"$mass"
  if [ ${mass} == 900 ]
  then
    # briefly extend the x domain to draw full contours in the mass 900 case
    python plotting/harvestToContoursPython3.py -b -d -i inputjsons/GHH"$mass"_SS2l_r04-02_sys_${name}_hypotest_"$version"__1_harvest_list.json -x fW -y fWW -l None --interpolationScheme rbf --interpolation $interpolation --sigmax 10 -o outputGraphs/outputGraphs"$mass".root \
      --xMin -5000 --xMax 5000
  else
    python plotting/harvestToContoursPython3.py -b -d -i inputjsons/GHH"$mass"_SS2l_r04-02_sys_${name}_hypotest_"$version"__1_harvest_list.json -x fW -y fWW -l None --interpolationScheme rbf --interpolation $interpolation --sigmax 10 -o outputGraphs/outputGraphs"$mass".root
  fi

  #saving some debug plots
  mv DebugFinalCurves.pdf debug_surfaces/"$mass"/DebugFinalCurves"$mass".pdf
  mv scipy_debug_surface_clsd1s.png debug_surfaces/"$mass"/scipy_debug_surface_clsd1s_rbf_${interpolation}_"$mass"_python3_${name}.png
  mv scipy_debug_surface_clsu1s.png debug_surfaces/"$mass"/scipy_debug_surface_clsu1s_rbf_${interpolation}_"$mass"_python3_${name}.png
  mv scipy_debug_surface_clsd2s.png debug_surfaces/"$mass"/scipy_debug_surface_clsd2s_rbf_${interpolation}_"$mass"_python3_${name}.png
  mv scipy_debug_surface_clsu2s.png debug_surfaces/"$mass"/scipy_debug_surface_clsu2s_rbf_${interpolation}_"$mass"_python3_${name}.png
  mv scipy_debug_surface.png debug_surfaces/"$mass"/scipy_debug_surface_rbf_${interpolation}_"$mass"_python3_${name}.png

  #this script does the final plotting
  python plotting/plotContourGHH.py --mass $mass --label rbf__${interpolation}_python3_${name}
  mv GHH"$mass"_contour_plot_rbf__${interpolation}_python3_${name}.* contourplots
done
