#!/usr/bin/env python

# contourPlotterExample.py #################
#
# Example for using contourPlotter.py
#
# See README for details
#
# By: Larry Lee - Dec 2017

import ROOT
import argparse
import contourPlotter
from ROOT import kGreen, kYellow, kBlue, kMagenta

parser = argparse.ArgumentParser()
parser.add_argument("--mass",type=str,default="",help="GHH mass contour to plot, [300,600,900]")
parser.add_argument("--label",type=str,default="",help="add extra string to end of plot name")
parser.add_argument("--fWfWW",type=str,default="",help="if used do mass v xsec plots, ['Combined','both','fW1350fWW0','fW0fWW6200']")
parser.add_argument("--no2sigma",action="store_true",default=False,help="dont draw +/- 2 sigma band")

opt = parser.parse_args()

if opt.fWfWW and opt.mass:
	raise ValueError("--mass cannot be called when plotting mass v xsec contours")

if opt.mass:
	drawTheorySysts = False

	plot = contourPlotter.contourPlotter("GHH{}_contour_plot_{}".format(opt.mass,opt.label),900,800) #800,600)
	plot.processLabel = "GHH mass = {} GeV".format(opt.mass)
	plot.lumiLabel = "#sqrt{s}=13 TeV"#, 1000 fb^{-1}, All limits at 95% CL"

	## Just open up a root file with TGraphs in it so you can hand them to the functions below!
	f = ROOT.TFile("outputGraphs/outputGraphs{}.root".format(opt.mass))
	f.ls()

	## Axes
	if opt.mass == "300": plot.drawAxes( [-2500,-9000,2500,9000] )
	elif opt.mass == "600": plot.drawAxes( [-2500,-11200,2500,11200] )
	elif opt.mass == "900": plot.drawAxes( [-3200,-16000,3200,16000] )
	else: raise NotImplemented("Incorrect mass value defined")

	## Main Result
	# plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")
	no2sigma=opt.no2sigma
	if not no2sigma: plot.drawOneSigmaBand(f.Get("Band_2s_0"), legendLabel="Expected limit #pm 2#sigma", color=kYellow, alpha=1., legendOrder=3)
	plot.drawOneSigmaBand(f.Get("Band_1s_0"), color=kGreen, alpha=1., legendLabel="Expected limit #pm 1#sigma", legendOrder=2)

	plot.drawExpected(f.Get("Exp_0"), alpha=1., title="Expected 95% CL limit", legendOrder=1)
	plot.drawObserved(      f.Get("Obs_0"), title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed 95% CL limit")

	## Draw Lines
	# plot.drawLine(  coordinates = [0,0,800,800], label = "Kinematically Forbidden or blah", style = 7, angle = 30 )

	## Axis Labels
	plot.setXAxisLabel( "fW" )
	plot.setYAxisLabel( "fWW"  )

	plot.createLegend(shape=(0.61,0.79,0.95,0.93) ).Draw()

	if drawTheorySysts:
		plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
		plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
		# coordinate in NDC
		plot.drawTheoryLegendLines( xyCoord=(0.234,0.6625), length=0.057 )

	plot.decorateCanvas( )
	plot.writePlot(format="pdf")
	plot.writePlot(format="png")

elif opt.fWfWW:
	drawTheorySysts = False
	drawboth = opt.fWfWW == 'both'

	if opt.fWfWW == 'both':
		plot = contourPlotter.contourPlotter("GHH_mass_v_xsec_contour_plot_{}".format(opt.label), 800, 600)
	else:
		plot = contourPlotter.contourPlotter("GHHx{}_mass_v_xsec_contour_plot_{}".format(opt.fWfWW,opt.label), 800, 600)

	if opt.fWfWW == 'both':
		plot.processLabel = "mass vs xsec for both pairs of (fW,fWW)"
	else:
		plot.processLabel = "GHHx{}".format(opt.fWfWW)
	plot.lumiLabel = "#sqrt{s}=13 TeV"  # , 1000 fb^{-1}, All limits at 95% CL"

	## Just open up a root file with TGraphs in it so you can hand them to the functions below!
	if opt.fWfWW == 'both':
		f = ROOT.TFile("outputGraphs/outputGraphsGHHxfW1350fWW0.root")
		f2 = ROOT.TFile("outputGraphs/outputGraphsGHHxfW0fWW6200.root")
	else:
		f = ROOT.TFile("outputGraphs/outputGraphsGHHx{}.root".format(opt.fWfWW))

	f.ls()

	## Axes
	if opt.fWfWW == 'fW1350fWW0':
		plot.drawAxes([300,0,2000,40])
	else:
		plot.drawAxes([300, 0, 2000, 176])
	# plot.setYAxisLog()

	## Main Result
	# plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")
	no2sigma = opt.no2sigma

	try:
		if not no2sigma:
			plot.drawOneSigmaBand(f.Get("Band_2s_0"), legendLabel="#pm2 #sigma_{exp}", color=kYellow, alpha=1.,
																				 legendOrder=3)
			if drawboth: plot.drawOneSigmaBand(f2.Get("Band_2s_0"), legendLabel="#pm2 #sigma_{exp}", color=kYellow, alpha=1.,
																					 legendOrder=3)
	except:
		print("No 2 sigma band was found, drawing the 1 sigma band only")

	plot.drawOneSigmaBand(f.Get("Band_1s_0"), color=kGreen, alpha=1., legendLabel="#pm1 #sigma_{exp}", legendOrder=2)
	if drawboth: plot.drawOneSigmaBand(f2.Get("Band_1s_0"), color=kGreen, alpha=1., legendLabel="#pm1 #sigma_{exp}", legendOrder=2)

	plot.drawExpected(f.Get("Exp_0"), alpha=1., title="Expected Limit", legendOrder=1)
	if drawboth: plot.drawExpected(f2.Get("Exp_0"), alpha=1., title="Expected Limit", legendOrder=1)

	# plot.drawObserved(f.Get("Obs_0"),
	# 									title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed Limit")
	if drawboth: plot.drawObserved(f2.Get("Obs_0"),
											title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed Limit")
	## Draw Lines
	# plot.drawLine(  coordinates = [0,0,800,800], label = "Kinematically Forbidden or blah", style = 7, angle = 30 )

	## Axis Labels
	plot.setXAxisLabel("m(H) [GeV]")
	plot.setYAxisLabel("95% CL upper limit on signal #sigma [fb]")

	plot.createLegend(shape=(0.62, 0.75, 0.95, 0.92)).Draw()

	plot.decorateCanvas()
	plot.writePlot(format="pdf")
	plot.writePlot(format="png")



