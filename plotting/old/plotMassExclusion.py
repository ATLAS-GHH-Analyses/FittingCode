import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from ROOT import kYellow, kGreen, gROOT
from ctypes import c_float


def make_df(results):
  with open("./XSections_13TeV.txt") as f:
    xsecs = f.readlines()
    xsecs = [x.strip() for x in xsecs if 'HHVV_0_6200_ML2' in x or 'HHVV_1350_0_ML2' in x]
  xsecs_dict = {}
  for x in xsecs:
    xsecs_dict[x.split()[4]] = float(x.split()[1])
  new_columns = {}
  for sig in results.index.values:
    i0 = sig.find('GHH') + 3
    i1 = sig.find('fW') + 2
    i2 = sig.find('fWW') + 3
    m = sig[i0:i1 - 2]
    fW = sig[i1:i2 - 3]
    fWW = sig[i2:]
    m = int(m)
    if "m" in fW:
      fW = -int(fW[1:])
    else:
      fW = int(fW)
    if "m" in fWW:
      fWW = -int(fWW[1:])
    else:
      fWW = int(fWW)
    identifier = '{}_HHVV_{}_{}_ML2'.format(m, fW, fWW)
    xsec = xsecs_dict[identifier] * 1000  # switch to fb units
    new_columns[sig] = [m, fW, fWW, xsec]
  new_df = pd.DataFrame.from_dict(new_columns, orient='index', columns=["mass", "fW", "fWW", "xsec"])
  full_df = new_df.join(results[['observed', 'median', '-1 sigma', '+1 sigma', '-2 sigma', '+2 sigma']])
  return full_df


results = pd.read_pickle("signal_limits_unblinded.pkl") #20211026.pkl")
# results = pd.read_pickle("archive/cleaned_on_20211124/signal_limits_20211026.pkl")
results = results.loc[[x for x in results.index.to_list() if "fW1350fWW0" in x or "fW0fWW6200" in x]]
signals = results.index.to_list()
full_df = make_df(results)

fig = plt.figure(figsize=(6.5,6.))

for fwfww in ["fW1350fWW0","fW0fWW6200"]:
    the_df= full_df.loc[[x for x in results.index.to_list() if fwfww in x]].sort_values(['mass'])
    fig = plt.figure(figsize=(6.5,6.))
    xsec=the_df['xsec']
    r=c_float()
    g=c_float()
    b=c_float()
    gROOT.GetColor(kGreen).GetRGB(r,g,b)
    plt.fill_between(the_df['mass'],the_df['-2 sigma']*xsec,the_df['+2 sigma']*xsec,color='yellow',label="Expected limit $\pm\ 2\sigma$")
    # plt.fill_between(the_df['mass'],the_df['-2 sigma']*xsec,the_df['+2 sigma']*xsec,color=(r2.value,g2.value,b2.value),label="$\pm2\ \sigma_{exp}$")
    plt.fill_between(the_df['mass'],the_df['-1 sigma']*xsec,the_df['+1 sigma']*xsec,color=(r.value,g.value,b.value),label="Expected limit $\pm\ 1\sigma$")
    plt.plot(the_df['mass'],xsec,'r',label="Theoretical")
    plt.plot(the_df['mass'],the_df['median']*xsec,'k--',label="Expected 95% CL limit")
    plt.plot(the_df['mass'],the_df['observed']*xsec,'k',label="Observed 95% CL limit")
    fw = fwfww[2:fwfww.find("fWW")]
    fww = fwfww[fwfww.find("fWW")+3:]
    plt.title("$(f_W,f_{{WW}})$ = ({},{}) ".format(fw,fww))
    plt.legend(loc = 'upper right')
    plt.grid()
    plt.xlabel("mass (GeV)")
    plt.ylabel("Signal Cross Section (fb)")
    x1, x2, y1, y2 = plt.axis()
    max_y = 200 if fwfww is "fW0fWW6200" else 70
    max_x = 1200 if fwfww is "fW0fWW6200" else 1500
    # max_x = 780 if fwfww is "fW0fWW6200" else 720
    plt.axis((300, max_x, 0, max_y))
    # plt.yscale('log')
    plt.savefig("mass_vs_xsec_{}unblinded.png".format(fwfww))
    plt.savefig("mass_vs_xsec_{}unblinded.pdf".format(fwfww))
