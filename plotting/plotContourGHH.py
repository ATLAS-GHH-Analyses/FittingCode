#!/usr/bin/env python

# contourPlotterExample.py #################
#
# Example for using contourPlotter.py
#
# See README for details
#
# By: Larry Lee - Dec 2017
import pandas as pd
import math
import ROOT
import argparse
import contourPlotter
from ROOT import kGreen, kYellow, kRed, kBlack, TLatex, gPad, TCanvas, TGraph
from array import array

ROOT.gROOT.LoadMacro("macros/atlasrootstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("macros/atlasrootstyle/AtlasUtils.C")
ROOT.SetAtlasStyle()

parser = argparse.ArgumentParser()
parser.add_argument("--mass",type=str,default="",help="GHH mass contour to plot, [300,600,900]")
parser.add_argument("--path",type=str,default=".",help="input path")
parser.add_argument("--label",type=str,default="",help="add extra string to end of plot name")
parser.add_argument("--fWfWW",type=str,default="",help="if used do mass v xsec plots, ['fW1350fWW0','fW0fWW6200']")
parser.add_argument("--no2sigma",action="store_true",default=False,help="dont draw +/- 2 sigma band")
parser.add_argument("--toy",action="store_true",default=False,help="draw pseudo-observed limit for mass scan, together with --fWfWW")

def make_df(results):
  with open("../unblind_inclusive/XSections_13TeV.txt") as f:
     xsecs = f.readlines()
     xsecs = [x.strip() for x in xsecs if 'HHVV_0_6200_ML2' in x or 'HHVV_1350_0_ML2' in x]
     xsecs_dict = {}
     for x in xsecs: xsecs_dict[x.split()[4]] = float(x.split()[1])
     new_columns = {}
     for sig in results.index.values:
        i0 = sig.find('GHH') + 3
        i1 = sig.find('fW') + 2
        i2 = sig.find('fWW') + 3
        m = sig[i0:i1 - 2]
        fW = sig[i1:i2 - 3]
        fWW = sig[i2:]
        m = int(m)
        if "m" in fW: fW = -int(fW[1:])
        else: fW = int(fW)
        if "m" in fWW: fWW = -int(fWW[1:])
        else: fWW = int(fWW)
        identifier = '{}_HHVV_{}_{}_ML2'.format(m, fW, fWW)
        xsec = xsecs_dict[identifier] * 1000  # switch to fb units
        new_columns[sig] = [m, fW, fWW, xsec]
     new_df = pd.DataFrame.from_dict(new_columns, orient='index', columns=["mass", "fW", "fWW", "xsec"])
     full_df = new_df.join(results[['observed', 'median', '-1 sigma', '+1 sigma', '-2 sigma', '+2 sigma']])
     return full_df

opt = parser.parse_args()

if opt.fWfWW and opt.mass:
	raise ValueError("--mass cannot be called when plotting mass v xsec contours")

if opt.mass:
	drawTheorySysts = False

	plot = contourPlotter.contourPlotter("GHH{}_contour_plot_{}".format(opt.mass,opt.label),900,800) #800,600)
	#if opt.mass==300:
	#   plot.processLabel = "m_{H} = 300 GeV"
	#elif opt.mass==600:
	#   plot.processLabel = "m_{H} = 600 GeV"
	#else: plot.processLabel = "m_{H} = 900 GeV"
	#plot.lumiLabel = "#sqrt{s}=13 TeV, 139 fb^{-1}"#, 1000 fb^{-1}, All limits at 95% CL"
	plot.processLabel = ""
	plot.lumiLabel = ""
	if opt.mass=="300": extraInfo = "m_{H} = 300 GeV"
	elif opt.mass=="600": extraInfo = "m_{H} = 600 GeV"
	else: extraInfo = "m_{H} = 900 GeV"

	## Just open up a root file with TGraphs in it so you can hand them to the functions below!
	f = ROOT.TFile("{}/outputGraphs/outputGraphs{}.root".format(opt.path,opt.mass))
	f.ls()
	#print(f.Get("Band_2s_0").GetN())
	x_fw_2sigma, y_fww_2sigma = array('d'), array('d')
	x_fw_1sigma, y_fww_1sigma = array('d'), array('d')
	x_fw_exp, y_fww_exp = array('d'), array('d')
	x_fw_obs, y_fww_obs = array('d'), array('d')
	for i in range(f.Get("Band_2s_0").GetN()):
	   x=ROOT.Double(0)
	   y=ROOT.Double(0)
	   f.Get("Band_2s_0").GetPoint(i, x, y)
	   x_fw_2sigma.append(x*0.05/25.)
	   y_fww_2sigma.append(y*0.05/25.)
	for i in range(f.Get("Band_1s_0").GetN()):
	   x=ROOT.Double(0)
	   y=ROOT.Double(0)
	   f.Get("Band_1s_0").GetPoint(i, x, y)
	   x_fw_1sigma.append(x*0.05/25.)
	   y_fww_1sigma.append(y*0.05/25.)
	for i in range(f.Get("Exp_0").GetN()):
	   x=ROOT.Double(0)
	   y=ROOT.Double(0)
	   f.Get("Exp_0").GetPoint(i, x, y)
	   x_fw_exp.append(x*0.05/25.)
	   y_fww_exp.append(y*0.05/25.)
	for i in range(f.Get("Obs_0").GetN()):
	   x=ROOT.Double(0)
	   y=ROOT.Double(0)
	   f.Get("Obs_0").GetPoint(i, x, y)
	   x_fw_obs.append(x*0.05/25.)
	   y_fww_obs.append(y*0.05/25.)
	g_exp2sigma = TGraph(f.Get("Band_2s_0").GetN(), x_fw_2sigma, y_fww_2sigma)
	g_exp1sigma = TGraph(f.Get("Band_1s_0").GetN(), x_fw_1sigma, y_fww_1sigma)
	g_exp = TGraph(f.Get("Exp_0").GetN(), x_fw_exp, y_fww_exp)
	g_obs = TGraph(f.Get("Obs_0").GetN(), x_fw_obs, y_fww_obs)
	## Axes
	if opt.mass == "300": plot.drawAxes( [-6,-16,6,25] , 1.3, 0.05)
	elif opt.mass == "600": plot.drawAxes( [-6,-18,6,30], 1.3, 0.05 )
	#elif opt.mass == "600": plot.drawAxes( [-12,-30,12,50], 1.3, 0.05 ) # resolved only
	elif opt.mass == "900": plot.drawAxes( [-8,-30,8,50], 1.3, 0.05 )
	#elif opt.mass == "900": plot.drawAxes( [-50,-250,50,300], 1.3, 0.05 ) # resolved only
	else: raise NotImplemented("Incorrect mass value defined")

	## Main Result
	# plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")
	no2sigma=opt.no2sigma
	if not no2sigma: plot.drawOneSigmaBand(g_exp2sigma, legendLabel="#pm 2#sigma_{exp}", color=kYellow, alpha=1., legendOrder=3)
	plot.drawOneSigmaBand(g_exp1sigma, color=kGreen, alpha=1., legendLabel="#pm 1#sigma_{exp}", legendOrder=2)

	plot.drawExpected(g_exp, alpha=1., title="Expected limit", legendOrder=1)
	plot.drawObserved(g_obs, title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed limit", color=kBlack, alpha=1.)

	## Draw Lines
	# plot.drawLine(  coordinates = [0,0,800,800], label = "Kinematically Forbidden or blah", style = 7, angle = 30 )

	## Axis Labels
	plot.setXAxisLabel( "#frac{#rho_{H}f_{W}}{#Lambda^{2}} [TeV^{-2}]" )
	plot.setYAxisLabel( "#frac{#rho_{H}f_{WW}}{#Lambda^{2}} [TeV^{-2}]"  )
	#plot.setXAxisLabel( "\\frac{\\rho_{H}f_{W}}{\Lambda^{2}}\,\\text{[TeV]}^{-2}" )
	#plot.setYAxisLabel( "\\frac{\\rho_{H}f_{WW}}{\Lambda^{2}}\,\\text{[TeV]}^{-2}"  )
	#plot.setXAxisLabel( "\\frac{f_{W}}{\Lambda^{2}}\,\\text{[TeV]}^{-2}" )
	#plot.setYAxisLabel( "\\frac{f_{WW}}{\Lambda^{2}}\,\\text{[TeV]}^{-2}"  )
	plot.createLegend(shape=(0.60,0.70,0.85,0.88) ).Draw()

	if drawTheorySysts:
		plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
		plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
		# coordinate in NDC
		plot.drawTheoryLegendLines( xyCoord=(0.234,0.6625), length=0.057 )
	plot.canvas.cd()
	xx=0.24
	yy=0.85 # 0.85, 0.90

	ROOT.ATLAS_LABEL(xx, yy)
	delx = 0.115*550*gPad.GetWh()/(472*gPad.GetWw())
	label_second = ROOT.TLatex()
	label_second.SetNDC()
	label_second.SetTextFont(42)
	label_second.SetTextSize(0.04)
	label_second.DrawLatex(xx+delx+0.03, yy, "Internal") #"Work in progress")

	info = ROOT.TLatex()
	info.SetNDC()
	info.SetTextSize(0.035)
	info.DrawLatex(xx, yy-.05, "#sqrt{s} = 13 TeV, 139 fb^{-1}")
	extra = ROOT.TLatex()
	extra.SetNDC()
	extra.SetTextFont(42)
	extra.SetTextSize(0.035)
	extra.DrawLatex(xx, yy-.1, extraInfo)

	plot.canvas.Update()
	plot.decorateCanvas( )
	plot.writePlot(format="pdf")
	plot.writePlot(format="png")
	plot.writePlot(format="eps")

if opt.fWfWW:
	if opt.toy:
		if opt.fWfWW=="fW1350fWW0": toydata="GHH300fW1350fWW0mubestfit"
		else: toydata="GHH300fW0fWW6200mubestfit"
		#resultsToy = pd.read_pickle("../unblind_injection/signal_limits_"+opt.fWfWW+".pkl")
		resultsToy = pd.read_pickle("../unblind_fixBin/signal_limits.pkl")
		resultsToy = resultsToy.loc[[x for x in resultsToy.index.to_list() if opt.fWfWW in x]]
		full_df_toy = make_df(resultsToy)
		the_df_toy= full_df_toy.loc[[x for x in resultsToy.index.to_list() if opt.fWfWW in x]].sort_values(['mass'])
	results = pd.read_pickle(opt.path+"/signal_limits.pkl")
	#results = pd.read_pickle("injection_WWWNorm/signal_limits.pkl")
	results = results.loc[[x for x in results.index.to_list() if opt.fWfWW in x]]
	signals = results.index.to_list()
	full_df = make_df(results)
	the_df= full_df.loc[[x for x in results.index.to_list() if opt.fWfWW in x]].sort_values(['mass'])
	drawTheorySysts = False
	#if opt.toy: plot = contourPlotter.contourPlotter("GHH_mass_scan_"+opt.fWfWW+"_toy",800,600)
	if opt.toy: plot = contourPlotter.contourPlotter("GHH_mass_scan_"+opt.fWfWW+"_fixBin",800,600)
	else: plot = contourPlotter.contourPlotter("GHH_mass_scan_"+opt.fWfWW,800,600)

	plot.processLabel = ""
	plot.lumiLabel = ""
	if opt.fWfWW=="fW1350fWW0": 
		extraInfo = "(f_{W}, f_{WW}) = (1350, 0)" # rhoHfW
		#extraInfo = "(f_{W}, f_{WW}) = (67.5, 0)"
		#plot.drawAxes( [300,0,1500,70] )
		plot.drawAxes( [300,0.5,1500,10000] ) #  log y
		#plot.drawAxes( [300,0,600,200] ) # resolved only
		n=14
		#n=10 # boosted only
	elif opt.fWfWW=="fW0fWW6200": 
		extraInfo = "(f_{W}, f_{WW}) = (0, 6200)" # rhoHfW
		#extraInfo = "(f_{W}, f_{WW}) = (0, 310)"
		#plot.drawAxes( [300,0,1200,200] )
		plot.drawAxes( [300,0.5,1200,100000] ) # log y
		#plot.drawAxes( [300,0,600,200] ) # resolved only
		n=13
		#n=12 #boosted only
	else: raise NotImplemented("Incorrect mass value defined") 
	x_mass, y_exp2sigmaup, y_exp2sigmado = array('d'), array('d'), array('d')
	y_exp1sigmaup, y_exp1sigmado, y_toy_obs = array('d'), array('d'), array('d')
	y_theory, y_exp, y_obs = array('d'), array('d'), array('d')
	x2_mass, y2_exp2sigma, y2_exp1sigma = array('d'), array('d'), array('d')
	for i in range( n ):
	   x_mass.append(the_df['mass'][i])
	   x2_mass.append(the_df['mass'][i])
	   y_exp2sigmaup.append(the_df['+2 sigma'][i]*the_df['xsec'][i])
	   y_exp2sigmado.append(the_df['-2 sigma'][i]*the_df['xsec'][i])
	   y2_exp2sigma.append(the_df['+2 sigma'][i]*the_df['xsec'][i])
	   y_exp1sigmaup.append(the_df['+1 sigma'][i]*the_df['xsec'][i])
	   y_exp1sigmado.append(the_df['-1 sigma'][i]*the_df['xsec'][i])
	   y2_exp1sigma.append(the_df['+1 sigma'][i]*the_df['xsec'][i])
	   y_exp.append(the_df['median'][i]*the_df['xsec'][i])
	   y_obs.append(the_df['observed'][i]*the_df['xsec'][i])
	   if opt.toy:
	      y_toy_obs.append(the_df_toy['observed'][i]*the_df['xsec'][i])
	      print(the_df_toy['observed'][i])
	   y_theory.append(the_df['xsec'][i])
	   #print(the_df['mass'][i])
	   #print(the_df['xsec'][i])
	for i in range( n ):
	   x2_mass.append(the_df['mass'][n-i-1])
	   y2_exp2sigma.append(the_df['-2 sigma'][n-i-1]*the_df['xsec'][n-i-1])
	   y2_exp1sigma.append(the_df['-1 sigma'][n-i-1]*the_df['xsec'][n-i-1])
	#print(y2_exp2sigma)
	g_exp2sigma = TGraph(2*n, x2_mass, y2_exp2sigma)
	g_exp1sigma = TGraph(2*n, x2_mass, y2_exp1sigma)
	g_exp = TGraph(n, x_mass, y_exp)
	g_obs = TGraph(n, x_mass, y_obs)
	if opt.toy: g_toy_obs = TGraph(n, x_mass, y_toy_obs) 
	g_theory = TGraph(n, x_mass, y_theory)

	## Just open up a root file with TGraphs in it so you can hand them to the functions below!
	#f = ROOT.TFile("outputGraphs_final/outputGraphs{}.root".format(opt.mass))
	#f.ls()
	### Axes
	#if opt.mass == "300": plot.drawAxes( [-2900,-8000,2900,12000] )
	#elif opt.mass == "600": plot.drawAxes( [-2900,-9000,2900,14900] )
	#elif opt.mass == "900": plot.drawAxes( [-3900,-14900,3900,24000] )
	#else: raise NotImplemented("Incorrect mass value defined")

	### Main Result
	## plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")
	no2sigma=opt.no2sigma
	if not no2sigma: plot.drawOneSigmaBand(g_exp2sigma, legendLabel="#pm 2#sigma_{exp}", color=kYellow, alpha=1., legendOrder=4)
	plot.drawOneSigmaBand(g_exp1sigma, color=kGreen, alpha=1., legendLabel="#pm 1#sigma_{exp}", legendOrder=3)

	plot.drawExpected(g_exp, alpha=1., title="Expected limit", legendOrder=2)
	plot.drawObserved(g_obs, title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed limit", color=kBlack, alpha=1., legendOrder=1)
	plot.drawObserved(g_theory, title="Theory", color=kRed, alpha=1., legendOrder=5)
	if opt.toy:
	   if toydata == "GHH300fW1350fWW0mubestfit": name_toy = "Exp. limit: injected GHH300fW1350fWW0 #mu_{SIG} = 0.30"
	   elif toydata == "GHH300fW0fWW6200mubestfit": name_toy = "Exp. limit: injected GHH300fW0fWW6200 #mu_{SIG} = 0.24"
	   #plot.drawExpected(g_toy_obs, color=kRed, alpha=1., title=name_toy, legendOrder=6)
	   plot.drawObserved(g_toy_obs, title="Pseudo-Observed Limit",  color=kRed, alpha=1., style=7, legendOrder=6)

	## Draw Lines
	# plot.drawLine(  coordinates = [0,0,800,800], label = "Kinematically Forbidden or blah", style = 7, angle = 30 )

	## Axis Labels
	plot.setXAxisLabel( "m_{H} [GeV]" )
	plot.setYAxisLabel( "#sigma(pp #rightarrow VH) #times BR(H #rightarrow VV) [fb]"  )
	#plot.setXAxisLabel( "m_{H}\,\\text{[GeV]}" )
	#plot.setYAxisLabel( "\\sigma(pp \\rightarrow VH) \\times \\text{BR}(H \\rightarrow VV)\,\\text{[fb]}"  )
	plot.setYAxisLog()

	if opt.toy: plot.createLegend(shape=(0.5,0.6,0.85,0.88) ).Draw()
	else: plot.createLegend(shape=(0.60,0.6,0.85,0.88) ).Draw()

	if drawTheorySysts:
		plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Up") )
		plot.drawTheoryUncertaintyCurve( f.Get("Obs_0_Down") )
		# coordinate in NDC
		plot.drawTheoryLegendLines( xyCoord=(0.234,0.6625), length=0.057 )
	plot.canvas.cd()
	xx=0.24
	yy=0.85 #0.85, 0.90

	ROOT.ATLAS_LABEL(xx, yy)
	delx = 0.115*550*gPad.GetWh()/(472*gPad.GetWw())
	label_second = ROOT.TLatex()
	label_second.SetNDC()
	label_second.SetTextFont(42)
	label_second.SetTextSize(0.04)
	label_second.DrawLatex(xx+delx+0.03, yy, "Internal") #"Work in progress")

	info = ROOT.TLatex()
	info.SetNDC()
	info.SetTextSize(0.035)
	info.DrawLatex(xx, yy-.05, "#sqrt{s} = 13 TeV, 139 fb^{-1}")
	extra = ROOT.TLatex()
	extra.SetNDC()
	extra.SetTextFont(42)
	extra.SetTextSize(0.035)
	extra.DrawLatex(xx, yy-.1, extraInfo)

	plot.canvas.Update()
	plot.decorateCanvas( )
	plot.writePlot(format="pdf")
	plot.writePlot(format="png")
	plot.writePlot(format="eps")
#elif opt.fWfWW:
#	drawTheorySysts = False
#	drawboth = opt.fWfWW == 'both'
#
#	if opt.fWfWW == 'both':
#		plot = contourPlotter.contourPlotter("GHH_mass_v_xsec_contour_plot_{}".format(opt.label), 800, 600)
#	else:
#		plot = contourPlotter.contourPlotter("GHHx{}_mass_v_xsec_contour_plot_{}".format(opt.fWfWW,opt.label), 800, 600)
#
#	if opt.fWfWW == 'both':
#		plot.processLabel = "mass vs xsec for both pairs of (fW,fWW)"
#	else:
#		plot.processLabel = "GHHx{}".format(opt.fWfWW)
#	plot.lumiLabel = "#sqrt{s}=13 TeV, 139 fb^{-1}"  # , 1000 fb^{-1}, All limits at 95% CL"
#
#	## Just open up a root file with TGraphs in it so you can hand them to the functions below!
#	if opt.fWfWW == 'both':
#		f = ROOT.TFile("outputGraphs/outputGraphsGHHxfW1350fWW0.root")
#		f2 = ROOT.TFile("outputGraphs/outputGraphsGHHxfW0fWW6200.root")
#	else:
#		f = ROOT.TFile("outputGraphs/outputGraphsGHHx{}.root".format(opt.fWfWW))
#
#	f.ls()
#
#	## Axes
#	if opt.fWfWW == 'fW1350fWW0':
#		plot.drawAxes([300,0,2000,40])
#	else:
#		plot.drawAxes([300, 0, 2000, 176])
#	# plot.setYAxisLog()
#
#	## Main Result
#	# plot.drawTextFromTGraph2D( f.Get("CLs_gr")  , angle=30 , title = "Grey Numbers Represent Observed CLs Value")
#	no2sigma = opt.no2sigma
#
#	try:
#		if not no2sigma:
#			plot.drawOneSigmaBand(f.Get("Band_2s_0"), legendLabel="#pm2 #sigma_{exp}", color=kYellow, alpha=1.,
#																				 legendOrder=3)
#			if drawboth: plot.drawOneSigmaBand(f2.Get("Band_2s_0"), legendLabel="#pm2 #sigma_{exp}", color=kYellow, alpha=1.,
#																					 legendOrder=3)
#	except:
#		print("No 2 sigma band was found, drawing the 1 sigma band only")
#
#	plot.drawOneSigmaBand(f.Get("Band_1s_0"), color=kGreen, alpha=1., legendLabel="#pm1 #sigma_{exp}", legendOrder=2)
#	if drawboth: plot.drawOneSigmaBand(f2.Get("Band_1s_0"), color=kGreen, alpha=1., legendLabel="#pm1 #sigma_{exp}", legendOrder=2)
#
#	plot.drawExpected(f.Get("Exp_0"), alpha=1., title="Expected Limit", legendOrder=1)
#	if drawboth: plot.drawExpected(f2.Get("Exp_0"), alpha=1., title="Expected Limit", legendOrder=1)
#
#	# plot.drawObserved(f.Get("Obs_0"),
#	# 									title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed Limit")
#	if drawboth: plot.drawObserved(f2.Get("Obs_0"),
#											title="Observed Limit (#pm1 #sigma_{theory}^{SUSY})" if drawTheorySysts else "Observed Limit")
#	## Draw Lines
#	# plot.drawLine(  coordinates = [0,0,800,800], label = "Kinematically Forbidden or blah", style = 7, angle = 30 )
#
#	## Axis Labels
#	plot.setXAxisLabel("m(H) [GeV]")
#	plot.setYAxisLabel("95% CL upper limit on signal #sigma [fb]")
#
#	plot.createLegend(shape=(0.62, 0.75, 0.95, 0.92)).Draw()
#
#	plot.decorateCanvas()
#	plot.writePlot(format="pdf")
#	plot.writePlot(format="png")



