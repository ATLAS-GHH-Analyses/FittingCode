#! /usr/bin/python
# This was copied from https://gitlab.cern.ch/atlas-phys-susy-wg/Common/Interpretation/-/blob/master/Plots/plotStopH.py
# This is the plotting script for various upgrade studies
from optparse import OptionParser
import logging
import os
from ROOT import kBlack, kWhite, kGray, kRed, kPink, kMagenta, kViolet, kBlue, kAzure, kCyan, kTeal, kGreen, kSpring, \
  kYellow, kOrange, kDashed, kSolid, kDotted
import ROOT
import math

ROOT.gROOT.SetBatch(True)

# Init the logger for the script and various modules
# INFO is not too noisy and helps to keep track of what is going on
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
log = logging.getLogger("myPlotLogger")
log.setLevel(logging.INFO)

# Init the Parser for the command line options
parser = OptionParser()
parser.add_option("-p", "--plot", dest='plot', default='VRSS2lBoostedInc_Meff', help="Analysis region to plot")
parser.add_option("-Z", "--significance", dest='significance', default='positive', help="Significance")
parser.add_option("-F", "--folder", dest='folder', default='./results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW2130fWW0/', help="Input folder")
# parser.add_option("-o", "--model_one", dest='model_one', default='T1T1_onestepN2N2_1000_150', help="Signal Model")
# parser.add_option("-s", "--selection", dest='selection', default='SRL', help="Selection")
# parser.add_option("-T", "--training", dest='training', default='5Lx10', help="Selection")
parser.add_option("--xlabel", dest='xlabel', default='M_{eff} [GeV]', help="X axis label")
parser.add_option("--ylabel", dest='ylabel', default='Events', help="Y axis label")
parser.add_option("--info", dest='info', default='', help="Extra info")
parser.add_option("--doBefore", action='store_true', default=False, help="plot before fit")  # not needed for upgrade
parser.add_option("--doBlind", action='store_true', default=False, help="doBlind search")  # not needed for upgrade
parser.add_option("--setLogy", action='store_true', default=False, help="Log plot")
parser.add_option("--doSignificance", dest='doSignificance', action='store_true', default=False,
                  help="compute significance in ratio box")
parser.add_option("--plotSignal", dest='plotSignal', action='store_true', default=False,
                  help="plot signal over the background")
parser.add_option("-m","--merge_backgrounds",dest='merge_backgrounds',action="store_true",default=False,help="merge background histograms")
parser.add_option("--atlas_left", action ="store_true", default=False,help="Put ATLAS label on left, legend on right")
parser.add_option("--data_over_b", action="store_true",default=False,help="Plot ratio of data to background only")
(options, args) = parser.parse_args()

signal = 0

options.doBlind = True # hard-coding this
# Put together the input file name here
if options.doBefore == True:
  inputFile = options.folder + options.plot + '_beforeFit.root'
else:
  inputFile = options.folder + options.plot + '_afterFit.root'

# inputFileSignal = options.folder + 'NewSignalDistrosStopH/' + options.plot + '_beforeFit.root'

# if (options.doSignificance or options.plotSignal):
#   file_sig = ROOT.TFile.Open(inputFileSignal, 'READ')
#   print
#   inputFileSignal, options.model_one, options.model_two, options.model_three, options.model_four, options.model_five
#   if file_sig.IsOpen():
#     signal = file_sig.Get(options.model_one)
#     if signal:
#       print
#       "File Open Correctly"
#


print
"INPUT FILE IS: ", inputFile
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("macros/atlasrootstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("macros/atlasrootstyle/AtlasUtils.C")
ROOT.SetAtlasStyle()
# Open the file with the pieces from the HistFitter run
file = ROOT.TFile.Open(inputFile, 'READ')

total_SM = file.Get("SM_total")
if total_SM:
  total_SM.SetLineColor(kBlue)
  error_band = file.Get("h_total_error_band")

if options.data_over_b:
  ratio = file.Get("h_ratio_excl_sig")
else:
  ratio = file.Get("h_ratio")
ratio_err = file.Get("h_rel_error_band")

if "excl" in options.folder:
  idx=options.folder.rfind("GHH")
  mc_signame = options.folder[idx:-1] #careful, trailing /
  print(mc_signame)
  mc_signal = file.Get(mc_signame)
  if mc_signal:
    mc_signal.SetFillColor(kRed)
    mc_signal.SetLineColor(kRed)

resolved = "Resolved" in options.plot

WZ = file.Get("WZResolved") if resolved else file.Get("WZBoosted")
if WZ:
  WZ.SetFillColor( kMagenta-7)
  WZ.SetLineColor( kMagenta-7)

WWW = file.Get("WWW")
if WWW:
  WWW.SetFillColor(kYellow)
  WWW.SetLineColor(kYellow)

VVV = file.Get("VVV")
if VVV:
  VVV.SetFillColor(kOrange-9)
  VVV.SetLineColor(kOrange-9)

ZZ = file.Get("ZZ")
if ZZ:
  ZZ.SetFillColor(kMagenta-10)
  ZZ.SetLineColor(kMagenta-10)

TopX = file.Get("TopX")
if TopX:
  TopX.SetFillColor(kBlue-9)
  TopX.SetLineColor(kBlue-9)

ChargeFlip = file.Get("ChargeFlip")
if ChargeFlip:
  ChargeFlip.SetFillColor(kGreen-7)
  ChargeFlip.SetLineColor(kGreen-7)

NonPromptEl = file.Get("NonPromptEl")
if NonPromptEl:
  NonPromptEl.SetFillColor(kAzure+10)
  NonPromptEl.SetLineColor(kAzure+10)

NonPromptMu = file.Get("NonPromptMu")
if NonPromptMu:
  NonPromptMu.SetFillColor(kAzure-4)
  NonPromptMu.SetLineColor(kAzure-4)

if options.merge_backgrounds:
  # NonPrompt = file.Get("NonPromptEl")
  # NonPrompt.Add("NonPromptMu",1.)
  if NonPromptEl and NonPromptMu:
    NonPrompt = NonPromptMu+NonPromptEl
    NonPrompt.SetFillColor(kAzure-4)
    NonPrompt.SetLineColor(kAzure-4)
  Others = VVV+ZZ+TopX
  Others.SetFillColor(kOrange-9)
  Others.SetLineColor(kOrange-9)

PhotonConversion = file.Get("PhotonConversion")
if PhotonConversion:
  PhotonConversion.SetFillColor(kOrange+1)
  PhotonConversion.SetLineColor(kOrange+1)

ssWW = file.Get("ssWW")
if ssWW:
  ssWW.SetFillColor(kOrange-7)
  ssWW.SetLineColor(kOrange-7)

data = file.Get("h_obsData")
# if option.plot == 'CRT_GenFiltMET':
#    data = dfile.Get("h_obsData")
# else:
#    data = file.Get("h_obsData")
data.SetLineColor(kBlack)

SM_stack = ROOT.THStack()

SM_stack.Add(WZ)
SM_stack.Add(WWW)
if options.merge_backgrounds:
  SM_stack.Add(Others)
  if ChargeFlip: SM_stack.Add(ChargeFlip)
  if NonPromptMu and NonPromptEl: SM_stack.Add(NonPrompt)
else:
  SM_stack.Add(VVV)
  SM_stack.Add(ZZ)
  SM_stack.Add(TopX)
  SM_stack.Add(ChargeFlip)
  SM_stack.Add(NonPromptEl)
  SM_stack.Add(NonPromptMu)
if PhotonConversion: SM_stack.Add(PhotonConversion)
SM_stack.Add(ssWW)
if "excl" in options.folder and not ("ssWW" in options.plot or "CR3l" in options.plot):
  SM_stack.Add(mc_signal)


for i in range(0, data.GetN()):
  eyh = data.GetErrorYhigh(i)
  eyl = data.GetErrorYlow(i)
  data.SetPointError(i, 0, 0, eyl, eyh)

c = ROOT.TCanvas(options.plot, "", 1000, 1000)
pad1 = ROOT.TPad("pad1", "pad1", 0.0, 0.3, 1.0, 1.0, 0)
pad2 = ROOT.TPad("pad2", "pad2", 0.0, 0.0, 1.0, 0.3, 0)

pad1.SetRightMargin(0.1)
pad1.SetBottomMargin(0.025)
pad1.Draw()
if options.setLogy:
  pad1.SetLogy()
pad2.SetRightMargin(0.1)
pad2.SetTopMargin(0.045)
pad2.SetBottomMargin(0.3)
pad2.Draw()

pad1.cd()

maxY = SM_stack.GetMaximum()
if signal:
  if signal.GetMaximum() > maxY:
    maxY = signal.GetMaximum()

SM_stack.Draw("HIST")
SM_stack.GetYaxis().SetTitle(options.ylabel)
SM_stack.GetYaxis().SetTitleOffset(1.5)
SM_stack.SetMaximum(1.4 * maxY)
if "Boosted" in options.plot and options.atlas_left:
  SM_stack.SetMaximum(2*maxY)
if "SR" in options.plot and "Boosted" in options.plot and not options.doBefore:
  SM_stack.SetMaximum(2.5*maxY)
if "ssWW" in options.plot or "CR3l" in options.plot:
  SM_stack.SetMaximum(2*maxY)
SM_stack.GetXaxis().SetLabelSize(0.)
# SM_stack.GetYaxis().SetLabelSize(0.06)

if options.setLogy:
  SM_stack.SetMaximum(100. * maxY)
  SM_stack.SetMinimum(0.1)
  SM_stack.GetYaxis().SetTitleOffset(1.)
  ROOT.gPad.SetLogy()

total_SM.Draw("HISTSAME")
# error_band.Draw("FSAME")

if signal:
  print
  "SIGNAL 1"
  # print(signal.GetBinContent(1))
  signal.SetLineStyle(2)
  signal.SetLineWidth(3)
  signal.SetLineColor(kRed + 2)
  #    ratio.SetMarkerColor(kPink+6)
  signal.SetFillStyle(0)
  signal.Draw("HISTSAME")


if (options.doBlind):
  if signal: raise NotImplemented("should be no signal if blinded, see plotStopH.py")


error_band.SetFillStyle(3004);
error_band.SetFillColor(kBlack);
data.SetMarkerSize(1.8);
ratio_err.SetFillStyle(3004)
ratio_err.SetFillColor(kBlack)

mgup = ROOT.TMultiGraph()
if not options.doSignificance:
  mgup.Add(data, "EP");
mgup.Add(error_band, "F");
mgup.Draw();
# data.Draw("EPSAME")

#legend placement
if "VR" in options.plot:
  if resolved or options.atlas_left: leg = ROOT.TLegend(0.61, 0.6, 0.96, 0.92, "")
  else: leg = ROOT.TLegend(0.186, 0.6, 0.516, 0.92, "")
if "SR" in options.plot:
  if resolved or options.atlas_left: leg = ROOT.TLegend(0.61, 0.6, 0.95, 0.92, "")
  else: leg = ROOT.TLegend(0.186, 0.6, 0.516, 0.92, "")
if "ssWW" in options.plot:
  if resolved or options.atlas_left: leg = ROOT.TLegend(0.61, 0.6, 0.95, 0.92, "")
  else: leg = ROOT.TLegend(0.186, 0.6, 0.516, 0.92, "")
if "CR3l" in options.plot:
  if resolved or options.atlas_left: leg = ROOT.TLegend(0.61, 0.6, 0.95, 0.92, "")
  else: leg = ROOT.TLegend(0.186, 0.6, 0.516, 0.92, "")

leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
total_SM_leg = file.Get("SM_total")
total_SM_leg.SetLineColor(kBlue)
total_SM_leg.SetFillStyle(3004)
total_SM_leg.SetFillColor(kBlack)
leg.AddEntry(total_SM_leg, "#font[42]{Model (s + b)}", "lf")
if resolved: leg.AddEntry(WZ, "#font[42]{WZResolved}", "f")
else: leg.AddEntry(WZ, "#font[42]{WZBoosted}", "f")
leg.AddEntry(WWW, "#font[42]{WWW}", "f")
if options.merge_backgrounds:
  leg.AddEntry(Others, "#font[42]{Others}", "f")
  if ChargeFlip: leg.AddEntry(ChargeFlip, "#font[42]{ChargeFlip}", "f")
  if NonPromptEl and NonPromptMu: leg.AddEntry(NonPrompt, "#font[42]{NonPrompt}", "f")
else:
  leg.AddEntry(VVV, "#font[42]{VVV}", "f")
  leg.AddEntry(ZZ, "#font[42]{ZZ}", "f")
  leg.AddEntry(TopX, "#font[42]{TopX}", "f")
  leg.AddEntry(ChargeFlip, "#font[42]{ChargeFlip}", "f")
  leg.AddEntry(NonPromptEl, "#font[42]{NonPromptEl}", "f")
  leg.AddEntry(NonPromptMu, "#font[42]{NonPromptMu}", "f")
if PhotonConversion: leg.AddEntry(PhotonConversion, "#font[42]{PhotonConversion}", "f")
leg.AddEntry(ssWW, "#font[42]{ssWW}", "f")
if "excl" in options.folder and not ("ssWW" in options.plot or "CR3l" in options.plot):
  leg.AddEntry(mc_signal, "#font[42]{{{}}}".format(mc_signame), "f")
# if options.doSignificance or options.plotSignal:
#   leg.AddEntry(signal, "#font[42]{" + options.model_one + "}", "l")

leg.Draw()
#label stuff
xx=.44 if "Boosted" in options.plot and not options.atlas_left else .19
yy=.884

ROOT.ATLAS_LABEL(xx, yy)

label_second = ROOT.TLatex()
label_second.SetNDC()
label_second.SetTextFont(42)
label_second.SetTextSize(0.035)
label_second.DrawLatex(xx+.12, yy, "Internal") #"Work in progress")

info = ROOT.TLatex()
info.SetNDC()
info.SetTextSize(0.03)
info.DrawLatex(xx, yy-.05, "#sqrt{s}=13 TeV, L = 139 fb^{-1}")

if options.info:
  extra = ROOT.TLatex()
  extra.SetNDC()
  extra.SetTextFont(42)
  extra.SetTextSize(0.035)
  extra.DrawLatex(xx, yy-.1, options.info)

ROOT.gPad.RedrawAxis()

pad2.cd()

if options.doSignificance:
  print("there!")
  significance_plot = WZ.Clone()
  for i in range(significance_plot.GetNbinsX() + 1):

    if signal:
      if options.significance == "positive":
        tot_bkg = total_SM.Integral(i, significance_plot.GetNbinsX())
        tot_sig = signal.Integral(i, significance_plot.GetNbinsX())
      else:
        tot_bkg = total_SM.Integral(0, i)
        tot_sig = signal.Integral(0, i)
        print("*********** Negative ************")
      significance = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(tot_sig, tot_bkg, 0.3)

      if math.isnan(significance):
        significance = 0.
      if math.isinf(significance):
        significance = 6.
      significance_plot.SetBinContent(i, significance)
      significance_plot.SetBinError(i, 0.)
      significance_plot.SetMarkerColor(signal.GetLineColor())

  significance_plot.SetMinimum(0.)
  significance_plot.SetMaximum(5.)
  if options.xlabel:
    significance_plot.GetXaxis().SetTitle(options.xlabel)
  significance_plot.GetYaxis().SetTitle("z_{N}")
  significance_plot.GetXaxis().SetTitleSize(0.12)
  significance_plot.GetYaxis().SetTitleSize(0.12)
  significance_plot.GetXaxis().SetTitleOffset(1.)
  significance_plot.GetYaxis().SetTitleOffset(0.5)
  significance_plot.GetXaxis().SetLabelSize(0.12)
  significance_plot.GetYaxis().SetLabelSize(0.12)
  significance_plot.GetYaxis().SetNdivisions(5)
  significance_plot.Draw("P")
  ROOT.gPad.RedrawAxis()



else:
  frame = WZ.Clone()
  for i in range(frame.GetNbinsX()):
    frame.SetBinContent(i, 0.)
  frame.SetMaximum(2.)
  frame.SetMinimum(0.)
  if "VR" in options.plot:
    frame.SetMaximum(1.5)
    frame.SetMinimum(0.5)

  frame.Draw("HIST")

  ratio.Draw("EP")
  ratio_err.Draw("F")

  if options.xlabel:
    frame.GetXaxis().SetTitle(options.xlabel)
  if options.data_over_b:
    frame.GetYaxis().SetTitle("Data/b")
  else:
    frame.GetYaxis().SetTitle("Data/(s+b)")
  frame.GetXaxis().SetTitleSize(0.12)
  frame.GetYaxis().SetTitleSize(0.12)
  frame.GetXaxis().SetTitleOffset(1.)
  frame.GetYaxis().SetTitleOffset(0.43)
  frame.GetXaxis().SetLabelSize(0.12)
  frame.GetYaxis().SetLabelSize(0.12)
  frame.GetYaxis().SetNdivisions(505)
  frame.GetYaxis().CenterTitle();
  pad2.SetGridy();

  frame.Draw("AXIS")
  frame.Draw("AXIG SAME")
  # ratio.Draw("EP")
  ratio_err.SetFillStyle(3004);
  ratio_err.SetMarkerColor(kBlack);
  ratio.SetMarkerSize(1.8);
  # ratio_err.Draw("a3 SAME");
  # ratio_err.Draw("F")

  mg = ROOT.TMultiGraph()
  mg.Add(ratio, "EP");
  mg.Add(ratio_err, "F");
  mg.Draw();

  ROOT.gPad.RedrawAxis()

  if "SR" in options.plot or "VR" in options.plot:
    if "Resolved" in options.plot:
      # frame.GetXaxis().SetBinLabel(1, "0-300")
      # frame.GetXaxis().SetBinLabel(2, "300-400")
      # frame.GetXaxis().SetBinLabel(3, "400-500")
      # frame.GetXaxis().SetBinLabel(4, "500-600")
      # frame.GetXaxis().SetBinLabel(5, "600-1000")
      frame.GetXaxis().ChangeLabel(2, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(3, -1, -1, -1, -1, -1, "300")
      frame.GetXaxis().ChangeLabel(4, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(5, -1, -1, -1, -1, -1, "400")
      frame.GetXaxis().ChangeLabel(6, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(7, -1, -1, -1, -1, -1, "500")
      frame.GetXaxis().ChangeLabel(8, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(9, -1, -1, -1, -1, -1, "600")
      frame.GetXaxis().ChangeLabel(10, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(11, -1, -1, -1, -1, -1, "1000")
    else:
      print("bla")
      frame.GetXaxis().ChangeLabel(2,-1,-1,-1,-1,-1," ")
      frame.GetXaxis().ChangeLabel(3,-1,-1,-1,-1,-1,"600")
      frame.GetXaxis().ChangeLabel(4,-1,-1,-1,-1,-1," ")
      frame.GetXaxis().ChangeLabel(5,-1,-1,-1,-1,-1,"800")
      frame.GetXaxis().ChangeLabel(6,-1,-1,-1,-1,-1," ")
      frame.GetXaxis().ChangeLabel(7,-1,-1,-1,-1,-1,"2000")
      # frame.GetXaxis().SetBinLabel(1, "0-600")
      # frame.GetXaxis().SetBinLabel(2, "600-800")
      # frame.GetXaxis().SetBinLabel(3, "800-2000")
    # frame.GetXaxis().SetLabelSize(0.16)

out_dir = os.path.join('plots_ghh/' + options.folder[options.folder.find('results')+8:])
if not os.path.exists(out_dir):
  os.makedirs(out_dir)
if options.doBefore == True:
  c.SaveAs(
    out_dir + options.plot + '_beforeFit' + '.pdf')
  c.SaveAs(
    out_dir + options.plot + '_beforeFit' + '.png')
else:
  c.SaveAs(
    out_dir + options.plot + '_afterFit' + '.pdf')
  c.SaveAs(
    out_dir + options.plot + '_afterFit' + '.png')
  c.Clear()
c.Close()




