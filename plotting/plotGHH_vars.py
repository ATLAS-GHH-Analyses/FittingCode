#! /usr/bin/python
# This was copied from https://gitlab.cern.ch/atlas-phys-susy-wg/Common/Interpretation/-/blob/master/Plots/plotStopH.py
# This is the plotting script for various upgrade studies
from optparse import OptionParser
import logging
import os
from ROOT import kBlack, kWhite, kGray, kRed, kPink, kMagenta, kViolet, kBlue, kAzure, kCyan, kTeal, kGreen, kSpring, \
  kYellow, kOrange, kDashed, kSolid, kDotted, TArrow
import ROOT
import math

ROOT.gROOT.SetBatch(True)

# Init the logger for the script and various modules
# INFO is not too noisy and helps to keep track of what is going on
logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
log = logging.getLogger("myPlotLogger")
log.setLevel(logging.INFO)

# Init the Parser for the command line options
parser = OptionParser()
parser.add_option("-p", "--plot", dest='plot', default='VRSS2lBoostedInc_Meff', help="Analysis region to plot")
parser.add_option("-Z", "--significance", dest='significance', default='positive', help="Significance")
parser.add_option("-F", "--folder", dest='folder', default='./results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW2130fWW0/', help="Input folder")
# parser.add_option("-o", "--model_one", dest='model_one', default='T1T1_onestepN2N2_1000_150', help="Signal Model")
# parser.add_option("-s", "--selection", dest='selection', default='SRL', help="Selection")
# parser.add_option("-T", "--training", dest='training', default='5Lx10', help="Selection")
#parser.add_option("--xlabel", dest='xlabel', default='M_{eff} [GeV]', help="X axis label")
parser.add_option("--ylabel", dest='ylabel', default='Events', help="Y axis label")
parser.add_option("--info", dest='info', default='', help="Extra info")
parser.add_option("--doBefore", action='store_true', default=False, help="plot before fit")  # not needed for upgrade
parser.add_option("--doBlind", action='store_true', default=False, help="doBlind search")  # not needed for upgrade
parser.add_option("--setLogy", action='store_true', default=False, help="Log plot")
parser.add_option("--doSignificance", dest='doSignificance', action='store_true', default=False,
                  help="compute significance in ratio box")
parser.add_option("--plotSignal", dest='plotSignal', action='store_true', default=False,
                  help="plot signal over the background")
parser.add_option("--plotTwoSignals", dest='plotTwoSignals', action='store_true', default=False,
                  help="plot signal GHH600fW1350fWW0 and GHH300fW0fWW4600 (no-stack) over the background")
parser.add_option("-m","--merge_backgrounds",dest='merge_backgrounds',action="store_true",default=False,help="merge background histograms")
parser.add_option("--atlas_left", action ="store_true", default=False,help="Put ATLAS label on left, legend on right")
parser.add_option("--data_over_b", action="store_true",default=False,help="Plot ratio of data to background only")
(options, args) = parser.parse_args()

signal = 0

options.doBlind = True # hard-coding this
# Put together the input file name here
if options.doBefore == True:
  inputFile = options.folder + options.plot + '_beforeFit.root'
  #if options.plotTwoSignals:
  #   inputFileSig1 = 'results/GHH_excl_Inclusive_sys_r0402_final_GHH300fW0fWW4600/'+ options.plot +'_beforeFit.root'
  #   inputFileSig2 = 'results/GHH_excl_Inclusive_sys_r0402_final_GHH600fW1350fWW0/'+ options.plot +'_beforeFit.root'
else:
  inputFile = options.folder + options.plot + '_afterFit.root'
  #if options.plotTwoSignals:
  #   inputFileSig1 = 'results/GHH_excl_Inclusive_sys_r0402_final_GHH300fW0fWW4600/'+ options.plot +'_beforeFit.root'
  #   inputFileSig2 = 'results/GHH_excl_Inclusive_sys_r0402_final_GHH600fW1350fWW0/'+ options.plot +'_beforeFit.root'

if options.plotTwoSignals and "SR" in options.plot:
  signalname1 = 'hGHH300fW0fWW4600Nom_'+options.plot.split('_')[0]+'_obs_'+options.plot.split('_')[1]
  signalname2 = 'hGHH600fW1350fWW0Nom_'+options.plot.split('_')[0]+'_obs_'+options.plot.split('_')[1]
# inputFileSignal = options.folder + 'NewSignalDistrosStopH/' + options.plot + '_beforeFit.root'

# if (options.doSignificance or options.plotSignal):
#   file_sig = ROOT.TFile.Open(inputFileSignal, 'READ')
#   print
#   inputFileSignal, options.model_one, options.model_two, options.model_three, options.model_four, options.model_five
#   if file_sig.IsOpen():
#     signal = file_sig.Get(options.model_one)
#     if signal:
#       print
#       "File Open Correctly"
#
xmin = 0
if "Meff" in options.plot: xlabel = "m_{\mathrm {eff}}\,\\text{[GeV]}"
elif "fatjet1Pt" in options.plot: 
   xlabel = "p_{\\text{T}}(\\text{J}_{1})\,\\text{[GeV]}"
   xmin = 200
   xmax = 700
   #binning = array.array('d', [200, 300, 400, 500, 600, 700, 800])
   #binningP = pointer(binning)
elif "Lep1Pt" in options.plot: 
   xlabel = "p_{\\text{T}}(\ell_{1})\,\\text{[GeV]}"
   xmin = 0 
   xmax = 480
elif "Lep2Pt" in options.plot: 
   xlabel = "p_{\\text{T}}(\ell_{2})\,\\text{[GeV]}"
   xmin = 20
   xmax = 200
elif "MET" in options.plot: 
   xlabel = "E_{\\text{T}}^{\\text{miss}}\,\\text{[GeV]}"
   if "CR3l" in options.plot: xmin = 40
   elif "ssWW" in options.plot: xmin = 30
   else: xmin = 60
   xmax = 330
elif "mlll" in options.plot: 
   xlabel = "m_{\ell\ell\ell}\,\\text{[GeV]}"
   xmin = 80
   xmax = 680
elif "mll" in options.plot: 
   xlabel = "m_{\ell\ell}\,\\text{[GeV]}"
   xmin = 100
   xmax = 600
elif "jet1Pt" in options.plot: 
   xlabel = "p_{\\text{T}}(\\text{j}_{1})\,\\text{[GeV]}"
   xmin = 0
   if "ssWW" in options.plot: xmax = 760
   else: xmax = 400
elif "jet2Pt" in options.plot: 
   xlabel = "p_{\\text{T}}(\\text{j}_{2})\,\\text{[GeV]}"
   xmin = 0
   xmax = 150
else: raise NotImplemented("Incorrect plot defined")

print
"INPUT FILE IS: ", inputFile
# Setting up the ATLAS style
ROOT.gROOT.LoadMacro("macros/atlasrootstyle/AtlasStyle.C")
ROOT.gROOT.LoadMacro("macros/atlasrootstyle/AtlasUtils.C")
ROOT.SetAtlasStyle()
# Open the file with the pieces from the HistFitter run
file = ROOT.TFile.Open(inputFile, 'READ')
if options.plotTwoSignals:
  #fileSig1 = ROOT.TFile.Open(inputFileSig1, 'READ')
  #fileSig2 = ROOT.TFile.Open(inputFileSig2, 'READ')
  fileSig = ROOT.TFile.Open('data/backupCacheFile_GHH_SS2l_r04-02_sys_20220416.root', 'READ')

total_SM = file.Get("SM_total")
if total_SM:
  #total_SM.SetLineColor(kBlue)
  #total_SM.SetLineColorAlpha(kBlue, 0.0)
  total_SM.SetLineWidth(1)
  total_SM.SetLineColorAlpha(kBlack, 0.0)
  error_band = file.Get("h_total_error_band")

if options.data_over_b:
  ratio = file.Get("h_ratio_excl_sig")
else:
  ratio = file.Get("h_ratio")
ratio_err = file.Get("h_rel_error_band")
if "Meff" not in options.plot: ratio.GetXaxis().SetRangeUser(xmin, xmax)

if "excl" in options.folder and options.plotSignal:
  idx=options.folder.rfind("GHH")
  mc_signame = options.folder[idx:-1] #careful, trailing /
  print(mc_signame)
  mc_signal = file.Get(mc_signame)
  if mc_signal:
    mc_signal.SetFillColor(kRed)
    mc_signal.SetLineColor(kRed)

resolved = "Resolved" in options.plot

WZ = file.Get("WZResolved") if resolved else file.Get("WZBoosted")
if WZ:
  WZ.SetFillColor( kMagenta-7)
  WZ.SetLineColor( kBlack )
  WZ.SetLineWidth( 1 )

WWW = file.Get("WWW")
if WWW:
  WWW.SetFillColor(kYellow)
  WWW.SetLineColor(kBlack)
  WWW.SetLineWidth(1)

VVV = file.Get("VVV")
if VVV:
  VVV.SetFillColor(kOrange-9)
  VVV.SetLineColor(kBlack)
  VVV.SetLineWidth(1)

ZZ = file.Get("ZZ")
if ZZ:
  ZZ.SetFillColor(kMagenta-10)
  ZZ.SetLineColor(kBlack)
  ZZ.SetLineWidth(1)

TopX = file.Get("TopX")
if TopX:
  TopX.SetFillColor(kBlue-9)
  TopX.SetLineColor(kBlack)
  TopX.SetLineWidth(1)

ChargeFlip = file.Get("ChargeFlip")
if ChargeFlip:
  ChargeFlip.SetFillColor(kGreen-7)
  ChargeFlip.SetLineColor(kBlack)
  ChargeFlip.SetLineWidth(1)

NonPromptEl = file.Get("NonPromptEl")
if NonPromptEl:
  NonPromptEl.SetFillColor(kAzure+10)
  NonPromptEl.SetLineColor(kBlack)
  NonPromptEl.SetLineWidth(1)

NonPromptMu = file.Get("NonPromptMu")
if NonPromptMu:
  NonPromptMu.SetFillColor(kAzure-4)
  NonPromptMu.SetLineColor(kBlack)
  NonPromptMu.SetLineWidth(1)

if options.merge_backgrounds:
  # NonPrompt = file.Get("NonPromptEl")
  # NonPrompt.Add("NonPromptMu",1.)
  if NonPromptEl and NonPromptMu:
    NonPrompt = NonPromptMu+NonPromptEl
    NonPrompt.SetFillColor(kAzure-4)
    NonPrompt.SetLineColor(kBlack)
    NonPrompt.SetLineWidth(1)
  Others = VVV+ZZ+TopX
  Others.SetFillColor(kOrange-9)
  Others.SetLineColor(kBlack)
  Others.SetLineWidth(1)

PhotonConversion = file.Get("PhotonConversion")
if PhotonConversion:
  PhotonConversion.SetFillColor(kOrange+1)
  PhotonConversion.SetLineColor(kBlack)
  PhotonConversion.SetLineWidth(1)

ssWW = file.Get("ssWW")
if ssWW:
  ssWW.SetFillColor(kOrange-7)
  ssWW.SetLineColor(kBlack)
  ssWW.SetLineWidth(1)

data = file.Get("h_obsData")
# if option.plot == 'CRT_GenFiltMET':
#    data = dfile.Get("h_obsData")
# else:
#    data = file.Get("h_obsData")
data.SetLineColor(kBlack)
if "Meff" not in options.plot: data.GetXaxis().SetRangeUser(xmin, xmax)

SM_stack = ROOT.THStack()

if options.merge_backgrounds: SM_stack.Add(Others)
else:
  SM_stack.Add(VVV)
  SM_stack.Add(ZZ)
  SM_stack.Add(TopX)
if ChargeFlip: SM_stack.Add(ChargeFlip)
if PhotonConversion: SM_stack.Add(PhotonConversion)
if options.merge_backgrounds:
  if NonPromptMu and NonPromptEl: SM_stack.Add(NonPrompt)
else:
  SM_stack.Add(NonPromptEl)
  SM_stack.Add(NonPromptMu)
SM_stack.Add(ssWW)
SM_stack.Add(WWW)
SM_stack.Add(WZ)
if "excl" in options.folder and options.plotSignal:
  SM_stack.Add(mc_signal)


for i in range(0, data.GetN()):
  eyh = data.GetErrorYhigh(i)
  eyl = data.GetErrorYlow(i)
  if data.GetY()[i] == 0: data.SetPointError(i, 0, 0, 0, 0)
  else: data.SetPointError(i, 0, 0, eyl, eyh)

c = ROOT.TCanvas(options.plot, "", 1000, 1000)
pad1 = ROOT.TPad("pad1", "pad1", 0.0, 0.3, 1.0, 1.0, 0)
pad2 = ROOT.TPad("pad2", "pad2", 0.0, 0.0, 1.0, 0.3, 0)

pad1.SetRightMargin(0.1)
pad1.SetBottomMargin(0.025)
pad1.Draw()
if options.setLogy:
  pad1.SetLogy()
pad2.SetRightMargin(0.1)
pad2.SetTopMargin(0.045)
pad2.SetBottomMargin(0.3)
pad2.Draw()

pad1.cd()

maxY = SM_stack.GetMaximum()
if signal:
  if signal.GetMaximum() > maxY:
    maxY = signal.GetMaximum()

SM_stack.Draw("HIST")
binwidth=str(int(WZ.GetBinWidth(0)))
if "Meff" not in options.plot: SM_stack.GetYaxis().SetTitle("Events / "+binwidth+" GeV")
else: SM_stack.GetYaxis().SetTitle(options.ylabel)
SM_stack.GetYaxis().SetTitleOffset(1.5)
if "Meff" not in options.plot: SM_stack.GetXaxis().SetRangeUser(xmin, xmax)
SM_stack.SetMaximum(1.4 * maxY)
if "Boosted" in options.plot and options.atlas_left:
  SM_stack.SetMaximum(2*maxY)
if "SR" in options.plot and "Boosted" in options.plot:
  SM_stack.SetMaximum(2.5*maxY)
if "SR" in options.plot and "Resolved" in options.plot:
  SM_stack.SetMaximum(2.5*maxY)
if "ssWW" in options.plot or "CR3l" in options.plot:
  SM_stack.SetMaximum(2*maxY)
if "ssWW" in options.plot and "Meff" in options.plot:
  SM_stack.SetMaximum(1200)
if "CR3lResolved" in options.plot and "Meff" in options.plot:
  SM_stack.SetMaximum(5500)
if "CR3lBoosted" in options.plot and "Meff" in options.plot:
  SM_stack.SetMaximum(550)
SM_stack.GetXaxis().SetLabelSize(0.)
SM_stack.GetYaxis().SetLabelSize(0.05)

if options.setLogy:
  SM_stack.SetMaximum(100. * maxY)
  if "ssWW" in options.plot and "Meff" not in options.plot: SM_stack.SetMaximum(300. * maxY)
  if options.plotTwoSignals: SM_stack.SetMaximum(2000. * maxY)
  if "SRSS2lBoosted" in options.plot and "Meff" in options.plot: SM_stack.SetMaximum(1000. * maxY)
  if "SRSS2lResolved" in options.plot and "Meff" in options.plot: SM_stack.SetMaximum(2000. * maxY)
  SM_stack.SetMinimum(0.1)
  SM_stack.GetYaxis().SetTitleOffset(1.)
  ROOT.gPad.SetLogy()
if "Meff" not in options.plot: total_SM.GetXaxis().SetRangeUser(xmin, xmax)
total_SM.Draw("HISTSAME")
# error_band.Draw("FSAME")

if options.plotTwoSignals:
  #signal1 = fileSig1.Get('GHH300fW0fWW4600')
  #signal1.SetLineStyle(2)
  #signal1.SetLineWidth(3)
  #signal1.SetLineColor(kGreen+2)
  #signal1.SetFillStyle(0)
  #signal1.Draw("HISTSAME")
  #signal2 = fileSig2.Get('GHH600fW1350fWW0')
  #signal2.SetLineStyle(2)
  #signal2.SetLineWidth(3)
  #signal2.SetLineColor(kBlue)
  #signal2.SetFillStyle(0)
  #signal2.Draw("HISTSAME")
  signal1 = fileSig.Get(signalname1)
  signal1.SetLineStyle(2)
  signal1.SetLineWidth(3)
  signal1.SetLineColor(kRed-4)
  signal1.SetFillStyle(0)
  signal1.Draw("HISTSAME")
  signal2 = fileSig.Get(signalname2)
  signal2.SetLineStyle(2)
  signal2.SetLineWidth(3)
  signal2.SetLineColor(kBlue)
  signal2.SetFillStyle(0)
  signal2.Draw("HISTSAME")

if (options.doBlind):
  if signal: raise NotImplemented("should be no signal if blinded, see plotStopH.py")


error_band.SetFillStyle(3004);
error_band.SetFillColor(kBlack);
if "Meff" not in options.plot: error_band.GetXaxis().SetRangeUser(xmin, xmax)
data.SetMarkerSize(1.8);
ratio_err.SetFillStyle(3004)
ratio_err.SetFillColor(kBlack)
if "Meff" not in options.plot: ratio_err.GetXaxis().SetRangeUser(xmin, xmax)

mgup = ROOT.TMultiGraph()
if not options.doSignificance:
  mgup.Add(data, "EP");
mgup.Add(error_band, "F");
mgup.Draw();
# data.Draw("EPSAME")

#legend placement
if "VR" in options.plot:
  if resolved or options.atlas_left: leg = ROOT.TLegend(0.55, 0.55, 0.96, 0.92, "")
  else: leg = ROOT.TLegend(0.186, 0.6, 0.516, 0.92, "")
if "SR" in options.plot:
  if resolved or options.atlas_left: leg = ROOT.TLegend(0.55, 0.55, 0.95, 0.92, "")
  else: leg = ROOT.TLegend(0.186, 0.6, 0.516, 0.92, "")
if "ssWW" in options.plot:
  if resolved or options.atlas_left: leg = ROOT.TLegend(0.55, 0.55, 0.95, 0.92, "")
  else: leg = ROOT.TLegend(0.186, 0.6, 0.516, 0.92, "")
if "CR3l" in options.plot:
  if resolved or options.atlas_left: leg = ROOT.TLegend(0.55, 0.70, 0.95, 0.92, "")
  else: leg = ROOT.TLegend(0.186, 0.6, 0.516, 0.92, "")

leg.SetFillStyle(0)
leg.SetFillColor(0)
leg.SetBorderSize(0)
leg.SetMargin(0.20)
leg.SetTextSize(0.04)
total_SM_leg = file.Get("SM_total")
#total_SM_leg.SetLineColor(kBlack)
total_SM_leg.SetLineWidth(0)
total_SM_leg.SetLineColorAlpha(kWhite, 0.0)
total_SM_leg.SetFillStyle(3004)
total_SM_leg.SetFillColor(kBlack)
leg.AddEntry(data, "#font[42]{Data}", "ep")
leg.AddEntry(total_SM_leg, "#font[42]{Uncertainty}", "f")
#leg.AddEntry(total_SM_leg, "#font[42]{Model (s + b)}", "lf")

if resolved: leg.AddEntry(WZ, "#font[42]{WZ}", "f")
else: leg.AddEntry(WZ, "#font[42]{WZ}", "f")
leg.AddEntry(WWW, "#font[42]{WWW}", "f")
if "CR3l" not in options.plot: leg.AddEntry(ssWW, "#font[42]{ssWW}", "f")
if options.merge_backgrounds:
  if NonPromptEl and NonPromptMu: leg.AddEntry(NonPrompt, "#font[42]{Non-prompt}", "f")
else:
  leg.AddEntry(NonPromptEl, "#font[42]{NonPromptEl}", "f")
  leg.AddEntry(NonPromptMu, "#font[42]{NonPromptMu}", "f")
if PhotonConversion: leg.AddEntry(PhotonConversion, "#font[42]{Photon conversion}", "f")
if ChargeFlip: leg.AddEntry(ChargeFlip, "#font[42]{Charge-flip}", "f")
if options.merge_backgrounds: leg.AddEntry(Others, "#font[42]{Other}", "f")
else:
  leg.AddEntry(VVV, "#font[42]{VVV}", "f")
  leg.AddEntry(ZZ, "#font[42]{ZZ}", "f")
  leg.AddEntry(TopX, "#font[42]{TopX}", "f")

if "excl" in options.folder and options.plotSignal:
  leg.AddEntry(mc_signal, "#font[42]{{{}}}".format(mc_signame), "f")
if options.plotTwoSignals:
  legSig = ROOT.TLegend(0.23, 0.49, 0.55, 0.73, "")
  legSig.SetFillStyle(0)
  legSig.SetFillColor(0)
  legSig.SetBorderSize(0)
  legSig.SetMargin(0.20)
  legSig.SetTextSize(0.04)
  #legSig.AddEntry(signal1, "#font[42]{#splitline{m_{H} = 300 GeV,}{f_{W} = 0, f_{WW} = 4600}}", "l")
  #legSig.AddEntry(signal2, "#font[42]{#splitline{m_{H} = 600 GeV,}{f_{W} = 1350, f_{WW} = 0}}", "l")
  legSig.AddEntry(signal1, "#font[42]{#splitline{m_{H} = 300 GeV,}{f_{W} = 0, f_{WW} = 230}}", "l")
  legSig.AddEntry(signal2, "#font[42]{#splitline{m_{H} = 600 GeV,}{f_{W} = 67.5, f_{WW} = 0}}", "l")
  legSig.Draw()
leg.Draw()
#label stuff
xx=.44 if "Boosted" in options.plot and not options.atlas_left else .20
yy=.884

ROOT.ATLAS_LABEL(xx, yy)

label_second = ROOT.TLatex()
label_second.SetNDC()
label_second.SetTextFont(42)
label_second.SetTextSize(0.04)
label_second.DrawLatex(xx+.12, yy, "Internal") #"Work in progress")

info = ROOT.TLatex()
info.SetNDC()
info.SetTextSize(0.04)
info.DrawLatex(xx, yy-.06, "#sqrt{s} = 13 TeV, 139 fb^{-1}")

if options.info:
  extra = ROOT.TLatex()
  extra.SetNDC()
  extra.SetTextFont(42)
  extra.SetTextSize(0.04)
  extra.DrawLatex(xx, yy-.12, options.info)

ROOT.gPad.RedrawAxis()

pad2.cd()
fRatioYmax = 2.
fRatioYmin = 0.

if options.doSignificance:
  print("there!")
  significance_plot = WZ.Clone()
  for i in range(significance_plot.GetNbinsX() + 1):

    if signal:
      if options.significance == "positive":
        tot_bkg = total_SM.Integral(i, significance_plot.GetNbinsX())
        tot_sig = signal.Integral(i, significance_plot.GetNbinsX())
      else:
        tot_bkg = total_SM.Integral(0, i)
        tot_sig = signal.Integral(0, i)
        print("*********** Negative ************")
      significance = ROOT.RooStats.NumberCountingUtils.BinomialExpZ(tot_sig, tot_bkg, 0.3)

      if math.isnan(significance):
        significance = 0.
      if math.isinf(significance):
        significance = 6.
      significance_plot.SetBinContent(i, significance)
      significance_plot.SetBinError(i, 0.)
      significance_plot.SetMarkerColor(signal.GetLineColor())

  significance_plot.SetMinimum(0.)
  significance_plot.SetMaximum(5.)
  significance_plot.GetXaxis().SetTitle(xlabel)
  significance_plot.GetYaxis().SetTitle("z_{N}")
  significance_plot.GetXaxis().SetTitleSize(0.12)
  significance_plot.GetYaxis().SetTitleSize(0.12)
  significance_plot.GetXaxis().SetTitleOffset(1.)
  significance_plot.GetYaxis().SetTitleOffset(0.5)
  significance_plot.GetXaxis().SetLabelSize(0.12)
  significance_plot.GetYaxis().SetLabelSize(0.12)
  significance_plot.GetYaxis().SetNdivisions(5)
  significance_plot.Draw("P")
  ROOT.gPad.RedrawAxis()

else:
  frame = WZ.Clone()
  for i in range(frame.GetNbinsX()):
    frame.SetBinContent(i, 0.)
  frame.SetMaximum(fRatioYmax)
  frame.SetMinimum(fRatioYmin)
  if "VR" in options.plot:
    frame.SetMaximum(fRatioYmax)
    frame.SetMinimum(fRatioYmin)

  frame.Draw("HIST")

  ratio.Draw("EP")
  ratio_err.Draw("F")

  frame.GetXaxis().SetTitle(xlabel)
  if options.data_over_b:
    frame.GetYaxis().SetTitle("Data/b")
  else:
    frame.GetYaxis().SetTitle("Data/Pred.")
  frame.GetXaxis().SetTitleSize(0.12)
  frame.GetYaxis().SetTitleSize(0.12)
  frame.GetXaxis().SetTitleOffset(1.1)
  frame.GetXaxis().SetLabelOffset(0.03)
  frame.GetYaxis().SetTitleOffset(0.43)
  frame.GetXaxis().SetLabelSize(0.12)
  frame.GetYaxis().SetLabelSize(0.12)
  frame.GetYaxis().SetNdivisions(505)
  frame.GetYaxis().CenterTitle();
  if "Meff" not in options.plot: frame.GetXaxis().SetRangeUser(xmin, xmax)
  pad2.SetGridy();

  frame.Draw("AXIS")
  frame.Draw("AXIG SAME")
  # ratio.Draw("EP")
  ratio_err.SetFillStyle(3004);
  ratio_err.SetMarkerColor(kBlack);
  ratio.SetMarkerSize(1.8);
  # ratio_err.Draw("a3 SAME");
  # ratio_err.Draw("F")

  mg = ROOT.TMultiGraph()
  mg.Add(ratio, "EP");
  mg.Add(ratio_err, "F");
  mg.Draw();

  ROOT.gPad.RedrawAxis()

  if "Meff" in options.plot and not "CR3l" in options.plot and not "ssWW" in options.plot:
    if "Resolved" in options.plot:
      # frame.GetXaxis().SetBinLabel(1, "0-300")
      # frame.GetXaxis().SetBinLabel(2, "300-400")
      # frame.GetXaxis().SetBinLabel(3, "400-500")
      # frame.GetXaxis().SetBinLabel(4, "500-600")
      # frame.GetXaxis().SetBinLabel(5, "600-1000")
      frame.GetXaxis().ChangeLabel(2, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(3, -1, -1, -1, -1, -1, "300")
      frame.GetXaxis().ChangeLabel(4, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(5, -1, -1, -1, -1, -1, "400")
      frame.GetXaxis().ChangeLabel(6, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(7, -1, -1, -1, -1, -1, "500")
      frame.GetXaxis().ChangeLabel(8, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(9, -1, -1, -1, -1, -1, "600")
      frame.GetXaxis().ChangeLabel(10, -1, -1, -1, -1, -1, " ")
      frame.GetXaxis().ChangeLabel(11, -1, -1, -1, -1, -1, "1000")
    else:
      print("bla")
      frame.GetXaxis().ChangeLabel(2,-1,-1,-1,-1,-1," ")
      frame.GetXaxis().ChangeLabel(3,-1,-1,-1,-1,-1,"600")
      frame.GetXaxis().ChangeLabel(4,-1,-1,-1,-1,-1," ")
      frame.GetXaxis().ChangeLabel(5,-1,-1,-1,-1,-1,"800")
      frame.GetXaxis().ChangeLabel(6,-1,-1,-1,-1,-1," ")
      frame.GetXaxis().ChangeLabel(7,-1,-1,-1,-1,-1,"2000")
      # frame.GetXaxis().SetBinLabel(1, "0-600")
      # frame.GetXaxis().SetBinLabel(2, "600-800")
      # frame.GetXaxis().SetBinLabel(3, "800-2000")
    # frame.GetXaxis().SetLabelSize(0.16)

# Add arrows when the ratio is beyond the limits of the ratio plot
for i in range(0, ratio.GetN()):
   val = ratio.GetY()[i]
   #print(ratio.GetY()[i])
   isUp=0
   arrow = TArrow()
   arrow.SetFillColor(10)
   arrow.SetFillStyle(1001)
   arrow.SetLineColor(kBlue-7)
   arrow.SetLineWidth(2)
   arrow.SetAngle(40)
   if val<=fRatioYmin and ratio.GetX()[i]<=xmax and ratio.GetX()[i]>=xmin: 
	isUp=-1
	arrow.DrawArrow(ratio.GetX()[i],fRatioYmin+0.05*(fRatioYmax-fRatioYmin), ratio.GetX()[i],fRatioYmin+0.03,0.040/(pad2.GetWw()/596.),"|>")
   elif val>=fRatioYmax and ratio.GetX()[i]<=xmax and ratio.GetX()[i]>=xmin: 
	isUp=1
	arrow.DrawArrow(ratio.GetX()[i],fRatioYmax-0.05*(fRatioYmax-fRatioYmin), ratio.GetX()[i],fRatioYmax-0.03,0.040/(pad2.GetWw()/596.),"|>")

#out_dir = os.path.join('plots_ghh/' + options.folder[options.folder.find('results')+8:])
out_dir = os.path.join(options.folder+'/plots_ghh_update3/')
inf_add = ''
if options.setLogy: inf_add = '_log'
if options.plotTwoSignals: inf_add += '_2prefitsigs'
if not os.path.exists(out_dir):
  os.makedirs(out_dir)
if options.doBefore == True:
  c.SaveAs(
    out_dir + options.plot + '_beforeFit' + inf_add + '.pdf')
  c.SaveAs(
    out_dir + options.plot + '_beforeFit' + inf_add + '.png')
  c.SaveAs(
    out_dir + options.plot + '_beforeFit' + inf_add + '.eps')
else:
  c.SaveAs(
    out_dir + options.plot + '_afterFit' + inf_add + '.pdf')
  c.SaveAs(
    out_dir + options.plot + '_afterFit' + inf_add + '.png')
  c.SaveAs(
    out_dir + options.plot + '_afterFit' + inf_add + '.eps')
  c.Clear()
c.Close()




