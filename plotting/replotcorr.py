import ROOT
import os
import argparse
# import numpy as np
from ROOT import *

parser = argparse.ArgumentParser()
parser.add_argument("--threshold",type=float,default=0.01,help="corellation threshold for removing n.p.")
parser.add_argument("--reduce",action="store_true",default=False,help="remove n.p. that are uncorrelated from all others, up to threshold value")

opt = parser.parse_args()


ROOT.gROOT.SetBatch(True)
dirr = "/afs/cern.ch/work/g/grbarbou/HeavyHiggs/HFUpdated/results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/"
f = ROOT.TFile(dirr+'corrMatrix.root')


def plot_original(f):
    orig_c = f.Get("c_corrMatrix_RooExpandedFitResult_afterFit")
    orig_c.Draw()
    orig_c.Print(dirr+"replotcorr.png")

def remove_aplha_prefix(h_corr):
    nbins = h_corr.GetYaxis().GetNbins()
    for i in range(nbins):
        title = h_corr.GetYaxis().GetBinLabel(i + 1)
        title2 = h_corr.GetXaxis().GetBinLabel(i + 1)
        if "alpha" in title:
            h_corr.GetYaxis().SetBinLabel(i + 1, title[6:])
        if "alpha" in title2:
            h_corr.GetXaxis().SetBinLabel(i + 1, title2[6:])
        if "mu_" in title or "gamma_" in title:
            h_corr.GetYaxis().SetBinLabel(i + 1, "#"+title[:])
        if "mu_" in title2 or "gamma_" in title2:
            h_corr.GetXaxis().SetBinLabel(i + 1, "#"+title2[:])


def reformat(h_corr, label_size, marker_size, lm, bm, ww, hh):

    myC = ROOT.TCanvas("reformat_corr", "reformat_corr", ww, hh)
    myC.cd()
    h_corr.GetXaxis().SetLabelSize(label_size)
    h_corr.GetYaxis().SetLabelSize(label_size)
    h_corr.GetXaxis().LabelsOption("v")
    h_corr.SetMarkerSize(marker_size)
    h_corr.SetMarkerColor(kWhite)
    gPad.SetLeftMargin(lm)
    gPad.SetRightMargin(0.13)
    gPad.SetBottomMargin(bm)
    gStyle.SetPalette(105)
    gStyle.SetPaintTextFormat("4.2f")
    gStyle.SetOptStat(00000000)
    gStyle.SetOptTitle(0)
    remove_aplha_prefix(h_corr)

    h_corr.Draw("colz")
    if opt.reduce:
        h_corr.Draw("textsame")
        myC.Print(dirr + "reduced_corr.png")
        myC.Print(dirr + "reduced_corr_excl.pdf")
    else:
        myC.Print(dirr + "reformatcorr.png")

def reduce_matrix(h_corr, threshold=0.1):
    '''copy from utils
    remove any n.p. with correlations (off-diagonal vals) below a certain threshold
    returns h_corr_reduced'''
    nxbins = h_corr.GetNbinsX()
    keep_idx = []
    index_x = 0
    for ix in range(1,nxbins+1):
        for iy in range(1,nxbins+1):
            if ix == (nxbins+1 - iy): continue  # because root flips x and y directions
            if abs(h_corr.GetBinContent(ix, iy)) >= threshold:
                keep_idx.append(ix)
                break
    rm_idx = [i for i in range(1,nxbins+1) if i not in keep_idx]
    print("n.p. found with correlations all below threshold, removing {} n.p.".format(len(rm_idx)))
    newSize = nxbins - len(rm_idx)
    h_corr_reduced = TH2F("h_corr_reduced","Reduced Correlation Matrix",newSize,0,newSize,newSize,0,newSize)
    for ix in range(1,nxbins+1):
        index_y = 0
        filled = False
        for iy in range(1,nxbins+1):
            if not ((ix in rm_idx) or ((nxbins+1-iy) in rm_idx)):
                h_corr_reduced.Fill(index_x, index_y, h_corr.GetBinContent(ix, iy))  # fill takes index_x as bin value
                index_y += 1  # SetBinLabel takes index_x as index, so need to increment
                if index_x == 0:  # only set the bin label first time around
                    h_corr_reduced.GetYaxis().SetBinLabel(index_y, h_corr.GetYaxis().GetBinLabel(iy))
                filled = True
        if filled:
            index_x += 1
            h_corr_reduced.GetXaxis().SetBinLabel(index_x, h_corr.GetXaxis().GetBinLabel(ix))

    return h_corr_reduced


########################################################################################################
# Main
# plot_original(f)
# Reformatting
h_corr = f.Get("h_corr_RooExpandedFitResult_afterFit")

if opt.reduce:
    h_red = reduce_matrix(h_corr, threshold=opt.threshold)
    label_size = 0.026 # 0.016
    marker_size = .7 #.5
    lm = 0.28 #0.22
    bm = 0.30 #0.24
    ww = 2400
    hh = 2100
    reformat(h_red, label_size, marker_size, lm , bm, ww, hh)
else:
    label_size = 0.01
    marker_size = .01
    lm = 0.18
    bm = 0.18
    ww = 12000
    hh = 10000
    reformat(h_corr, label_size, marker_size, lm , bm, ww, hh)

########################################################################################################



