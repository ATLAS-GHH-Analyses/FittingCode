import ROOT
import os
import argparse
import numpy as np
from ROOT import *
import re

parser = argparse.ArgumentParser()
parser.add_argument("--multiple",action="store_true",default=False,help="split pulls over multiple plots, incompatible with --reduce, modify nps dictionary in script")
parser.add_argument("--reduce",action="store_true",default=False,help="keep only top n systematic pulls")
parser.add_argument("--no_gammas",action="store_true",default=False,help="Remove gamma n.p. from pull plot")
parser.add_argument("--nps",type=dict,default=None,help="manually define number of nps per canvas for --multiple plotting")
parser.add_argument("--n_plots",type=int,default=4,help="define n of canvasses in --multiple plotting")
parser.add_argument("--no_blue",action="store_true",default=False,help="Lumi and gamma_* will show up as blue")
parser.add_argument("--group",type=str,default=None,help="Plot n.p. starting with a given string. For example: '--group FT_EFF_' plots only b-tagging n.p.")
parser.add_argument("--extension",type=str,default="pdf",help="output plot file type: [pdf,png,eps,...]")
parser.add_argument("--lower_limit",type=float,default=-3.2,help="define n of canvasses in --multiple plotting")
parser.add_argument("--output",type=str,default=None,help="manually set the output filename and directory")
parser.add_argument("--dirr",type=str,default="GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/",help="directory of results for input")

opt = parser.parse_args()
if opt.reduce and opt.multiple: raise NotImplementedError("cannot reduce and plot on multiple canvasses")
if opt.group is not None:
    if opt.reduce or opt.multiple or opt.no_gammas:
        raise NotImplementedError("group will by default remove gammas, cannot be used with reduce or multiple")

ROOT.gROOT.SetBatch(True)
# dirr = "/afs/cern.ch/work/g/grbarbou/HeavyHiggs/HFUpdated/results/GHH_excl_Inclusive_sys_r0402_new_GHH600fW1350fWW0/"
# dirr = "/afs/cern.ch/work/g/grbarbou/HeavyHiggs/HistFitter/results/GHH_bkg_Inclusive_sys_r0402_new_GHH600fW2130fWW0/"
# dirr = "/afs/cern.ch/work/g/grbarbou/HeavyHiggs/HistFitter/yuesresult/GHH_excl_Inclusive_sys_r0402_new_2WZNF_oldssWW_GHH600fW1350fWW0/"
dirr="/afs/cern.ch/work/g/grbarbou/HeavyHiggs/HFUpdated/results/"+opt.dirr
f = ROOT.TFile(dirr+'testSavePrims.root')#

def deduce_nps(g, n_plots):
    nbins = g.GetHistogram().GetYaxis().GetNbins()
    x = nbins // n_plots
    remainder = nbins % n_plots
    nps={}
    for i in range(n_plots):
        nps[i] = x+1 if i<remainder else x
    print(nps)
    return nps

def plot_original(f):
    orig_c = f.Get("fit_parameters")
    orig_c.Draw()
    orig_c.Print(dirr+"replotpulls."+opt.extension)

def standard_reformat(g,axis1):
    g.GetHistogram().GetYaxis().SetLabelSize(0.016)
    g.GetHistogram().GetXaxis().SetLabelSize(0.015)
    g.GetHistogram().GetXaxis().SetLabelOffset(0)
    g.GetHistogram().GetXaxis().SetTitleOffset(0.5)
    remove_alpha_prefix(g)
    axis1.SetLabelSize(0.015)
    axis1.SetLabelOffset(-0.01)
    axis1.SetTitleSize(0.02)
    axis1.SetTitleOffset(.5)

def reduced_reformat(g,axis1):
    g.GetHistogram().GetYaxis().SetLabelSize(0.023)#.023
    g.GetHistogram().GetXaxis().SetLabelSize(0.015)
    g.GetHistogram().GetXaxis().SetLabelOffset(0)
    g.GetHistogram().GetXaxis().SetTitleOffset(0.5)
    remove_alpha_prefix(g)
    axis1.SetLabelSize(0.015)
    axis1.SetLabelOffset(-0.01)
    axis1.SetTitleSize(0.02)
    axis1.SetTitleOffset(.9)

# Plot the reformatted pulls on a single canvas
def plot_reformat(g,_g,axis1,yMCstatBand,lMCstat,dirr,count):
    standard_reformat(g,axis1)
    myC = ROOT.TCanvas("reformat_fit_params", "reformat_fit_params",3200,6400)
    myC.cd()
    p_np = ROOT.TPad("p_np","p_np",0,1,1,0.,0)
    p_np.SetLeftMargin(0.35)
    p_np.Draw()
    p_np.cd()
    g.GetXaxis().SetLimits(opt.lower_limit,3)
    g.Draw("AP")
    myC.Update()
    # axis1.Draw("same")
    sig2band = get_2band(count)
    yMCstatBand.SetFillColor(kGreen)
    yMCstatBand.SetFillStyle(1001)
    sig2band.Draw("Fsame")
    yMCstatBand.Draw("Fsame")
    lMCstat.Draw("same")
    axis2 = get_axis(p_np)
    axis2.Draw("same")
    draw_values(g, size=0.012)
    g.Draw("Psame")
    _g.Draw("Psame")
    gPad.RedrawAxis()
    myC.Print(dirr+"reformatpulls."+opt.extension)

# Plot
def plot_nogammas(g,_g,axis1,yMCstatBand,lMCstat,dirr,count):
    standard_reformat(g,axis1)
    myC = ROOT.TCanvas("reformat_fit_params", "reformat_fit_params",3200,6400)
    myC.cd()
    p_np = ROOT.TPad("p_np","p_np",0,1,1,0.,0)
    p_np.SetLeftMargin(0.35)
    p_np.Draw()
    p_np.cd()
    g.GetXaxis().SetLimits(opt.lower_limit,3)
    g.Draw("AP")
    myC.Update()
    sig2band = get_2band(count)
    yMCstatBand.SetFillColor(kGreen)
    yMCstatBand.SetFillStyle(1001)
    sig2band.Draw("Fsame")
    yMCstatBand.Draw("Fsame")
    lMCstat.Draw("same")
    axis2 = get_axis(p_np)
    axis2.Draw("same")
    draw_values(g, size=0.012)
    g.Draw("Psame")
    _g.Draw("Psame")
    gPad.RedrawAxis()
    myC.Print(dirr+"reformatpulls_nogammas."+opt.extension)

def get_2band(count):
    sig2band = TGraph()
    sig2band.SetPoint(0, -2, 0)
    sig2band.SetPoint(1, -2, count + 2)
    sig2band.SetPoint(2, 2, count + 2)
    sig2band.SetPoint(3, 2, 0)
    sig2band.SetFillColor(kYellow)
    sig2band.SetFillStyle(1001)
    sig2band.Draw("Fsame")
    return sig2band

def get_axis(p_np):
    axis2 = TGaxis(p_np.GetUxmin(), p_np.GetUymax(), p_np.GetUxmax(), p_np.GetUymax(), p_np.GetUxmin() + 1,
                   p_np.GetUxmax() + 1, 510, "-L")
    axis2.SetLineColor(kBlue)
    axis2.SetLabelColor(kBlue)
    if opt.no_blue: axis2.SetTitle("#mu")
    else: axis2.SetTitle("#gamma / #mu")
    axis2.SetTitleColor(kBlue)
    axis2.SetLabelSize(0.015)
    axis2.SetLabelOffset(-0.01)
    axis2.SetTitleSize(0.02)
    axis2.SetTitleOffset(.9)
    return axis2

def draw_values(g, size=0.015):
    tmpText = ROOT.TLatex()
    color=kBlack
    alpha=1
    tmpText.SetTextSize(size)
    tmpText.SetTextColorAlpha(color, alpha)
    x, y, n = g.GetX(), g.GetY(), g.GetN()
    for i in range(n):
        parVal = g.GetPointX(i)
        parLowError=g.GetErrorXlow(i)
        parHighErrors=g.GetErrorXhigh(i)
        if opt.multiple:
            if i+1 < g.GetYaxis().GetFirst() or i+1 > g.GetYaxis().GetLast():
                continue
        if parLowError != parHighErrors:
            tmpText.DrawLatex(-3,y[i],"%.2f"%parVal+"\pm^{{%.2f}}_{{%.2f}}"%(parLowError,parHighErrors))
        else:
            if "#mu" in g.GetHistogram().GetYaxis().GetBinLabel(i+1):
                parVal+=1
            tmpText.DrawLatex(-3,y[i]-.3,"%.2f"%parVal+" \pm %.2f"%parLowError)

# Reduce to top 20 alpha pulls and plot on single canvas
def plot_reduced(g,_g,axis1,yMCstatBand,lMCstat,dirr,count):
    reduced_reformat(g,axis1)
    myC = ROOT.TCanvas("reformat_fit_params", "reformat_fit_params",1760,1400) #1600
    myC.cd()
    p_np = ROOT.TPad("p_np","p_np",0,1,1,0.,0)
    p_np.SetLeftMargin(0.36) #.3
    p_np.Draw()
    p_np.cd()
    g.GetXaxis().SetLimits(opt.lower_limit,3) #4.2
    g.Draw("AP")
    myC.Update()
    # axis1.SetY1(p_np.GetUymax())
    # axis1.SetY2(p_np.GetUymax())
    # axis1.SetX1(p_np.GetUxmin())
    # axis1.SetX2(p_np.GetUxmax())
    sig2band = get_2band(count)
    yMCstatBand.SetFillColor(kGreen)
    yMCstatBand.SetFillStyle(1001)
    sig2band.Draw("Fsame")
    yMCstatBand.Draw("Fsame")
    lMCstat.Draw("same")
    axis2 = get_axis(p_np)
    axis2.Draw("same")
    draw_values(g)
    # axis1.Draw("same")
    g.Draw("Psame")
    _g.Draw("Psame")
    gPad.RedrawAxis()
    myC.Print(dirr+"reduced_pulls."+opt.extension)

def plot_group(g,_g,axis1,yMCstatBand,lMCstat,dirr,count):
    reduced_reformat(g,axis1)
    myC = ROOT.TCanvas("reformat_fit_params", "reformat_fit_params",1760,1400) #1600
    myC.cd()
    p_np = ROOT.TPad("p_np","p_np",0,1,1,0.,0)
    p_np.SetLeftMargin(0.4) #.3
    p_np.Draw()
    p_np.cd()
    g.GetXaxis().SetLimits(opt.lower_limit,3)
    g.Draw("AP")
    myC.Update()
    axis1.SetY1(p_np.GetUymax())
    axis1.SetY2(p_np.GetUymax())
    # axis1.Draw("same")
    sig2band = get_2band(count)
    yMCstatBand.SetFillColor(kGreen)
    yMCstatBand.SetFillStyle(1001)
    sig2band.Draw("Fsame")
    yMCstatBand.Draw("Fsame")
    lMCstat.Draw("same")
    if "#mu" in opt.group:
        axis2 = get_axis(p_np)
        axis2.Draw("same")
    draw_values(g)
    g.Draw("Psame")
    if "#mu" in opt.group: _g.Draw("Psame")
    gPad.RedrawAxis()
    if opt.output is not None:
        myC.Print("pull_plots/"+opt.output+"."+opt.extension)
    else:
        myC.Print(dirr+opt.group+"_pulls."+opt.extension)


def remove_alpha_prefix(g):
    nbins = g.GetHistogram().GetYaxis().GetNbins()
    print("The number of n.p. is",nbins)
    for i in range(nbins):
        title = g.GetHistogram().GetYaxis().GetBinLabel(i + 1)
        if "#alpha" in title:
            g.GetHistogram().GetYaxis().SetBinLabel(i + 1, title[7:])


# Split the reformatted pull plot onto multiple canvasses
def plot_multiple(nps, g, _g, axis1, yMCstatBand, lMCstat, dirr):
    standard_reformat(g,axis1)
    g.GetXaxis().SetLimits(opt.lower_limit,3)
    idx = 0
    for grp in nps:
        if grp == 0:
            ru_start = -1  # special case if 0th grouping, extend range user to -1
        else:
            ru_start = 2 * idx + 1
        ru_end = 2 * (idx + nps[grp]) + 1
        l_start = 2 * idx + 1
        l_end = ru_end
        r_start = idx + 1
        r_end = idx + nps[grp]

        subC = ROOT.TCanvas("subCanvas", "subCanvas", 2400, 2400)
        subC.cd()
        p_sub = ROOT.TPad("p_sub", "p_sub", 0, 1, 1, 0., 0)
        p_sub.SetLeftMargin(0.3)
        p_sub.Draw()
        p_sub.cd()
        g.GetHistogram().GetYaxis().SetRange(r_start, r_end)
        g.GetHistogram().GetYaxis().SetRangeUser(ru_start, ru_end)
        yMCstatBand.GetHistogram().GetYaxis().SetRangeUser(ru_start, ru_end)
        lMCstat_new = TLine(0, l_start, 0, l_end)
        lMCstat_new.SetLineStyle(8)

        g.Draw("AP")
        subC.Update()

        _g.Draw("Psame")
        count = 2*(g.GetHistogram().GetYaxis().GetNbins() + 1)
        sig2band = get_2band(count)
        yMCstatBand.SetFillColor(kGreen)
        yMCstatBand.SetFillStyle(1001)
        sig2band.Draw("Fsame")
        yMCstatBand.Draw("Fsame")
        lMCstat_new.Draw("same")
        if grp == list(nps.keys())[-1]:
            axis2 = get_axis(p_sub)
            axis2.Draw("same")
        draw_values(g) #should take ru_end and ru_start
        g.Draw("Psame")
        _g.Draw("Psame")
        gPad.RedrawAxis()
        subC.Print(dirr + "pulls_subcanvas{}.{}".format(grp,opt.extension))
        idx += nps[grp]

def indexes_to_keep(g, n=20):
    """
    Returns list of indices to keep, includes all mu and gamma np, and top N alpha np
    :param g: the TGraph
    :param n: keep top n systematic pulls
    """
    n_no_blue=0
    nbins = g.GetHistogram().GetYaxis().GetNbins()
    values=[]
    nps=[]
    keep_idx=[]
    for i in range(nbins):
        title = g.GetHistogram().GetYaxis().GetBinLabel(i + 1)
        if ("#gamma") in title or ("Lumi") in title:
            n_no_blue+=1
        if ("#mu" in title) or ("#gamma") in title or ("Lumi") in title:
            if opt.no_gammas and ("#gamma") in title:
                continue
            keep_idx.append(i)
            continue
        values.append(abs(g.GetPointX(i)))
        nps.append(i)
    inds = np.array(values).argsort()
    ranking = np.array(nps)[inds]
    # print(ranking)
    top_n = ranking[-n:].tolist() # gives the largest n, because ranking goes smallest to largest
    if n == 0: #dont rank if we keep all n.p.
        top_n = nps
    return top_n+keep_idx, n_no_blue

def group_indexes(g, group, n=0):
    """
    Returns list of indices to keep, includes all mu and gamma np, and top N alpha np
    :param g: the TGraph
    :param n: keep top n systematic pulls
    """
    nbins = g.GetHistogram().GetYaxis().GetNbins()
    values=[]
    nps=[]
    keep_idx=[]
    regex = re.compile(group)
    for i in range(nbins):
        title = g.GetHistogram().GetYaxis().GetBinLabel(i + 1)
        # if group in title:
        if regex.search(title):
            values.append(abs(g.GetPointX(i)))
            nps.append(i)

    if n == 0: #dont rank if we keep all n.p.
        return nps
    else:
        inds = np.array(values).argsort()
        ranking = np.array(nps)[inds]
        top_n = ranking[-n:].tolist()
        return top_n

def extract_values_to_keep(g, n=20, group=None):
    if group is not None:
        keep_idx = group_indexes(g, group)
    else:
        keep_idx, n_no_blue = indexes_to_keep(g, n)
    N = (2. * np.arange(1, len(keep_idx) + 1))
    errN = np.zeros(len(keep_idx))
    print(keep_idx)
    parNames = []
    parVals = []
    parLowErrors = []
    parHighErrors = []
    for i in keep_idx:
        parNames.append(g.GetHistogram().GetYaxis().GetBinLabel(i + 1))
        title = g.GetHistogram().GetYaxis().GetBinLabel(i + 1)
        if opt.no_blue and ("Lumi" in title or "gamma" in title):
            parVals.append(g.GetPointX(i)+1)
            parLowErrors.append(g.GetErrorXlow(i))
            parHighErrors.append(g.GetErrorXhigh(i))
        else:
            parVals.append(g.GetPointX(i))
            parLowErrors.append(g.GetErrorXlow(i))
            parHighErrors.append(g.GetErrorXhigh(i))
    parVals = np.array(parVals)
    parLowErrors = np.array(parLowErrors)
    parHighErrors = np.array(parHighErrors)
    #ToDo: Greg the -4 and -3 are hard-coded here (mu_SIG, mu_ssWW, mu_WZ, Lumi), change to calc automatically
    if n==0:
        #bit of trickery in the event of --no_gammas being called without --reduce
        idx = -4
        if opt.no_blue: idx+=1
    elif opt.group:
        if "#mu" in opt.group:
            idx=-4 if "excl" in dirr else -3
        else:
            idx=0
    else:
        idx = n+n_no_blue if opt.no_blue else n
    _N = N[idx:]
    _parVals = parVals[idx:]
    print(parVals)
    return  N, errN, parNames, parVals, parLowErrors, parHighErrors, _N, _parVals


def reduce_g(N, errN, parNames, parVals, parLowErrors, parHighErrors):
    """
    Reduces g by keeping only top n alpha params
    """
    red_g = TGraphAsymmErrors(len(N), parVals, N, parLowErrors, parHighErrors, errN, errN)
    count = int(N[-1]) # i believe this is correct, so count/2 is total number of bins
    red_g.GetHistogram().GetYaxis().Set(int(count / 2), 1, count + 1)
    for i in range(len(N)):
        red_g.GetHistogram().GetYaxis().SetBinLabel(i+1, parNames[i])
    red_g.GetHistogram().GetYaxis().SetRangeUser(1, count + 1)
    red_g.GetHistogram().GetYaxis().SetTickLength(0.)
    red_g.GetHistogram().GetYaxis().SetLabelSize(0.035)
    red_g.GetHistogram().GetXaxis().SetRangeUser(-1.5, 1.5)
    red_g.GetHistogram().GetXaxis().SetTitle("#alpha")
    red_g.SetName("fit_results")
    red_g.SetTitle("")
    red_g.SetMarkerStyle(8)
    red_g.SetMarkerSize(2)
    red_g.SetLineWidth(1)

    return red_g


def reduce__g(_N, _paramVals):
    red__g = TGraph(len(_N),_paramVals,_N)
    red__g.SetName("_fit_results")
    red__g.SetTitle("")
    red__g.SetMarkerStyle(8)
    red__g.SetMarkerColor(kBlue)
    red__g.SetMarkerSize(2)
    return red__g

#######################################################################################################
# Main
g=f.Get("fit_results")
_g=f.Get("_fit_results")
axis1 = f.Get("axis1")
yMCstatBand = f.Get("yMCstatBand")
lMCstat = f.Get("lMCstat")

if opt.group is not None:
    N, errN, parNames, parVals, parLowErrors, parHighErrors, _N, _parVals = extract_values_to_keep(g, group=opt.group)
    red_g = reduce_g(N, errN, parNames, parVals, parLowErrors, parHighErrors)
    red__g = reduce__g(_N, _parVals)
    count = int(N[-1]) # i believe this is correct, so count/2 is total number of bins
    lMCstat_new = TLine(0, 1, 0, int(N[-1]) + 1)
    lMCstat_new.SetLineStyle(8)
    plot_group(red_g, red__g, axis1, yMCstatBand, lMCstat_new, dirr, count)
elif opt.reduce:
    N, errN, parNames, parVals, parLowErrors, parHighErrors, _N, _parVals = extract_values_to_keep(g, n=20)
    red_g = reduce_g(N, errN, parNames, parVals, parLowErrors, parHighErrors)
    red__g = reduce__g(_N, _parVals)
    count = int(N[-1]) # i believe this is correct, so count/2 is total number of bins
    lMCstat_new = TLine(0, 1, 0, int(N[-1])+1)
    lMCstat_new.SetLineStyle(8)
    plot_reduced(red_g, red__g, axis1, yMCstatBand, lMCstat_new, dirr, count)
elif opt.no_gammas:
    N, errN, parNames, parVals, parLowErrors, parHighErrors, _N, _parVals = extract_values_to_keep(g, n=0)
    red_g = reduce_g(N, errN, parNames, parVals, parLowErrors, parHighErrors)
    red__g = reduce__g(_N, _parVals)
    lMCstat_new = TLine(0, 1, 0, int(N[-1]) + 1)
    lMCstat_new.SetLineStyle(8)
    if opt.multiple:
        nps = deduce_nps(red_g, opt.n_plots) if opt.nps == None else opt.nps
        plot_multiple(nps, red_g, red__g, axis1, yMCstatBand, lMCstat_new, dirr)
    else:
        count = int(N[-1])
        plot_nogammas(red_g, red__g, axis1, yMCstatBand, lMCstat_new, dirr,count)
else:
    if opt.multiple:
        nps = deduce_nps(g, opt.n_plots) if opt.nps == None else opt.nps
        plot_multiple(nps, g, _g, axis1, yMCstatBand, lMCstat, dirr)
    else:
        if opt.no_blue: raise NotImplementedError("Greg: Is this requested? If so I can implement")
        # nbins = g.GetHistogram().GetYaxis().GetNbins()
        # N = (2. * np.arange(1, nbins + 1))
        count = 2*(g.GetHistogram().GetYaxis().GetNbins() + 1)
        plot_reformat(g, _g, axis1, yMCstatBand, lMCstat, dirr, count)




########################################################################################################

########################################################################################################


