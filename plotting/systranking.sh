signal='GHH600fW2130fWW0'

SystRankingPlotMP.py  --noFit -w results/GHH_Ranking_Inclusive_sys_r0402_new_$signal/Ranking_combined_NormalMeasurement_model_afterFit.root \
 -f SRSS2lBoostedInc_Meff,SRSS2lResolvedInc_Meff,ssWWResolvedInc_Meff,CR3lBoostedInc_Meff,CR3lResolvedInc_Meff \
 -p "mu_SIG" --lumi 139 --maxCores -1 --sysOnly -o "plotsMP_r04_ranking_$signal/" --atlas "Internal" --noMu
