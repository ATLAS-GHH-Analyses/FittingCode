import os
import pandas as pd

def read_file(filename):
  with open(filename) as a:
    cont = a.readlines()
    cont = [x.strip() for x in cont]
  return cont

lines = read_file("limits_v1.txt")

results={}
ctr=-1
for i,line in enumerate(lines):
  if line == "--":
    ctr+=1
    continue
  elif lines[i-1] == "--":
    values=[]
    partial_line = line[19:]
    sig = partial_line[:partial_line.find("/")]
    # print(partial_line[:partial_line.find("/")])
    ind=line.find("is:")
    end=line.find(" +/- ")
    val = line[ind+4:end]
  else:
    val = line[-7:]
  try:
    values.append(float(val))
  except:
    val = val[val.find(" ")+1:]
    values.append(float(val))
  # print(line[-7:])
  if lines[i+1] == "--":
    results[sig] = values

table_of_results = pd.DataFrame.from_dict(results, orient='index')
table_of_results.rename(columns={0: 'observed', 1: 'median', 2: '-1 sigma', 3: '+1 sigma', 4: '-2 sigma', 5: '+2 sigma'}, inplace=True)
# print(table_of_results)
# print(ctr)

all_signals = set(read_file("samplelist_4thSig.txt"))
found_signals = set(results.keys())
missing_signals = all_signals - found_signals
print("missing signals: ")
print(missing_signals)

# print((table_of_results[table_of_results['median'] < 1.]).shape)

table_of_results.to_pickle("signal_limits.pkl")
table_of_results.to_csv("signal_limits.csv")
