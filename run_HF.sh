channel_label=$2
type=$3
m=$4
#variable=""

#if [[ $channel_label == TriLepton ]]
#then 
#	variable=mtlep
#else
#	variable=htmet
#fi

label=${type}${channel_label}
signal=$1 # 'GHH600fW2130fWW0'


########################## New signal fits

HistFitter.py -w -f -l -F excl -c "mode='excl_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -w -c "mode='excl_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -p -c "mode='excl_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py

#HistFitter.py -w -f -l -F excl -c "mode='toys_excl_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py

########################## Pull and Ranking Plots

#HistFitter.py -w -f -F bkg -D "before,after,corrMatrix" -c "mode='bkg_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -w -f -F excl -D "before,after,corrMatrix" -c "mode='excl_Inclusive_sys_r0402_new'; suffix='$signal';"  FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -f -F bkg -D "before,after,corrMatrix" -c "mode='bkg_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -f -F excl -D "before,after,corrMatrix" -c "mode='excl_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py

#
#HistFitter.py -w -f -F bkg -D "before,after,corrMatrix" -c "mode='bkg_Inclusive_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -w -f -F excl -D "before,after,corrMatrix" -c "mode='excl_Inclusive_r0402_new'; suffix='$signal';"  FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -f -F bkg -D "before,after,corrMatrix" -c "mode='bkg_Inclusive_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -f -F excl -D "before,after,corrMatrix" -c "mode='excl_Inclusive_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py

#HistFitter.py -w -f -F bkg -D "before,after,corrMatrix" -c "mode='bkg_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -w -f -F excl -D "before,after,corrMatrix" -c "mode='excl_Inclusive_sys_r0402_new'; suffix='$signal';" FittingCode/systematics/GHH_2WZNF.py



#HistFitter.py -w -f -F excl -c "mode='Ranking_Inclusive_sys_r0402_new'; suffix='$signal';"  FittingCode/systematics/GHH_2WZNF.py
#
#mkdir  plotsMP_r04_ranking_$signal
#
#SystRankingPlotMP.py  -w results/GHH_Ranking_Inclusive_sys_r0402_new_$signal/Ranking_combined_NormalMeasurement_model_afterFit.root \
# -f SRSS2lBoostedInc_Meff,SRSS2lResolvedInc_Meff,ssWWResolvedInc_Meff,CR3lBoostedInc_Meff,CR3lResolvedInc_Meff \
# -p "mu_SIG" --lumi 139 --maxCores -1 --sysOnly -o "plotsMP_r04_ranking_$signal/" --atlas "Work in progress"

########################## Pull and Ranking Plots Asimov data

#HistFitter.py -w -f -a -F bkg -D "corrMatrix" -c "mode='bkgAsimov_Inclusive_sys_r0402_new'; suffix='GHH600fWm790fWW0';" FittingCode/systematics/GHH_2WZNF.py
#HistFitter.py -w -f -a -F excl -c "mode='exclAsimov_Inclusive_sys_r0402_new'; suffix='GHH600fWm790fWW0';" FittingCode/systematics/GHH_2WZNF.py

#HistFitter.py -w -f -a -F excl -c "mode='exclAsimov_Inclusive_sys_r0402_new'; suffix='GHH600fWm790fWW0';"  FittingCode/systematics/GHH_2WZNF.py
#
#mkdir  plotsMP_exclAsimov
#
#SystRankingPlotMP.py  -w results/GHH_exclAsimov_Inclusive_sys_r0402_new_GHH600fWm790fWW0/Exclusion_combined_NormalMeasurement_model_afterFit_asimovData.root
#-f SRSS2lBoostedInc_Meff,SRSS2lResolvedInc_Meff,ssWWResolvedInc_Meff,CR3lBoostedInc_Meff,CR3lResolvedInc_Meff
#-p "mu_SIG"
#--lumi 139
#--maxCores -1
#--dataSet "asimovData"
#--sysOnly -o "plotsMP_exclAsimov/"
