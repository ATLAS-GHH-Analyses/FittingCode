#region=VR
for var in Lep1Pt Lep2Pt
#for var in Lep1Pt Lep2Pt fatjet1Pt jet1Pt jet2Pt mll MET
do
  #rm -f decorrelate_input_vars_${var}.C
  #rm -f decorrelate_${var}.root
  #cp decorrelate_input_vars.C decorrelate_input_vars_${var}.C
  #sed -i "s/decorrelate_input_vars/decorrelate_input_vars_${var}/g" decorrelate_input_vars_${var}.C
  rm -f out.${var}.log
  root -l -b -q decorrelate_input_vars_${var}.C"(\"${var}\")" > out.${var}.log 2>&1 &
done
