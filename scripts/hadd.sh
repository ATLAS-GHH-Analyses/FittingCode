#!/bin/bash

path1="/home/storage3/xuyue/atlas/GHH/ntuple-GHH-r04-02_sys/mc16a"
echo $Path1
path2="/home/storage3/xuyue/atlas/GHH/ntuple-GHH-r04-02_sys/mc16d"
echo $Path2
path3="/home/storage3/xuyue/atlas/GHH/ntuple-GHH-r04-02_sys/mc16e"
echo $Path3
for file in data GHH300X  GHH600X  GHH900X  singletops  singletopWt  ttbar  ttZ  VVV WH WWW ZZ GHH300Y GHH600Y singletopt ttW tz Wgamma Wjets Zgamma Zjets GHH3f600f0 GHH3fm600f0 GHH3f350f2100 GHH3fm350f2100 GHH3fm350fm2100 GHH3f350fm2100 GHH3f0f3000 GHH3f0fm3000 GHH6f650f0 GHH6fm650f0 GHH6f400f2400 GHH6fm400f2400 GHH6fm400fm2400 GHH6f400fm2400 GHH6f0f3500 GHH6f0fm3500 GHH9f800f0 GHH9fm800f0 GHH9f600f3600 GHH9fm600f3600 GHH9fm600fm3600 GHH9f600fm3600 GHH9f0f5000 GHH9f0fm5000  WZAlternative ssWWAlternative ttH oldssWW
do
rm -f ${file}.root
hadd ${file}.root $path1/$file/*root $path2/$file/*root $path3/$file/*root
done 
