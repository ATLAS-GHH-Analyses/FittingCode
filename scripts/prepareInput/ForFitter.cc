#include "TROOT.h"
#include "TLorentzVector.h"
#include "TChain.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "THStack.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TString.h"
#include "TFile.h"
#include "include.h"
#include <vector>
#include <iostream>

vector<TString> split( const char *_str, const char *_pattern)
{
  string str = _str;
  string pattern = _pattern;
  vector<TString> ret;
  if(pattern.empty()) return ret;
  size_t start=0,index=str.find(pattern,0);
  while(index!=str.npos)
  {
    if(start!=index)
      ret.push_back(str.substr(start,index-start));
    start=index+pattern.length();
    index=str.find(pattern,start);
  }
  if(!str.substr(start).empty())
    ret.push_back(str.substr(start).c_str());
  return ret;
}

void ForFitter(const char* argStr="SR") {
    //gROOT -> ForceStyle();
    //gStyle -> SetOptStat(0);
    cout<<argStr<<endl;

    TString input = argStr;

    TString nameVariable = "Meff";

    TH1F* hRebin[330000];
    TH1F* hRebinNew[330000];
    TH1F* hRebinNew2[330000];

    cout<<"string"<<endl;
    TString name_all[44] = {"GHH300X","GHH300Y","GHH3f600f0",
                            "GHH3fm600f0","GHH3f350f2100","GHH3fm350f2100","GHH3fm350fm2100","GHH3f350fm2100","GHH3f0f3000","GHH3f0fm3000",
                            "GHH600X", "GHH600Y", "GHH6f650f0","GHH6fm650f0","GHH6f400f2400","GHH6fm400f2400","GHH6fm400fm2400","GHH6f400fm2400","GHH6f0f3500","GHH6f0fm3500",
                            "GHH900X", "GHH9f800f0","GHH9fm800f0","GHH9f600f3600","GHH9fm600f3600","GHH9fm600fm3600","GHH9f600fm3600","GHH9f0f5000","GHH9f0fm5000",
                            "data","TopX","WWW","SingleTop","ssWW","VVV","Wjets","ZZ","ttbar","Zjets","WZ","Vgamma","ChargeFlip","NonPrompt","PhotonConversion"};
    TString name_sys_oneside[5] = {"FATJET_JER",
                            "FATJET_JMR",
                            "FATJET_SubR",
                            "MET_SoftTrk_ResoPara",
                            "MET_SoftTrk_ResoPerp"}; // One Side sys with only one side branch in ntuple 
    // 8
    // Double_t xbins_boosted[4] = {400, 600, 800, 2000}; 
    // Double_t xbins_resolved[6] = {200, 300, 400, 500, 600, 1000};
    
    TH1F* h_bkg_boosted;
    TH1F* h_bkg_resolved;

    int i_hist = 0;
    cout<<argStr<<endl;
    TFile *myfile = TFile::Open("../"+input+".root");

    TFile f1(input+".root","recreate");
    f1.cd();
    cout<<"GetListOfKeys"<<endl; 
    for (TObject *i : *myfile->GetListOfKeys()) {
      TString name = i->GetName();
      TString newName;
      if(name.Contains("hWHPDFset=")) continue;

	if(i_hist%1000 == 0) std::cout<<i_hist<<std::endl;

      hRebin[i_hist]= (TH1F*)myfile->Get(name);
      
      TObjArray *tx = name.Tokenize("_");

      if(name.Contains("hWH")) name = "hWWW"+split(name.Data(), "hWH")[0];
      if(name.Contains("__1up")){
       //TObjArray *tup = name.Tokenize("__1up");
       //name = ((TObjString *)(tup->At(0)))->String()+"High"+((TObjString *)(tup->At(1)))->String(); 
       name = split(name.Data(), "__1up")[0]+"High"+split(name.Data(), "__1up")[1];
       //std::cout<<name<<std::endl;
      }
      else if (name.Contains("__1down")){
        name = split(name.Data(), "__1down")[0]+"Low"+split(name.Data(), "__1down")[1];
        //TObjArray *tdown = name.Tokenize("__1down");
       //name = ((TObjString *)(tdown->At(0)))->String()+"Low"+((TObjString *)(tdown->At(1)))->String();
      }

	if ( name.Contains("hNonPromptElInclusiveFF") || name.Contains("hNonPromptMuInclusiveFF") ) {
	   if(name.Contains("hNonPromptElInclusiveFF")) name = "hNonPromptElInclusiveFFEl"+split(name.Data(), "hNonPromptElInclusiveFF")[0];
	   else if(name.Contains("hNonPromptMuInclusiveFF")) name = "hNonPromptMuInclusiveFFMu"+split(name.Data(), "hNonPromptMuInclusiveFF")[0];
	   cout<<name<<endl;
	}

	hRebin[i_hist]->SetNameTitle(name.Data(), name.Data());
      // if (hRebin[i_hist]->GetNbinsX()!=120&&name.Contains("ssWW")) cout<<name<<endl;
      // hRebin[i_hist]->Sumw2();
   
      hRebinNew[i_hist] = new TH1F(name,name,hRebin[i_hist]->GetNbinsX(),0,hRebin[i_hist]->GetNbinsX());
      if( (name.Contains("SR") || name.Contains("VR") ) && name.Contains("Meff") && !name.Contains("MeffNoRebin")) {
	   if(name.Contains("SR")) {
		TString nameUnblind = split(name.Data(), "SR")[0]+"SRUnblind"+split(name.Data(), "SR")[1];
		hRebin[i_hist]->SetNameTitle(nameUnblind.Data(), nameUnblind.Data());
		hRebin[i_hist]->Write(nameUnblind.Data(),TObject::kWriteDelete);
	   }
	   //cout<<nameUnblind.Data()<<endl;
       for(int i_bin=1; i_bin < hRebin[i_hist]->GetNbinsX()+1; i_bin++){
          hRebinNew[i_hist]->SetBinContent(i_bin, hRebin[i_hist]->GetBinContent(i_bin));
          hRebinNew[i_hist]->SetBinError(i_bin, hRebin[i_hist]->GetBinError(i_bin));
       }
      hRebinNew[i_hist]->Write(name.Data(),TObject::kWriteDelete);
     }
     else hRebin[i_hist]->Write(name.Data(),TObject::kWriteDelete);
    
     for(int i_sys=0; i_sys < 5; i_sys++){
          if( !name.Contains(name_sys_oneside[i_sys]) ) continue;

          if (!name.Contains("High_")) std::cout<<"one side sys not contain UP sys"<<std::endl;
          
	    TString newname = split(name.Data(), "High_")[0]+"Low_"+split(name.Data(), "High_")[1]; 
	    TH1F* hRebinOneSideSys = (TH1F*)hRebin[i_hist]->Clone(newname);
	    hRebinOneSideSys->SetNameTitle(newname.Data(), newname.Data());
            hRebinNew2[i_hist] = new TH1F(newname,newname,hRebinOneSideSys->GetNbinsX(),0,hRebinOneSideSys->GetNbinsX());
            if( (newname.Contains("SR") || newname.Contains("VR")) && newname.Contains("Meff") && !newname.Contains("MeffNoRebin")) {
		   if(newname.Contains("SR")) {
			TString nameUnblind = split(newname.Data(), "SR")[0]+"SRUnblind"+split(newname.Data(), "SR")[1];
			hRebinOneSideSys->SetNameTitle(nameUnblind.Data(), nameUnblind.Data());
			hRebinOneSideSys->Write(nameUnblind.Data(),TObject::kWriteDelete);
		   }
		   //cout<<nameUnblind.Data()<<endl;
               for(int i_bin=1; i_bin < hRebinOneSideSys->GetNbinsX()+1; i_bin++){
                 hRebinNew2[i_hist]->SetBinContent(i_bin, hRebinOneSideSys->GetBinContent(i_bin));
                 hRebinNew2[i_hist]->SetBinError(i_bin, hRebinOneSideSys->GetBinError(i_bin));
               }
               hRebinNew2[i_hist]->Write(newname.Data(),TObject::kWriteDelete);
            }
            else hRebinOneSideSys->Write(newname.Data(),TObject::kWriteDelete);
           
		deletepointer(hRebinOneSideSys);
    }

      i_hist ++;
    }
    myfile->Close();
    f1.Close();

    cout<<i_hist<<endl;
    cout<<"done"<<endl;
}
