#include "TROOT.h"
#include "TLorentzVector.h"
#include "TChain.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TMath.h"
#include "THStack.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TString.h"
#include "TFile.h"
#include <vector>
#include <iostream>

vector<TString> split( const char *_str, const char *_pattern)
{
   string str = _str;
   string pattern = _pattern;
   vector<TString> ret;
   if(pattern.empty()) return ret;
   size_t start=0,index=str.find(pattern,0);
   while(index!=str.npos)
   {
	if(start!=index) ret.push_back(str.substr(start,index-start));
	start=index+pattern.length();
	index=str.find(pattern,start);
   }
   if(!str.substr(start).empty())
	ret.push_back(str.substr(start).c_str());
   return ret;
}

Double_t significance(Double_t b0, Double_t s0, Double_t db) {
  if(db==0) return sqrt(2*(s0+b0)*log(1+s0/b0)-2*s0);
  else {
    Double_t tmp = b0-db*db;
    Double_t b = 0.5*(tmp+sqrt(pow(tmp,2)+4*db*db*(b0+s0)));
    return sqrt(2*(b0+s0)*log((b0+s0)/b)-(b0+s0)+b-(b-b0)*b0/db/db);
  }
}

void Rebin_GHH(const char* argStr="../SR.root", const char* argStrName="SR") {
    gROOT -> ForceStyle();
    gStyle -> SetOptStat(0);

    TString nameVariable = "Meff";

    TH1F* hRebin[500000];
    TH1F* hRebinNew[500000];

    cout<<"string"<<endl;
    TString name_all[43] = {"GHH300X","GHH300Y","GHH3f600f0",
                            "GHH3fm600f0","GHH3f350f2100","GHH3fm350f2100","GHH3fm350fm2100","GHH3f350fm2100","GHH3f0f3000","GHH3f0fm3000",
                            "GHH600X", "GHH600Y", "GHH6f650f0","GHH6fm650f0","GHH6f400f2400","GHH6fm400f2400","GHH6fm400fm2400","GHH6f400fm2400","GHH6f0f3500","GHH6f0fm3500",
                            "GHH900X", "GHH9f800f0","GHH9fm800f0","GHH9f600f3600","GHH9fm600f3600","GHH9fm600fm3600","GHH9f600fm3600","GHH9f0f5000","GHH9f0fm5000",
                            "data","TopX","WWW","SingleTop","ssWW","VVV","Wjets","ZZ","ttbar","Zjets","WZ","Vgamma","ChargeFlip","NonPrompt"};
    
    TH1F* hh1 = new TH1F("hh1","None",120,400,2000); 
    for(int i=0; i<120; i++){
        hh1->SetBinContent(i+1, 0);
        hh1->SetBinError(i+1, 0);
    }

    int n_bins_boosted = 3;
    int n_bins_resolved = 5;
    TString binning = "8";
    //3lCR: nbins = 120; xMin = 200.; xMax = 3000.;
    // Double_t xbins_WZCR[25] = {200,3000};
    // others
    //boosted: nbins = 120, xMin = 400.; xMax = 2000.;
    //resolved: nbins = 120, xMin = 200.; xMax = 1000.;
    //array of low-edges (xbins[25] is the upper edge of last bin

    // 1
    // Double_t xbins_boosted[4] = {400, 600, 800, 2000}; 
    // Double_t xbins_resolved[5] = {200, 300, 400, 600, 1000};
    // 2
    // Double_t xbins_boosted[4] = {400, 600, 800, 2000}; 
    // Double_t xbins_resolved[4] = {200, 300, 500, 1000};
    // 3
    // Double_t xbins_boosted[4] = {400, 600, 800, 2000}; 
    // Double_t xbins_resolved[6] = {200, 250, 300, 400, 600, 1000};
    // 4
    // Double_t xbins_boosted[5] = {400, 500, 600, 800, 2000}; 
    // Double_t xbins_resolved[5] = {200, 300, 400, 600, 1000};
    // 5
    // Double_t xbins_boosted[5] = {400, 500, 600, 800, 2000}; 
    // Double_t xbins_resolved[4] = {200, 300, 500, 1000};
    // 6
    // Double_t xbins_boosted[5] = {400, 500, 600, 800, 2000}; 
    // Double_t xbins_resolved[6] = {200, 250, 300, 400, 600, 1000};
     // 7
    // Double_t xbins_boosted[6] = {400, 700, 1000, 1300, 1600, 2000}; 
    // Double_t xbins_resolved[6] = {200, 300, 450, 600, 800, 1000};
    // 8
     Double_t xbins_boosted[4] = {400, 600, 800, 2000}; 
     Double_t xbins_resolved[6] = {200, 300, 400, 500, 600, 1000};
    // 9
    // Double_t xbins_boosted[3] = {400 , 800, 2000}; 
    // Double_t xbins_resolved[6] = {200, 300, 400, 500, 600, 1000};
    // 10
    // Double_t xbins_boosted[3] = {400 , 700, 2000}; 
    // Double_t xbins_resolved[6] = {200, 300, 400, 500, 600, 1000};
    // 11
    // Double_t xbins_boosted[3] = {400 , 800, 2000}; 
    // Double_t xbins_resolved[6] = {200, 250, 300, 400, 600, 1000};
    // 12
    // Double_t xbins_boosted[3] = {400 , 700, 2000}; 
    // Double_t xbins_resolved[6] = {200, 250, 300, 400, 600, 1000};
    // 13
    // Double_t xbins_boosted[3] = {400 , 800, 2000}; 
    // Double_t xbins_resolved[5] = {200, 300, 400, 600, 1000};
    // 14
    //Double_t xbins_boosted[3] = {400 , 700, 2000}; 
    //Double_t xbins_resolved[5] = {200, 300, 400, 600, 1000};


    //Double_t xbins_VRboosted[5] = {400, 600, 800, 1000, 2000}; 
    //Double_t xbins_VRresolved[5] = {200, 300, 400, 600, 1000};

    int i_hist = 0;
    std::vector <TString> sysTrees;
    TString input = argStr; TString output = argStrName;
    std::cout<<input<<std::endl;

    TFile *myfile = TFile::Open(input);
    std::cout<<output<<std::endl;
    TFile f1(output+".root","recreate");
    f1.cd();

    for (TObject *i : *myfile->GetListOfKeys()) {
      sysTrees.push_back(i->GetName());
      TString name = i->GetName();
      if( name.Contains("PDF") && name.Contains("MUR")  ) continue;
      if( name.Contains("ssWWBoosted") ) continue;
      if( name.Contains("ssWW500Boosted") ) continue;
      if( name.Contains("ssWWResolved") && (name.Contains("mm_")||name.Contains("pp_")) ) continue;
      if( name.Contains("ssWW500Resolvedee")||name.Contains("ssWW500Resolvedemu")||name.Contains("ssWW500Resolvedmue")||name.Contains("ssWW500Resolvedmumu")) continue;

      if(i_hist%1000 == 0) std::cout<<i_hist<<std::endl;

      hRebin[i_hist]= (TH1F*)myfile->Get(name);
      // if (hRebin[i_hist]->GetNbinsX()!=120&&name.Contains("ssWW")) cout<<name<<endl;
      // hRebin[i_hist]->Sumw2();
      hRebin[i_hist]->SetBinContent(1, hRebin[i_hist]->GetBinContent(0)+hRebin[i_hist]->GetBinContent(1));
      hRebin[i_hist]->SetBinError(1, sqrt( pow(hRebin[i_hist]->GetBinError(0), 2) + pow(hRebin[i_hist]->GetBinError(1), 2) ) );
      hRebin[i_hist]->SetBinContent(hRebin[i_hist]->GetNbinsX(), hRebin[i_hist]->GetBinContent(hRebin[i_hist]->GetNbinsX())+hRebin[i_hist]->GetBinContent(hRebin[i_hist]->GetNbinsX()+1));
      hRebin[i_hist]->SetBinError(hRebin[i_hist]->GetNbinsX(), sqrt( pow(hRebin[i_hist]->GetBinError(hRebin[i_hist]->GetNbinsX()), 2) + pow(hRebin[i_hist]->GetBinError(hRebin[i_hist]->GetNbinsX()+1), 2) ) );
      hRebin[i_hist]->SetBinContent(0, 0);
      hRebin[i_hist]->SetBinError(0, 0);
      hRebin[i_hist]->SetBinContent(hRebin[i_hist]->GetNbinsX()+1, 0);
      hRebin[i_hist]->SetBinError(hRebin[i_hist]->GetNbinsX()+1, 0);
      if(name.Contains("Norm")) hRebin[i_hist]->Write();
      else if(name.Contains("Meff")){
	   TString newName = split(name.Data(), "Meff")[0] + "MeffNoRebin"; 
	   hRebin[i_hist]->SetNameTitle(newName.Data(), newName.Data());
	   hRebin[i_hist]->Write(newName.Data(),TObject::kWriteDelete);

         if(name.Contains("CR3l")) hRebinNew[i_hist] = (TH1F*)hRebin[i_hist]->Rebin(120,name);
         else if(name.Contains("ssWWResolved") || name.Contains("ssWW500Resolved")) hRebinNew[i_hist] = (TH1F*)hRebin[i_hist]->Rebin(32,name);
         else if (name.Contains("VR") || name.Contains("SR")){
           if (name.Contains("Boosted")) hRebinNew[i_hist] = (TH1F*)hRebin[i_hist]->Rebin(n_bins_boosted, name, xbins_boosted);
           else if (name.Contains("Resolved")) hRebinNew[i_hist] = (TH1F*)hRebin[i_hist]->Rebin(n_bins_resolved, name, xbins_resolved);
         }
         hRebinNew[i_hist]->Write(name.Data(),TObject::kWriteDelete);
      }
	else hRebin[i_hist]->Write(name.Data(),TObject::kWriteDelete);
      i_hist ++;
    }
    myfile->Close();
    f1.Close();
    cout<<i_hist<<endl;
    cout<<"done"<<endl;
}
