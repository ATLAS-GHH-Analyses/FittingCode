// #ifndef fcnc_include
// #define fcnc_include

#include "TROOT.h"
#include "TObject.h"
#include "TChain.h"
#include "TFile.h"
#include "TH1F.h"
#include "TMinuit.h"
#include "THStack.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "TLorentzVector.h"
#include "TLine.h"
#include "TLegend.h"
#include "TMVA/Tools.h"
#include "TLatex.h"
#include "TH2D.h"
#include <sstream>
#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <sstream>
#include "TMVA/Tools.h"
#include "TMVA/Factory.h"
#include "TMVA/Reader.h"
#include "TKey.h"

#include <string>
#include <chrono>
#include <unistd.h>
#include <cstdlib>
#include <thread>
#include <sys/ioctl.h>
using namespace std;

const Double_t GeV=1000;
const Double_t PI=3.1415926536;

// #include "fcnc_include.h"
void findAndReplaceAll(string & data, string toSearch, string replaceStr)
{
  // Get the first occurrence
  size_t pos = data.find(toSearch);
 
  // Repeat till end is reached
  while( pos != string::npos)
  {
    // Replace this occurrence of Sub String
    data.replace(pos, toSearch.size(), replaceStr);
    // Get the next occurrence from the current position
    pos =data.find(toSearch, pos + replaceStr.size());
  }
}
 
//vector<TString> split( const char* _str, const char* _pattern)
//{
//  string str = _str;
//  string pattern = _pattern;
//  vector<TString> ret;
//  if(pattern.empty()) return ret;
//  size_t start=0,index=str.find(pattern,0);
//  while(index!=str.npos)
//  {
//    if(start!=index)
//      ret.push_back(str.substr(start,index-start));
//    start=index+pattern.length();
//    index=str.find(pattern,start);
//  }
//  if(!str.substr(start).empty())
//    ret.push_back(str.substr(start).c_str());
//  return ret;
//}


vector<TString> readTovecString(TString filename){
  vector<TString> v;
  ifstream file(filename.Data());
  string ss;
  if(!file.good()) {
    printf("file not fould %s\n", filename.Data());
    exit(0);
  }
  while(!file.eof()){
    getline(file,ss,'\n');
    v.push_back(TString(ss));
  }
  return v;
}

double integral(TH1 *hist, double xlow, double xhigh, double *error){
  return hist->IntegralAndError(hist->FindBin(xlow), hist->FindBin(xhigh)-1, *error);
}

Double_t significance(Double_t b0, Double_t s0, Double_t db) {
  if(db==0) return sqrt(2*(s0+b0)*log(1+s0/b0)-2*s0);
  else {
    Double_t tmp = b0-db*db;
    Double_t b = 0.5*(tmp+sqrt(pow(tmp,2)+4*db*db*(b0+s0)));
    return sqrt(2*(b0+s0)*log((b0+s0)/b)-(b0+s0)+b-(b-b0)*b0/db/db);
  }
}

void SetMax(TH1* h1, TH1* h2, Double_t scale=1.0) {
  h1->SetMaximum(scale*TMath::Max(h1->GetMaximum(),h2->GetMaximum()));
  h2->SetMaximum(scale*TMath::Max(h1->GetMaximum(),h2->GetMaximum()));
}

void SetMax(THStack* h1, TH1* h2, Double_t scale=1.0) {
  h1->SetMaximum(scale*TMath::Max(h1->GetMaximum(),h2->GetMaximum()));
}

void Copy(TH1F* h1, TH1F* h2) {
  if(h1->GetNbinsX()!=h2->GetNbinsX()) {
    printf("Error: TH1Fs do not have same number of bins\n");
    return;
  }
  for(Int_t i=1; i<=h1->GetNbinsX(); i++) {
    h2->SetBinContent(i,h1->GetBinContent(i));
    h2->SetBinError(i,h1->GetBinError(i));
  }
}

Float_t AtoF(const char* str) {
  Float_t num = 1.;
  // split by '*'
  string Str = str;
  for(size_t i=0,n; i <= Str.length(); i=n+1) {
    n = Str.find('*',i);
    if (n == string::npos) n = Str.length();
    string tmp = Str.substr(i,n-i);
    num *= atof(tmp.c_str());
  }
  return num;
}


void PrintTime(int timeInSec){

  struct winsize w; 
  ioctl(0, TIOCGWINSZ, &w);
  auto start = chrono::steady_clock::now();
  for(;;){
    sleep(timeInSec);
    auto end = chrono::steady_clock::now();
    stringstream ss;
    ss<<"Elapsed time in seconds : "<< chrono::duration_cast<chrono::seconds>(end - start).count()
    << " sec";

    printf("%*s\n" , w.ws_col, ss.str().c_str());
    
  }
}

void Copy(TH1F* h1, TH1F* h2);
template<typename T,typename D>
TString CharAppend(T* aa, D* bb){
  std::stringstream ss;
  std::string str;
  ss<<aa<<bb;
  str = ss.str();
  return str;
}

template<typename T,typename D>
TString CharAppend(T aa, D bb){
  std::stringstream ss;
  std::string str;
  ss<<aa<<bb;
  str = ss.str();
  return str;
}


template<typename T,typename D>
TString CharAppend(T* aa, D bb){
  std::stringstream ss;
  std::string str;
  ss<<aa<<bb;
  str = ss.str();
  return str;
}


template<typename T,typename D>
TString CharAppend(T aa, D* bb){
  std::stringstream ss;
  std::string str;
  ss<<aa<<bb;
  str = ss.str();
  return str;
}

template<typename T>
void deletepointer(T *&a){
  if (a)
  {
    delete a;
    a = NULL;
  }
}

template<typename T,typename D>
double rms(T aa, D bb){
  return sqrt(pow(aa,2) + pow(bb,2));
}

template <typename T>
int findi(std::vector<T> vect, T target){
  int i = 0;
  for(auto ele : vect){
    if(ele == target){
      return i;
    }
    i++;
  }
  return -1;
}

template<typename T, typename D>
int FindBin(T* ary, int nbins, D nb){
  int hibin = nbins;
  int lowbin = 0;
  int newlimit = -1;
  if (nb>ary[nbins-1] || nb<ary[0])
  {
    printf("Error: exceed range\n");
    return -1;
  }
  for(;;){
    newlimit = (hibin + lowbin)/2;
    if (ary[newlimit]>nb) hibin = newlimit; 
    else if (ary[newlimit]<nb) lowbin = newlimit;
    if(newlimit == (hibin + lowbin)/2) return newlimit;
  }
}



template<typename T>
T Max(T a, T b) {
  if(a>b) return a;
  else return b;
}

// #endif







