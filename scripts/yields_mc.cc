#include<iomanip>
#include<iostream>
Double_t significance(Double_t b0, Double_t s0, Double_t db) {
  if(db==0) return sqrt(2*(s0+b0)*log(1+s0/b0)-2*s0);
  else {
    Double_t tmp = b0-db*db;
    Double_t b = 0.5*(tmp+sqrt(pow(tmp,2)+4*db*db*(b0+s0)));
    return sqrt(2*(b0+s0)*log((b0+s0)/b)-(b0+s0)+b-(b-b0)*b0/db/db);
  }
}

void yields_mc(const char* argStr="SRBoosted") {
    gROOT -> ForceStyle();
    gStyle -> SetOptStat(0);

    TFile* tfile;
    // cout<<"define n"<<endl;
    TString nameVariable = "MeffNoRebin";

    ofstream fout("output_MC.tex",ios::app);

    TString region = argStr; //SRBoosted, SRResolved, 3lCR, ssWWResolved
    fout<<region<<endl;
    tfile = new TFile("../GHH_SS2l_r04-02_sys_20211026_oldssWW.root");
    // fout<<"\\begin{table}[hp]"<<endl;
    // fout<<"\\begin{center}"<<endl;
    if(region=="3lCR") {
        fout<<"\\begin{tabular}{l|c c}"<<endl;
        fout<<"\\hline\\hline"<<endl;
        fout<<" & Boosted & Resolved  \\\\ \\hline" <<endl;
    }
    else{
         fout<<"\\begin{tabular}{l|c c c c|c}"<<endl;
         fout<<"\\hline\\hline"<<endl;
         fout<<"& $e^\\pm e^\\pm$ & $e^\\pm \\mu^\\pm$ & $\\mu^\\pm e^\\pm$ & $\\mu^\\pm \\mu^\\pm$ & $\\ell\\ell$ \\\\ \\hline"<<endl;

    }

    int n_bins = 1;
    TH1F* hSRboostedInc[100];
    TH1F* hSRboostedee[100];
    TH1F* hSRboostedemu[100];
    TH1F* hSRboostedmue[100];
    TH1F* hSRboostedmumu[100];
    TH1F* hSRresolvedInc[100];
    TH1F* hSRresolvedee[100];
    TH1F* hSRresolvedemu[100];
    TH1F* hSRresolvedmue[100];
    TH1F* hSRresolvedmumu[100];

    TH1F* hVRboostedInc[100];
    TH1F* hVRboostedee[100];
    TH1F* hVRboostedemu[100];
    TH1F* hVRboostedmue[100];
    TH1F* hVRboostedmumu[100];
    TH1F* hVRresolvedInc[100];
    TH1F* hVRresolvedee[100];
    TH1F* hVRresolvedemu[100];
    TH1F* hVRresolvedmue[100];
    TH1F* hVRresolvedmumu[100];

    TH1F* hssWWresolvedInc[100];
    TH1F* hssWWresolvedee[100];
    TH1F* hssWWresolvedemu[100];
    TH1F* hssWWresolvedmue[100];
    TH1F* hssWWresolvedmumu[100];

    TH1F* hCR3lboosted[100];
    TH1F* hCR3lresolved[100];
    TH1F* hCR3lInc[100];

    fout<<setiosflags(ios::fixed);
    fout.precision(2); //输出小数点后两位
    
    float total_ee = 0.;
    float total_emu = 0.;
    float total_mue = 0.;
    float total_mumu = 0.;
    float total_ee_error  = 0.;
    float total_emu_error  = 0.;
    float total_mue_error  = 0.;
    float total_mumu_error  = 0.;
    int n_name = 0;
    cout<<"string"<<endl;
    // TString name_all[43] = {"GHH300X","GHH300Y","GHH3f600f0",
    //                         "GHH3fm600f0","GHH3f350f2100","GHH3fm350f2100","GHH3fm350fm2100","GHH3f350fm2100","GHH3f0f3000","GHH3f0fm3000",
    //                         "GHH600X", "GHH600Y", "GHH6f650f0","GHH6fm650f0","GHH6f400f2400","GHH6fm400f2400","GHH6fm400fm2400","GHH6f400fm2400","GHH6f0f3500","GHH6f0fm3500",
    //                         "GHH900X", "GHH9f800f0","GHH9fm800f0","GHH9f600f3600","GHH9fm600f3600","GHH9fm600fm3600","GHH9f600fm3600","GHH9f0f5000","GHH9f0fm5000",
    //                         "data","TopX","WWW","SingleTop","ssWW","VVV","Wjets","ZZ","ttbar","Zjets","WZ","Vgamma","ChargeFlip","NonPrompt"};
    TString name_all[13] = {"GHH300fW0fWW4600Nom",
                            "TopXNom","WWWNom","SingleTopNom","ssWWNom","VVVNom","WjetsNom","ZZNom","ttbarNom","ZjetsNom","WZNom","VgammaNom","data"};

    if(region=="SRBoosted" || region=="SRResolved") {
        n_name = 12;
    }
    else {
        n_name = 13;
    }
    TString name_reg[2] = {"Boosted","Resolved"};
    // TString name_bkg[12] = {"data","TopX","WWW","SingleTop","ssWW","VVV","Wjets","ZZ","ttbar","Zjets","WZ","Vgamma"};
    cout<<"bkg histo"<<endl;
    TH1F* hh1 = new TH1F("hh1","None",120,400,2000); 
    for(int i=0; i<120; i++){
        hh1->SetBinContent(i+1, 0);
        hh1->SetBinError(i+1, 0);
    }
    float total_ll[13]; float total_ll_error[13];
    for(int i=0; i<13; i++){
        total_ll[i]=0;
        total_ll_error[i]=0;
    }
    // TFile f1("SS2l_AddFlatWtaggerSFMjj200Bin5.root","recreate");
    // f1.cd();

    for(int i=0; i<n_name; i++){
        hSRboostedInc[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"Inc_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"Inc_obs_"+nameVariable) : hh1 ;
        hSRboostedee[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"ee_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"ee_obs_"+nameVariable) : hh1 ;
        hSRboostedemu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"emu_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"emu_obs_"+nameVariable) : hh1 ;
        hSRboostedmue[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"mue_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"mue_obs_"+nameVariable) : hh1 ;
        hSRboostedmumu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"mumu_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[0]+"mumu_obs_"+nameVariable) : hh1 ;
       
        hSRresolvedInc[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"Inc_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"Inc_obs_"+nameVariable) : hh1;
        hSRresolvedee[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"ee_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"ee_obs_"+nameVariable) : hh1 ;
        hSRresolvedemu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"emu_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"emu_obs_"+nameVariable) : hh1;
        hSRresolvedmue[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"mue_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"mue_obs_"+nameVariable) : hh1 ;
        hSRresolvedmumu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"mumu_obs_"+nameVariable) ?  (TH1F*)tfile->Get("h"+name_all[i]+"_SRSS2l"+name_reg[1]+"mumu_obs_"+nameVariable) : hh1 ;

        hVRboostedInc[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"Inc_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"Inc_obs_"+nameVariable) : hh1 ;
        hVRboostedee[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"ee_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"ee_obs_"+nameVariable) : hh1 ;
        hVRboostedemu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"emu_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"emu_obs_"+nameVariable) : hh1 ;
        hVRboostedmue[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"mue_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"mue_obs_"+nameVariable) : hh1 ;
        hVRboostedmumu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"mumu_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[0]+"mumu_obs_"+nameVariable) : hh1 ;
       
        hVRresolvedInc[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"Inc_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"Inc_obs_"+nameVariable) : hh1 ;
        hVRresolvedee[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"ee_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"ee_obs_"+nameVariable) : hh1 ;
        hVRresolvedemu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"emu_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"emu_obs_"+nameVariable) : hh1 ;
        hVRresolvedmue[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"mue_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"mue_obs_"+nameVariable) : hh1 ;
        hVRresolvedmumu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"mumu_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_VRSS2l"+name_reg[1]+"mumu_obs_"+nameVariable) : hh1 ;
        
        hCR3lboosted[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_CR3l"+name_reg[0]+"Inc_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_CR3l"+name_reg[0]+"Inc_obs_"+nameVariable) : hh1 ;
        hCR3lresolved[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_CR3l"+name_reg[1]+"Inc_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_CR3l"+name_reg[1]+"Inc_obs_"+nameVariable) : hh1 ;
        hCR3lInc[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_CR3lInc_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_CR3lInc_obs_"+nameVariable) : hh1 ;

        hssWWresolvedInc[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"Inc_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"Inc_obs_"+nameVariable) : hh1;
        hssWWresolvedee[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"ee_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"ee_obs_"+nameVariable) : hh1 ;
        hssWWresolvedemu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"emu_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"emu_obs_"+nameVariable) : hh1;
        hssWWresolvedmue[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"mue_obs_"+nameVariable) ? (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"mue_obs_"+nameVariable) : hh1 ;
        hssWWresolvedmumu[i]= (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"mumu_obs_"+nameVariable) ?  (TH1F*)tfile->Get("h"+name_all[i]+"_ssWW"+name_reg[1]+"mumu_obs_"+nameVariable) : hh1 ;

        hSRboostedInc[i]->SetBinContent(1, hSRboostedInc[i]->GetBinContent(0)+hSRboostedInc[i]->GetBinContent(1));
        hSRboostedInc[i]->SetBinError(1, sqrt( pow(hSRboostedInc[i]->GetBinError(0), 2) + pow(hSRboostedInc[i]->GetBinError(1), 2) ) );
        hSRboostedInc[i]->SetBinContent(64, hSRboostedInc[i]->GetBinContent(64)+hSRboostedInc[i]->GetBinContent(65));
        hSRboostedInc[i]->SetBinError(64, sqrt( pow(hSRboostedInc[i]->GetBinError(64), 2) + pow(hSRboostedInc[i]->GetBinError(65), 2) ) );
        hSRboostedInc[i]->SetBinContent(0, 0);
        hSRboostedInc[i]->SetBinError(0, 0);
        hSRboostedInc[i]->SetBinContent(65, 0);
        hSRboostedInc[i]->SetBinError(65, 0);

        hSRboostedee[i]->SetBinContent(1, hSRboostedee[i]->GetBinContent(0)+hSRboostedee[i]->GetBinContent(1));
        hSRboostedee[i]->SetBinError(1, sqrt( pow(hSRboostedee[i]->GetBinError(0), 2) + pow(hSRboostedee[i]->GetBinError(1), 2) ) );
        hSRboostedee[i]->SetBinContent(64, hSRboostedee[i]->GetBinContent(64)+hSRboostedee[i]->GetBinContent(65));
        hSRboostedee[i]->SetBinError(64, sqrt( pow(hSRboostedee[i]->GetBinError(64), 2) + pow(hSRboostedee[i]->GetBinError(65), 2) ) );
        hSRboostedee[i]->SetBinContent(0, 0);
        hSRboostedee[i]->SetBinError(0, 0);
        hSRboostedee[i]->SetBinContent(65, 0);
        hSRboostedee[i]->SetBinError(65, 0);

        hSRboostedemu[i]->SetBinContent(1, hSRboostedemu[i]->GetBinContent(0)+hSRboostedemu[i]->GetBinContent(1));
        hSRboostedemu[i]->SetBinError(1, sqrt( pow(hSRboostedemu[i]->GetBinError(0), 2) + pow(hSRboostedemu[i]->GetBinError(1), 2) ) );
        hSRboostedemu[i]->SetBinContent(64, hSRboostedemu[i]->GetBinContent(64)+hSRboostedemu[i]->GetBinContent(65));
        hSRboostedemu[i]->SetBinError(64, sqrt( pow(hSRboostedemu[i]->GetBinError(64), 2) + pow(hSRboostedemu[i]->GetBinError(65), 2) ) );
        hSRboostedemu[i]->SetBinContent(0, 0);
        hSRboostedemu[i]->SetBinError(0, 0);
        hSRboostedemu[i]->SetBinContent(65, 0);
        hSRboostedemu[i]->SetBinError(65, 0);

        hSRboostedmue[i]->SetBinContent(1, hSRboostedmue[i]->GetBinContent(0)+hSRboostedmue[i]->GetBinContent(1));
        hSRboostedmue[i]->SetBinError(1, sqrt( pow(hSRboostedmue[i]->GetBinError(0), 2) + pow(hSRboostedmue[i]->GetBinError(1), 2) ) );
        hSRboostedmue[i]->SetBinContent(64, hSRboostedmue[i]->GetBinContent(64)+hSRboostedmue[i]->GetBinContent(65));
        hSRboostedmue[i]->SetBinError(64, sqrt( pow(hSRboostedmue[i]->GetBinError(64), 2) + pow(hSRboostedmue[i]->GetBinError(65), 2) ) );
        hSRboostedmue[i]->SetBinContent(0, 0);
        hSRboostedmue[i]->SetBinError(0, 0);
        hSRboostedmue[i]->SetBinContent(65, 0);
        hSRboostedmue[i]->SetBinError(65, 0);

        hSRboostedmumu[i]->SetBinContent(1, hSRboostedmumu[i]->GetBinContent(0)+hSRboostedmumu[i]->GetBinContent(1));
        hSRboostedmumu[i]->SetBinError(1, sqrt( pow(hSRboostedmumu[i]->GetBinError(0), 2) + pow(hSRboostedmumu[i]->GetBinError(1), 2) ) );
        hSRboostedmumu[i]->SetBinContent(64, hSRboostedmumu[i]->GetBinContent(64)+hSRboostedmumu[i]->GetBinContent(65));
        hSRboostedmumu[i]->SetBinError(64, sqrt( pow(hSRboostedmumu[i]->GetBinError(64), 2) + pow(hSRboostedmumu[i]->GetBinError(65), 2) ) );
        hSRboostedmumu[i]->SetBinContent(0, 0);
        hSRboostedmumu[i]->SetBinError(0, 0);
        hSRboostedmumu[i]->SetBinContent(65, 0);
        hSRboostedmumu[i]->SetBinError(65, 0);

        hSRresolvedInc[i]->SetBinContent(1, hSRresolvedInc[i]->GetBinContent(0)+hSRresolvedInc[i]->GetBinContent(1));
        hSRresolvedInc[i]->SetBinError(1, sqrt( pow(hSRresolvedInc[i]->GetBinError(0), 2) + pow(hSRresolvedInc[i]->GetBinError(1), 2) ) );
        hSRresolvedInc[i]->SetBinContent(32, hSRresolvedInc[i]->GetBinContent(32)+hSRresolvedInc[i]->GetBinContent(33));
        hSRresolvedInc[i]->SetBinError(32, sqrt( pow(hSRresolvedInc[i]->GetBinError(32), 2) + pow(hSRresolvedInc[i]->GetBinError(33), 2) ) );
        hSRresolvedInc[i]->SetBinContent(0, 0);
        hSRresolvedInc[i]->SetBinError(0, 0);
        hSRresolvedInc[i]->SetBinContent(33, 0);
        hSRresolvedInc[i]->SetBinError(33, 0);

        hSRresolvedee[i]->SetBinContent(1, hSRresolvedee[i]->GetBinContent(0)+hSRresolvedee[i]->GetBinContent(1));
        hSRresolvedee[i]->SetBinError(1, sqrt( pow(hSRresolvedee[i]->GetBinError(0), 2) + pow(hSRresolvedee[i]->GetBinError(1), 2) ) );
        hSRresolvedee[i]->SetBinContent(32, hSRresolvedee[i]->GetBinContent(32)+hSRresolvedee[i]->GetBinContent(33));
        hSRresolvedee[i]->SetBinError(32, sqrt( pow(hSRresolvedee[i]->GetBinError(32), 2) + pow(hSRresolvedee[i]->GetBinError(33), 2) ) );
        hSRresolvedee[i]->SetBinContent(0, 0);
        hSRresolvedee[i]->SetBinError(0, 0);
        hSRresolvedee[i]->SetBinContent(33, 0);
        hSRresolvedee[i]->SetBinError(33, 0);

        hSRresolvedemu[i]->SetBinContent(1, hSRresolvedemu[i]->GetBinContent(0)+hSRresolvedemu[i]->GetBinContent(1));
        hSRresolvedemu[i]->SetBinError(1, sqrt( pow(hSRresolvedemu[i]->GetBinError(0), 2) + pow(hSRresolvedemu[i]->GetBinError(1), 2) ) );
        hSRresolvedemu[i]->SetBinContent(32, hSRresolvedemu[i]->GetBinContent(32)+hSRresolvedemu[i]->GetBinContent(33));
        hSRresolvedemu[i]->SetBinError(32, sqrt( pow(hSRresolvedemu[i]->GetBinError(32), 2) + pow(hSRresolvedemu[i]->GetBinError(33), 2) ) );
        hSRresolvedemu[i]->SetBinContent(0, 0);
        hSRresolvedemu[i]->SetBinError(0, 0);
        hSRresolvedemu[i]->SetBinContent(33, 0);
        hSRresolvedemu[i]->SetBinError(33, 0);

        hSRresolvedmue[i]->SetBinContent(1, hSRresolvedmue[i]->GetBinContent(0)+hSRresolvedmue[i]->GetBinContent(1));
        hSRresolvedmue[i]->SetBinError(1, sqrt( pow(hSRresolvedmue[i]->GetBinError(0), 2) + pow(hSRresolvedmue[i]->GetBinError(1), 2) ) );
        hSRresolvedmue[i]->SetBinContent(32, hSRresolvedmue[i]->GetBinContent(32)+hSRresolvedmue[i]->GetBinContent(33));
        hSRresolvedmue[i]->SetBinError(32, sqrt( pow(hSRresolvedmue[i]->GetBinError(32), 2) + pow(hSRresolvedmue[i]->GetBinError(33), 2) ) );
        hSRresolvedmue[i]->SetBinContent(0, 0);
        hSRresolvedmue[i]->SetBinError(0, 0);
        hSRresolvedmue[i]->SetBinContent(33, 0);
        hSRresolvedmue[i]->SetBinError(33, 0);

        hSRresolvedmumu[i]->SetBinContent(1, hSRresolvedmumu[i]->GetBinContent(0)+hSRresolvedmumu[i]->GetBinContent(1));
        hSRresolvedmumu[i]->SetBinError(1, sqrt( pow(hSRresolvedmumu[i]->GetBinError(0), 2) + pow(hSRresolvedmumu[i]->GetBinError(1), 2) ) );
        hSRresolvedmumu[i]->SetBinContent(32, hSRresolvedmumu[i]->GetBinContent(32)+hSRresolvedmumu[i]->GetBinContent(33));
        hSRresolvedmumu[i]->SetBinError(32, sqrt( pow(hSRresolvedmumu[i]->GetBinError(32), 2) + pow(hSRresolvedmumu[i]->GetBinError(33), 2) ) );
        hSRresolvedmumu[i]->SetBinContent(0, 0);
        hSRresolvedmumu[i]->SetBinError(0, 0);
        hSRresolvedmumu[i]->SetBinContent(33, 0);
        hSRresolvedmumu[i]->SetBinError(33, 0);

        hVRboostedInc[i]->SetBinContent(1, hVRboostedInc[i]->GetBinContent(0)+hVRboostedInc[i]->GetBinContent(1));
        hVRboostedInc[i]->SetBinError(1, sqrt( pow(hVRboostedInc[i]->GetBinError(0), 2) + pow(hVRboostedInc[i]->GetBinError(1), 2) ) );
        hVRboostedInc[i]->SetBinContent(64, hVRboostedInc[i]->GetBinContent(64)+hVRboostedInc[i]->GetBinContent(65));
        hVRboostedInc[i]->SetBinError(64, sqrt( pow(hVRboostedInc[i]->GetBinError(64), 2) + pow(hVRboostedInc[i]->GetBinError(65), 2) ) );
        hVRboostedInc[i]->SetBinContent(0, 0);
        hVRboostedInc[i]->SetBinError(0, 0);
        hVRboostedInc[i]->SetBinContent(65, 0);
        hVRboostedInc[i]->SetBinError(65, 0);

        hVRboostedee[i]->SetBinContent(1, hVRboostedee[i]->GetBinContent(0)+hVRboostedee[i]->GetBinContent(1));
        hVRboostedee[i]->SetBinError(1, sqrt( pow(hVRboostedee[i]->GetBinError(0), 2) + pow(hVRboostedee[i]->GetBinError(1), 2) ) );
        hVRboostedee[i]->SetBinContent(64, hVRboostedee[i]->GetBinContent(64)+hVRboostedee[i]->GetBinContent(65));
        hVRboostedee[i]->SetBinError(64, sqrt( pow(hVRboostedee[i]->GetBinError(64), 2) + pow(hVRboostedee[i]->GetBinError(65), 2) ) );
        hVRboostedee[i]->SetBinContent(0, 0);
        hVRboostedee[i]->SetBinError(0, 0);
        hVRboostedee[i]->SetBinContent(65, 0);
        hVRboostedee[i]->SetBinError(65, 0);

        hVRboostedemu[i]->SetBinContent(1, hVRboostedemu[i]->GetBinContent(0)+hVRboostedemu[i]->GetBinContent(1));
        hVRboostedemu[i]->SetBinError(1, sqrt( pow(hVRboostedemu[i]->GetBinError(0), 2) + pow(hVRboostedemu[i]->GetBinError(1), 2) ) );
        hVRboostedemu[i]->SetBinContent(64, hVRboostedemu[i]->GetBinContent(64)+hVRboostedemu[i]->GetBinContent(65));
        hVRboostedemu[i]->SetBinError(64, sqrt( pow(hVRboostedemu[i]->GetBinError(64), 2) + pow(hVRboostedemu[i]->GetBinError(65), 2) ) );
        hVRboostedemu[i]->SetBinContent(0, 0);
        hVRboostedemu[i]->SetBinError(0, 0);
        hVRboostedemu[i]->SetBinContent(65, 0);
        hVRboostedemu[i]->SetBinError(65, 0);

        hVRboostedmue[i]->SetBinContent(1, hVRboostedmue[i]->GetBinContent(0)+hVRboostedmue[i]->GetBinContent(1));
        hVRboostedmue[i]->SetBinError(1, sqrt( pow(hVRboostedmue[i]->GetBinError(0), 2) + pow(hVRboostedmue[i]->GetBinError(1), 2) ) );
        hVRboostedmue[i]->SetBinContent(64, hVRboostedmue[i]->GetBinContent(64)+hVRboostedmue[i]->GetBinContent(65));
        hVRboostedmue[i]->SetBinError(64, sqrt( pow(hVRboostedmue[i]->GetBinError(64), 2) + pow(hVRboostedmue[i]->GetBinError(65), 2) ) );
        hVRboostedmue[i]->SetBinContent(0, 0);
        hVRboostedmue[i]->SetBinError(0, 0);
        hVRboostedmue[i]->SetBinContent(65, 0);
        hVRboostedmue[i]->SetBinError(65, 0);

        hVRboostedmumu[i]->SetBinContent(1, hVRboostedmumu[i]->GetBinContent(0)+hVRboostedmumu[i]->GetBinContent(1));
        hVRboostedmumu[i]->SetBinError(1, sqrt( pow(hVRboostedmumu[i]->GetBinError(0), 2) + pow(hVRboostedmumu[i]->GetBinError(1), 2) ) );
        hVRboostedmumu[i]->SetBinContent(64, hVRboostedmumu[i]->GetBinContent(64)+hVRboostedmumu[i]->GetBinContent(65));
        hVRboostedmumu[i]->SetBinError(64, sqrt( pow(hVRboostedmumu[i]->GetBinError(64), 2) + pow(hVRboostedmumu[i]->GetBinError(65), 2) ) );
        hVRboostedmumu[i]->SetBinContent(0, 0);
        hVRboostedmumu[i]->SetBinError(0, 0);
        hVRboostedmumu[i]->SetBinContent(65, 0);
        hVRboostedmumu[i]->SetBinError(65, 0);

        hVRresolvedInc[i]->SetBinContent(1, hVRresolvedInc[i]->GetBinContent(0)+hVRresolvedInc[i]->GetBinContent(1));
        hVRresolvedInc[i]->SetBinError(1, sqrt( pow(hVRresolvedInc[i]->GetBinError(0), 2) + pow(hVRresolvedInc[i]->GetBinError(1), 2) ) );
        hVRresolvedInc[i]->SetBinContent(32, hVRresolvedInc[i]->GetBinContent(32)+hVRresolvedInc[i]->GetBinContent(33));
        hVRresolvedInc[i]->SetBinError(32, sqrt( pow(hVRresolvedInc[i]->GetBinError(32), 2) + pow(hVRresolvedInc[i]->GetBinError(33), 2) ) );
        hVRresolvedInc[i]->SetBinContent(0, 0);
        hVRresolvedInc[i]->SetBinError(0, 0);
        hVRresolvedInc[i]->SetBinContent(33, 0);
        hVRresolvedInc[i]->SetBinError(33, 0);

        hVRresolvedee[i]->SetBinContent(1, hVRresolvedee[i]->GetBinContent(0)+hVRresolvedee[i]->GetBinContent(1));
        hVRresolvedee[i]->SetBinError(1, sqrt( pow(hVRresolvedee[i]->GetBinError(0), 2) + pow(hVRresolvedee[i]->GetBinError(1), 2) ) );
        hVRresolvedee[i]->SetBinContent(32, hVRresolvedee[i]->GetBinContent(32)+hVRresolvedee[i]->GetBinContent(33));
        hVRresolvedee[i]->SetBinError(32, sqrt( pow(hVRresolvedee[i]->GetBinError(32), 2) + pow(hVRresolvedee[i]->GetBinError(33), 2) ) );
        hVRresolvedee[i]->SetBinContent(0, 0);
        hVRresolvedee[i]->SetBinError(0, 0);
        hVRresolvedee[i]->SetBinContent(33, 0);
        hVRresolvedee[i]->SetBinError(33, 0);

        hVRresolvedemu[i]->SetBinContent(1, hVRresolvedemu[i]->GetBinContent(0)+hVRresolvedemu[i]->GetBinContent(1));
        hVRresolvedemu[i]->SetBinError(1, sqrt( pow(hVRresolvedemu[i]->GetBinError(0), 2) + pow(hVRresolvedemu[i]->GetBinError(1), 2) ) );
        hVRresolvedemu[i]->SetBinContent(32, hVRresolvedemu[i]->GetBinContent(32)+hVRresolvedemu[i]->GetBinContent(33));
        hVRresolvedemu[i]->SetBinError(32, sqrt( pow(hVRresolvedemu[i]->GetBinError(32), 2) + pow(hVRresolvedemu[i]->GetBinError(33), 2) ) );
        hVRresolvedemu[i]->SetBinContent(0, 0);
        hVRresolvedemu[i]->SetBinError(0, 0);
        hVRresolvedemu[i]->SetBinContent(33, 0);
        hVRresolvedemu[i]->SetBinError(33, 0);

        hVRresolvedmue[i]->SetBinContent(1, hVRresolvedmue[i]->GetBinContent(0)+hVRresolvedmue[i]->GetBinContent(1));
        hVRresolvedmue[i]->SetBinError(1, sqrt( pow(hVRresolvedmue[i]->GetBinError(0), 2) + pow(hVRresolvedmue[i]->GetBinError(1), 2) ) );
        hVRresolvedmue[i]->SetBinContent(32, hVRresolvedmue[i]->GetBinContent(32)+hVRresolvedmue[i]->GetBinContent(33));
        hVRresolvedmue[i]->SetBinError(32, sqrt( pow(hVRresolvedmue[i]->GetBinError(32), 2) + pow(hVRresolvedmue[i]->GetBinError(33), 2) ) );
        hVRresolvedmue[i]->SetBinContent(0, 0);
        hVRresolvedmue[i]->SetBinError(0, 0);
        hVRresolvedmue[i]->SetBinContent(33, 0);
        hVRresolvedmue[i]->SetBinError(33, 0);

        hVRresolvedmumu[i]->SetBinContent(1, hVRresolvedmumu[i]->GetBinContent(0)+hVRresolvedmumu[i]->GetBinContent(1));
        hVRresolvedmumu[i]->SetBinError(1, sqrt( pow(hVRresolvedmumu[i]->GetBinError(0), 2) + pow(hVRresolvedmumu[i]->GetBinError(1), 2) ) );
        hVRresolvedmumu[i]->SetBinContent(32, hVRresolvedmumu[i]->GetBinContent(32)+hVRresolvedmumu[i]->GetBinContent(33));
        hVRresolvedmumu[i]->SetBinError(32, sqrt( pow(hVRresolvedmumu[i]->GetBinError(32), 2) + pow(hVRresolvedmumu[i]->GetBinError(33), 2) ) );
        hVRresolvedmumu[i]->SetBinContent(0, 0);
        hVRresolvedmumu[i]->SetBinError(0, 0);
        hVRresolvedmumu[i]->SetBinContent(33, 0);
        hVRresolvedmumu[i]->SetBinError(33, 0);

        hssWWresolvedInc[i]->SetBinContent(1, hssWWresolvedInc[i]->GetBinContent(0)+hssWWresolvedInc[i]->GetBinContent(1));
        hssWWresolvedInc[i]->SetBinError(1, sqrt( pow(hssWWresolvedInc[i]->GetBinError(0), 2) + pow(hssWWresolvedInc[i]->GetBinError(1), 2) ) );
        hssWWresolvedInc[i]->SetBinContent(32, hssWWresolvedInc[i]->GetBinContent(32)+hssWWresolvedInc[i]->GetBinContent(33));
        hssWWresolvedInc[i]->SetBinError(32, sqrt( pow(hssWWresolvedInc[i]->GetBinError(32), 2) + pow(hssWWresolvedInc[i]->GetBinError(33), 2) ) );
        hssWWresolvedInc[i]->SetBinContent(0, 0);
        hssWWresolvedInc[i]->SetBinError(0, 0);
        hssWWresolvedInc[i]->SetBinContent(33, 0);
        hssWWresolvedInc[i]->SetBinError(33, 0);

        hssWWresolvedee[i]->SetBinContent(1, hssWWresolvedee[i]->GetBinContent(0)+hssWWresolvedee[i]->GetBinContent(1));
        hssWWresolvedee[i]->SetBinError(1, sqrt( pow(hssWWresolvedee[i]->GetBinError(0), 2) + pow(hssWWresolvedee[i]->GetBinError(1), 2) ) );
        hssWWresolvedee[i]->SetBinContent(32, hssWWresolvedee[i]->GetBinContent(32)+hssWWresolvedee[i]->GetBinContent(33));
        hssWWresolvedee[i]->SetBinError(32, sqrt( pow(hssWWresolvedee[i]->GetBinError(32), 2) + pow(hssWWresolvedee[i]->GetBinError(33), 2) ) );
        hssWWresolvedee[i]->SetBinContent(0, 0);
        hssWWresolvedee[i]->SetBinError(0, 0);
        hssWWresolvedee[i]->SetBinContent(33, 0);
        hssWWresolvedee[i]->SetBinError(33, 0);

        hssWWresolvedemu[i]->SetBinContent(1, hssWWresolvedemu[i]->GetBinContent(0)+hssWWresolvedemu[i]->GetBinContent(1));
        hssWWresolvedemu[i]->SetBinError(1, sqrt( pow(hssWWresolvedemu[i]->GetBinError(0), 2) + pow(hssWWresolvedemu[i]->GetBinError(1), 2) ) );
        hssWWresolvedemu[i]->SetBinContent(32, hssWWresolvedemu[i]->GetBinContent(32)+hssWWresolvedemu[i]->GetBinContent(33));
        hssWWresolvedemu[i]->SetBinError(32, sqrt( pow(hssWWresolvedemu[i]->GetBinError(32), 2) + pow(hssWWresolvedemu[i]->GetBinError(33), 2) ) );
        hssWWresolvedemu[i]->SetBinContent(0, 0);
        hssWWresolvedemu[i]->SetBinError(0, 0);
        hssWWresolvedemu[i]->SetBinContent(33, 0);
        hssWWresolvedemu[i]->SetBinError(33, 0);

        hssWWresolvedmue[i]->SetBinContent(1, hssWWresolvedmue[i]->GetBinContent(0)+hssWWresolvedmue[i]->GetBinContent(1));
        hssWWresolvedmue[i]->SetBinError(1, sqrt( pow(hssWWresolvedmue[i]->GetBinError(0), 2) + pow(hssWWresolvedmue[i]->GetBinError(1), 2) ) );
        hssWWresolvedmue[i]->SetBinContent(32, hssWWresolvedmue[i]->GetBinContent(32)+hssWWresolvedmue[i]->GetBinContent(33));
        hssWWresolvedmue[i]->SetBinError(32, sqrt( pow(hssWWresolvedmue[i]->GetBinError(32), 2) + pow(hssWWresolvedmue[i]->GetBinError(33), 2) ) );
        hssWWresolvedmue[i]->SetBinContent(0, 0);
        hssWWresolvedmue[i]->SetBinError(0, 0);
        hssWWresolvedmue[i]->SetBinContent(33, 0);
        hssWWresolvedmue[i]->SetBinError(33, 0);

        hssWWresolvedmumu[i]->SetBinContent(1, hssWWresolvedmumu[i]->GetBinContent(0)+hssWWresolvedmumu[i]->GetBinContent(1));
        hssWWresolvedmumu[i]->SetBinError(1, sqrt( pow(hssWWresolvedmumu[i]->GetBinError(0), 2) + pow(hssWWresolvedmumu[i]->GetBinError(1), 2) ) );
        hssWWresolvedmumu[i]->SetBinContent(32, hssWWresolvedmumu[i]->GetBinContent(32)+hssWWresolvedmumu[i]->GetBinContent(33));
        hssWWresolvedmumu[i]->SetBinError(32, sqrt( pow(hssWWresolvedmumu[i]->GetBinError(32), 2) + pow(hssWWresolvedmumu[i]->GetBinError(33), 2) ) );
        hssWWresolvedmumu[i]->SetBinContent(0, 0);
        hssWWresolvedmumu[i]->SetBinError(0, 0);
        hssWWresolvedmumu[i]->SetBinContent(33, 0);
        hssWWresolvedmumu[i]->SetBinError(33, 0);

        hCR3lboosted[i]->SetBinContent(1, hCR3lboosted[i]->GetBinContent(0)+hCR3lboosted[i]->GetBinContent(1));
        hCR3lboosted[i]->SetBinError(1, sqrt( pow(hCR3lboosted[i]->GetBinError(0), 2) + pow(hCR3lboosted[i]->GetBinError(1), 2) ) );
        hCR3lboosted[i]->SetBinContent(120, hCR3lboosted[i]->GetBinContent(120)+hCR3lboosted[i]->GetBinContent(121));
        hCR3lboosted[i]->SetBinError(120, sqrt( pow(hCR3lboosted[i]->GetBinError(120), 2) + pow(hCR3lboosted[i]->GetBinError(121), 2) ) );
        hCR3lboosted[i]->SetBinContent(0, 0);
        hCR3lboosted[i]->SetBinError(0, 0);
        hCR3lboosted[i]->SetBinContent(121, 0);
        hCR3lboosted[i]->SetBinError(121, 0);

        hCR3lresolved[i]->SetBinContent(1, hCR3lresolved[i]->GetBinContent(0)+hCR3lresolved[i]->GetBinContent(1));
        hCR3lresolved[i]->SetBinError(1, sqrt( pow(hCR3lresolved[i]->GetBinError(0), 2) + pow(hCR3lresolved[i]->GetBinError(1), 2) ) );
        hCR3lresolved[i]->SetBinContent(120, hCR3lresolved[i]->GetBinContent(120)+hCR3lresolved[i]->GetBinContent(121));
        hCR3lresolved[i]->SetBinError(120, sqrt( pow(hCR3lresolved[i]->GetBinError(120), 2) + pow(hCR3lresolved[i]->GetBinError(121), 2) ) );
        hCR3lresolved[i]->SetBinContent(0, 0);
        hCR3lresolved[i]->SetBinError(0, 0);
        hCR3lresolved[i]->SetBinContent(121, 0);
        hCR3lresolved[i]->SetBinError(121, 0);

        hCR3lInc[i]->SetBinContent(1, hCR3lInc[i]->GetBinContent(0)+hCR3lInc[i]->GetBinContent(1));
        hCR3lInc[i]->SetBinError(1, sqrt( pow(hCR3lInc[i]->GetBinError(0), 2) + pow(hCR3lInc[i]->GetBinError(1), 2) ) );
        hCR3lInc[i]->SetBinContent(120, hCR3lInc[i]->GetBinContent(120)+hCR3lInc[i]->GetBinContent(121));
        hCR3lInc[i]->SetBinError(120, sqrt( pow(hCR3lInc[i]->GetBinError(120), 2) + pow(hCR3lInc[i]->GetBinError(121), 2) ) );
        hCR3lInc[i]->SetBinContent(0, 0);
        hCR3lInc[i]->SetBinError(0, 0);
        hCR3lInc[i]->SetBinContent(121, 0);
        hCR3lInc[i]->SetBinError(121, 0);

        if(hSRboostedInc[i]->Integral() > 0) {
          hSRboostedInc[i]->Rebin(64./n_bins);
        }
        if(hSRboostedee[i]->Integral() > 0) {
          hSRboostedee[i]->Rebin(64./n_bins);
        }
        if(hSRboostedemu[i]->Integral() > 0) {
          hSRboostedemu[i]->Rebin(64./n_bins);
        }
        if(hSRboostedmue[i]->Integral() > 0) {
          hSRboostedmue[i]->Rebin(64./n_bins);
        }
        if(hSRboostedmumu[i]->Integral() > 0) {
          hSRboostedmumu[i]->Rebin(64./n_bins);
        }

        // cout<<"rebin hSRboostedInc "<<i<<" : "<<hSRboostedInc[i]->Integral(1,5)<<endl;
       
        if(hSRresolvedInc[i]->Integral() > 0) {
          hSRresolvedInc[i]->Rebin(32./n_bins);
        }
        if(hSRresolvedee[i]->Integral() > 0) {
          hSRresolvedee[i]->Rebin(32./n_bins);
        }
        if(hSRresolvedemu[i]->Integral() > 0) {
          hSRresolvedemu[i]->Rebin(32./n_bins);
        }
        if(hSRresolvedmue[i]->Integral() > 0) {
          hSRresolvedmue[i]->Rebin(32./n_bins);
        }
        if(hSRresolvedmumu[i]->Integral() > 0)  {
          hSRresolvedmumu[i]->Rebin(32./n_bins);
        }

        if(hVRboostedInc[i]->Integral() > 0)  {
          hVRboostedInc[i]->Rebin(64./n_bins);
        }
        if(hVRboostedee[i]->Integral() > 0) {
          hVRboostedee[i]->Rebin(64./n_bins);
        }
        if(hVRboostedemu[i]->Integral() > 0) {
          hVRboostedemu[i]->Rebin(64./n_bins);
        }
        if(hVRboostedmue[i]->Integral() > 0) {
          hVRboostedmue[i]->Rebin(64./n_bins);
        }
        if(hVRboostedmumu[i]->Integral() > 0) {
          hVRboostedmumu[i]->Rebin(64./n_bins);
        }
       
        if(hVRresolvedInc[i]->Integral() > 0) {
          hVRresolvedInc[i]->Rebin(32./n_bins);
        }
        if(hVRresolvedee[i]->Integral() > 0) {
          hVRresolvedee[i]->Rebin(32./n_bins);
        }
        if(hVRresolvedemu[i]->Integral() > 0) {
          hVRresolvedemu[i]->Rebin(32./n_bins);
        }
        if(hVRresolvedmue[i]->Integral() > 0) {
          hVRresolvedmue[i]->Rebin(32./n_bins);
        }
        if(hVRresolvedmumu[i]->Integral() > 0) {
          hVRresolvedmumu[i]->Rebin(32./n_bins);
        }

        if(hCR3lboosted[i]->Integral() > 0) {
	     hCR3lboosted[i]->Rebin(120./n_bins);
        }
        if(hCR3lresolved[i]->Integral() > 0) {
	     hCR3lresolved[i]->Rebin(120./n_bins);
        }
        if(hCR3lInc[i]->Integral() > 0) {
	     hCR3lInc[i]->Rebin(120./n_bins);
        }
        
        if(hssWWresolvedInc[i]->Integral() > 0) {
          hssWWresolvedInc[i]->Rebin(32./n_bins);
        }
        if(hssWWresolvedee[i]->Integral() > 0) {
          hssWWresolvedee[i]->Rebin(32./n_bins);
        }
        if(hssWWresolvedemu[i]->Integral() > 0) {
          hssWWresolvedemu[i]->Rebin(32./n_bins);
        }
        if(hssWWresolvedmue[i]->Integral() > 0) {
          hssWWresolvedmue[i]->Rebin(32./n_bins);
        }
        if(hssWWresolvedmumu[i]->Integral() > 0)  {
          hssWWresolvedmumu[i]->Rebin(32./n_bins);
        }
        // 3lCR
        // cout<<name_all[i]<<" & $"<<hCR3lboosted[i]->GetBinContent(1)<<"pm"<<hCR3lboosted[i]->GetBinError(1)<<"$ & $"<<hCR3lresolved[i]->GetBinContent(1)<<"pm"<<hCR3lresolved[i]->GetBinError(1)<<"$"<<endl;
        // total_ee += hCR3lboosted[i]->GetBinContent(1);  total_ee_error += hCR3lboosted[i]->GetBinError(1)*hCR3lboosted[i]->GetBinError(1);
        // total_emu += hCR3lresolved[i]->GetBinContent(1);  total_emu_error += hCR3lresolved[i]->GetBinError(1)*hCR3lresolved[i]->GetBinError(1);
        // SR and VR
        if(region == "VRBoosted"){
        total_ll[i] = hVRboostedee[i]->GetBinContent(1) + hVRboostedemu[i]->GetBinContent(1) + hVRboostedmue[i]->GetBinContent(1) + hVRboostedmumu[i]->GetBinContent(1);
        total_ll_error[i] = sqrt( pow(hVRboostedee[i]->GetBinError(1),2) + pow(hVRboostedemu[i]->GetBinError(1),2) + pow(hVRboostedmue[i]->GetBinError(1),2) + pow(hVRboostedmumu[i]->GetBinError(1),2) );

        if(i<12) {
            fout<<name_all[i]<<" & $"<<hVRboostedee[i]->GetBinContent(1)<<"\\pm"<<hVRboostedee[i]->GetBinError(1)<<"$ & $"<<hVRboostedemu[i]->GetBinContent(1)<<"\\pm"<<hVRboostedemu[i]->GetBinError(1)<<"$ & $"<<hVRboostedmue[i]->GetBinContent(1)<<"\\pm"<<hVRboostedmue[i]->GetBinError(1)<<"$ & $"<<hVRboostedmumu[i]->GetBinContent(1)<<"\\pm"<<hVRboostedmumu[i]->GetBinError(1)<<"$ & $"<<total_ll[i]<<"\\pm"<<total_ll_error[i]<<"$ \\\\"<<endl;
        total_ee += hVRboostedee[i]->GetBinContent(1);  total_ee_error += hVRboostedee[i]->GetBinError(1)*hVRboostedee[i]->GetBinError(1);
        total_emu += hVRboostedemu[i]->GetBinContent(1);  total_emu_error += hVRboostedemu[i]->GetBinError(1)*hVRboostedemu[i]->GetBinError(1);
        total_mue += hVRboostedmue[i]->GetBinContent(1);  total_mue_error += hVRboostedmue[i]->GetBinError(1)*hVRboostedmue[i]->GetBinError(1);
        total_mumu += hVRboostedmumu[i]->GetBinContent(1);  total_mumu_error += hVRboostedmumu[i]->GetBinError(1)*hVRboostedmumu[i]->GetBinError(1);
        }}

        else if(region == "VRResolved"){
        total_ll[i] = hVRresolvedee[i]->GetBinContent(1) + hVRresolvedemu[i]->GetBinContent(1) + hVRresolvedmue[i]->GetBinContent(1) + hVRresolvedmumu[i]->GetBinContent(1);
        total_ll_error[i] = sqrt( pow(hVRresolvedee[i]->GetBinError(1),2) + pow(hVRresolvedemu[i]->GetBinError(1),2) + pow(hVRresolvedmue[i]->GetBinError(1),2) + pow(hVRresolvedmumu[i]->GetBinError(1),2) );
        if(i<12) {
            fout<<name_all[i]<<" & $"<<hVRresolvedee[i]->GetBinContent(1)<<"\\pm"<<hVRresolvedee[i]->GetBinError(1)<<"$ & $"<<hVRresolvedemu[i]->GetBinContent(1)<<"\\pm"<<hVRresolvedemu[i]->GetBinError(1)<<"$ & $"<<hVRresolvedmue[i]->GetBinContent(1)<<"\\pm"<<hVRresolvedmue[i]->GetBinError(1)<<"$ & $"<<hVRresolvedmumu[i]->GetBinContent(1)<<"\\pm"<<hVRresolvedmumu[i]->GetBinError(1)<<"$ & $"<<total_ll[i]<<"\\pm"<<total_ll_error[i]<<"$ \\\\"<<endl;
        total_ee += hVRresolvedee[i]->GetBinContent(1);  total_ee_error += hVRresolvedee[i]->GetBinError(1)*hVRresolvedee[i]->GetBinError(1);
        total_emu += hVRresolvedemu[i]->GetBinContent(1);  total_emu_error += hVRresolvedemu[i]->GetBinError(1)*hVRresolvedemu[i]->GetBinError(1);
        total_mue += hVRresolvedmue[i]->GetBinContent(1);  total_mue_error += hVRresolvedmue[i]->GetBinError(1)*hVRresolvedmue[i]->GetBinError(1);
        total_mumu += hVRresolvedmumu[i]->GetBinContent(1);  total_mumu_error += hVRresolvedmumu[i]->GetBinError(1)*hVRresolvedmumu[i]->GetBinError(1);
        }}

        else if (region=="SRBoosted"){
        total_ll[i] = hSRboostedee[i]->GetBinContent(1) + hSRboostedemu[i]->GetBinContent(1) + hSRboostedmue[i]->GetBinContent(1) + hSRboostedmumu[i]->GetBinContent(1);
        total_ll_error[i] = sqrt( pow(hSRboostedee[i]->GetBinError(1),2) + pow(hSRboostedemu[i]->GetBinError(1),2) + pow(hSRboostedmue[i]->GetBinError(1),2) + pow(hSRboostedmumu[i]->GetBinError(1),2) );
        if(i<12) {
            fout<<name_all[i]<<" & $"<<hSRboostedee[i]->GetBinContent(1)<<"\\pm"<<hSRboostedee[i]->GetBinError(1)<<"$ & $"<<hSRboostedemu[i]->GetBinContent(1)<<"\\pm"<<hSRboostedemu[i]->GetBinError(1)<<"$ & $"<<hSRboostedmue[i]->GetBinContent(1)<<"\\pm"<<hSRboostedmue[i]->GetBinError(1)<<"$ & $"<<hSRboostedmumu[i]->GetBinContent(1)<<"\\pm"<<hSRboostedmumu[i]->GetBinError(1)<<"$ & $"<<total_ll[i]<<"\\pm"<<total_ll_error[i]<<"$ \\\\"<<endl;
        total_ee += hSRboostedee[i]->GetBinContent(1);  total_ee_error += hSRboostedee[i]->GetBinError(1)*hSRboostedee[i]->GetBinError(1);
        total_emu += hSRboostedemu[i]->GetBinContent(1);  total_emu_error += hSRboostedemu[i]->GetBinError(1)*hSRboostedemu[i]->GetBinError(1);
        total_mue += hSRboostedmue[i]->GetBinContent(1);  total_mue_error += hSRboostedmue[i]->GetBinError(1)*hSRboostedmue[i]->GetBinError(1);
        total_mumu += hSRboostedmumu[i]->GetBinContent(1);  total_mumu_error += hSRboostedmumu[i]->GetBinError(1)*hSRboostedmumu[i]->GetBinError(1);
        }}

        else if (region=="SRResolved"){
        total_ll[i] = hSRresolvedee[i]->GetBinContent(1) + hSRresolvedemu[i]->GetBinContent(1) + hSRresolvedmue[i]->GetBinContent(1) + hSRresolvedmumu[i]->GetBinContent(1);
        total_ll_error[i] = sqrt( pow(hSRresolvedee[i]->GetBinError(1),2) + pow(hSRresolvedemu[i]->GetBinError(1),2) + pow(hSRresolvedmue[i]->GetBinError(1),2) + pow(hSRresolvedmumu[i]->GetBinError(1),2) );
        if(i<12) {
            fout<<name_all[i]<<" & $"<<hSRresolvedee[i]->GetBinContent(1)<<"\\pm"<<hSRresolvedee[i]->GetBinError(1)<<"$ & $"<<hSRresolvedemu[i]->GetBinContent(1)<<"\\pm"<<hSRresolvedemu[i]->GetBinError(1)<<"$ & $"<<hSRresolvedmue[i]->GetBinContent(1)<<"\\pm"<<hSRresolvedmue[i]->GetBinError(1)<<"$ & $"<<hSRresolvedmumu[i]->GetBinContent(1)<<"\\pm"<<hSRresolvedmumu[i]->GetBinError(1)<<"$ & $"<<total_ll[i]<<"\\pm"<<total_ll_error[i]<<"$ \\\\"<<endl;
        total_ee += hSRresolvedee[i]->GetBinContent(1);  total_ee_error += hSRresolvedee[i]->GetBinError(1)*hSRresolvedee[i]->GetBinError(1);
        total_emu += hSRresolvedemu[i]->GetBinContent(1);  total_emu_error += hSRresolvedemu[i]->GetBinError(1)*hSRresolvedemu[i]->GetBinError(1);
        total_mue += hSRresolvedmue[i]->GetBinContent(1);  total_mue_error += hSRresolvedmue[i]->GetBinError(1)*hSRresolvedmue[i]->GetBinError(1);
        total_mumu += hSRresolvedmumu[i]->GetBinContent(1);  total_mumu_error += hSRresolvedmumu[i]->GetBinError(1)*hSRresolvedmumu[i]->GetBinError(1);
        }}

        else if (region=="3lCR"){
         if(i<12) {
            fout<<name_all[i]<<" & $"<<hCR3lboosted[i]->GetBinContent(1)<<"\\pm"<<hCR3lboosted[i]->GetBinError(1)<<"$ & $"<<hCR3lresolved[i]->GetBinContent(1)<<"\\pm"<<hCR3lresolved[i]->GetBinError(1)<<"$ \\\\"<<endl;
          total_ee += hCR3lboosted[i]->GetBinContent(1);  total_ee_error += hCR3lboosted[i]->GetBinError(1)*hCR3lboosted[i]->GetBinError(1);
          total_emu += hCR3lresolved[i]->GetBinContent(1);  total_emu_error += hCR3lresolved[i]->GetBinError(1)*hCR3lresolved[i]->GetBinError(1);
        }}

        else if (region=="ssWWResolved"){
        total_ll[i] = hssWWresolvedee[i]->GetBinContent(1) + hssWWresolvedemu[i]->GetBinContent(1) + hssWWresolvedmue[i]->GetBinContent(1) + hssWWresolvedmumu[i]->GetBinContent(1);
        total_ll_error[i] = sqrt( pow(hssWWresolvedee[i]->GetBinError(1),2) + pow(hssWWresolvedemu[i]->GetBinError(1),2) + pow(hssWWresolvedmue[i]->GetBinError(1),2) + pow(hssWWresolvedmumu[i]->GetBinError(1),2) );
        if(i<12) {
            fout<<name_all[i]<<" & $"<<hssWWresolvedee[i]->GetBinContent(1)<<"\\pm"<<hssWWresolvedee[i]->GetBinError(1)<<"$ & $"<<hssWWresolvedemu[i]->GetBinContent(1)<<"\\pm"<<hssWWresolvedemu[i]->GetBinError(1)<<"$ & $"<<hssWWresolvedmue[i]->GetBinContent(1)<<"\\pm"<<hssWWresolvedmue[i]->GetBinError(1)<<"$ & $"<<hssWWresolvedmumu[i]->GetBinContent(1)<<"\\pm"<<hssWWresolvedmumu[i]->GetBinError(1)<<"$ & $"<<total_ll[i]<<"\\pm"<<total_ll_error[i]<<"$ \\\\"<<endl;
        total_ee += hssWWresolvedee[i]->GetBinContent(1);  total_ee_error += hssWWresolvedee[i]->GetBinError(1)*hssWWresolvedee[i]->GetBinError(1);
        total_emu += hssWWresolvedemu[i]->GetBinContent(1);  total_emu_error += hssWWresolvedemu[i]->GetBinError(1)*hssWWresolvedemu[i]->GetBinError(1);
        total_mue += hssWWresolvedmue[i]->GetBinContent(1);  total_mue_error += hssWWresolvedmue[i]->GetBinError(1)*hssWWresolvedmue[i]->GetBinError(1);
        total_mumu += hssWWresolvedmumu[i]->GetBinContent(1);  total_mumu_error += hssWWresolvedmumu[i]->GetBinError(1)*hssWWresolvedmumu[i]->GetBinError(1);
        }}
        
    }
    float total_yield = 0; float total_error = 0;
    for(int i=0; i<12; i++){
        total_yield  += total_ll[i];
        total_error += pow(total_ll_error[i],2);
    }
    fout<<"\\hline"<<endl;
    if(region=="3lCR") fout<<"Total Sum "<< " & $"<<total_ee<<"\\pm"<<sqrt(total_ee_error)<<"$ & $"<<total_emu<<"\\pm"<<sqrt(total_emu_error)<<"$ \\\\"<<endl;
    else fout<<"Total Sum "<< " & $"<<total_ee<<"\\pm"<<sqrt(total_ee_error)<<"$ & $"<<total_emu<<"\\pm"<<sqrt(total_emu_error)<<"$ & $"<<total_mue<<"\\pm"<<sqrt(total_mue_error)<<"$ & $"<<total_mumu<<"\\pm"<<sqrt(total_mumu_error)<<"$ & $"<<total_yield<<"\\pm"<<sqrt(total_error)<<"$ \\\\"<<endl;
   
    if(region!="SRBoosted" && region!="SRResolved") {
        if(region=="3lCR") fout<<"Data "<< " & $"<<hCR3lboosted[12]->GetBinContent(1)<<"\\pm"<<hCR3lboosted[12]->GetBinError(1)<<"$ & $"<<hCR3lresolved[12]->GetBinContent(1)<<"\\pm"<<hCR3lresolved[12]->GetBinError(1)<<"$ \\\\"<<endl;
        else if(region=="VRBoosted") fout<<"Data "<<" & $"<<hVRboostedee[12]->GetBinContent(1)<<"\\pm"<<hVRboostedee[12]->GetBinError(1)<<"$ & $"<<hVRboostedemu[12]->GetBinContent(1)<<"\\pm"<<hVRboostedemu[12]->GetBinError(1)<<"$ & $"<<hVRboostedmue[12]->GetBinContent(1)<<"\\pm"<<hVRboostedmue[12]->GetBinError(1)<<"$ & $"<<hVRboostedmumu[12]->GetBinContent(1)<<"\\pm"<<hVRboostedmumu[12]->GetBinError(1)<<"$ & $"<<total_ll[12]<<"\\pm"<<total_ll_error[12]<<"$ \\\\"<<endl;
        else if(region=="VRResolved") fout<<"Data "<<" & $"<<hVRresolvedee[12]->GetBinContent(1)<<"\\pm"<<hVRresolvedee[12]->GetBinError(1)<<"$ & $"<<hVRresolvedemu[12]->GetBinContent(1)<<"\\pm"<<hVRresolvedemu[12]->GetBinError(1)<<"$ & $"<<hVRresolvedmue[12]->GetBinContent(1)<<"\\pm"<<hVRresolvedmue[12]->GetBinError(1)<<"$ & $"<<hVRresolvedmumu[12]->GetBinContent(1)<<"\\pm"<<hVRresolvedmumu[12]->GetBinError(1)<<"$ & $"<<total_ll[12]<<"\\pm"<<total_ll_error[12]<<"$ \\\\"<<endl;
        else if(region=="ssWWResolved") fout<<"Data "<< " & $"<<hssWWresolvedee[12]->GetBinContent(1)<<"\\pm"<<hssWWresolvedee[12]->GetBinError(1)<<"$ & $"<<hssWWresolvedemu[12]->GetBinContent(1)<<"\\pm"<<hssWWresolvedemu[12]->GetBinError(1)<<"$ & $"<<hssWWresolvedmue[12]->GetBinContent(1)<<"\\pm"<<hssWWresolvedmue[12]->GetBinError(1)<<"$ & $"<<hssWWresolvedmumu[12]->GetBinContent(1)<<"\\pm"<<hssWWresolvedmumu[12]->GetBinError(1)<<"$ & $"<<hssWWresolvedInc[12]->GetBinContent(1)<<"\\pm"<<hssWWresolvedInc[12]->GetBinError(1)<<"$ \\\\"<<endl;
    }
    fout<<"\\hline\\hline"<<endl;
    fout<<"\\end{tabular}"<<endl;
    fout<<" "<<endl;
    fout<<" "<<endl;
    fout<<" "<<endl;


    // cout<<"bkg unc:"<<hAllBkg->Integral()<<endl;
    // float bkg[n_bins]; float bkg_unc[n_bins]; float sig[29][n_bins];
    // float used_bkg = 0;
   
    // TFile f1("SS2l_AddFlatWtaggerSFBin4","update");
    // f1.cd();
    // hsig->Write();
    // // hbkg1->Write();
    // f1.Close();
    // cout<<"GHH600X:"<<hsig_reg0[2]->Integral()+hsig_reg1[2]->Integral()+hsig_reg2[2]->Integral()<<endl;
    // cout<<"GHH600X:"<<hsig_reg0[2]->Integral()+hsig_reg1[2]->Integral()<<endl;
}
