from configManager import configMgr
from configWriter import fitConfig,Measurement,Channel,Sample
from logger import INFO, Logger
from systematic import Systematic
import os
from ROOT import gROOT
import ROOT

import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(1, '/afs/cern.ch/work/y/yuxu/public/HeavyHiggs/GHH_FW/HistFitterTutorial/HistFitter/MineTest/r04-02_final/')
from systematic_objects import getSystematicObjects

#---------------------------------------
# Analysis setup
#---------------------------------------
if 'mode' not in dir():
    mode = 'excl_Inclusive_sys'

if 'suffix' not in dir():
    suffix = 'GHH6f650f0'

if 'toydata' not in dir():
    toydata = 'dataAsimovGHH300fW0fWW6200mubestfit'

if 'variable' not in dir():
    variable = 'Meff'

analysis_name = 'GHH_' + str(mode) + '_' + str(suffix) + '_' +str(toydata)
print(analysis_name)

#RootName = 'GHH_SS2l_r04-02_sys_20210929_WZInc_sepWZ' # issue in histogram title
#RootName = 'GHH_SS2l_r04-02_sys_20211009_WZInc_WWWSF1p6_SepWZ'
#RootName = 'GHH_SS2l_r04-02_sys_20211017' #pdf and scale: largest wrt nominal
#RootName = 'GHH_SS2l_r04-02_sys_20211026_oldssWW' # old ssWW with GEPS uncertainty from MG5 ssWW with WWW removed
#RootName = 'GHH_SS2l_r04-02_sys_20211026_oldssWW_fullMG5ssWWForShapeSys' # old ssWW with GEPS uncertainty from full MG5 ssWW
#RootName = 'GHH_SS2l_r04-02_sys_20211122_noWWWSF' # no WWW SF applied, old ssWW
#RootName = 'GHH_SS2l_r04-02_sys_20211204_decorrelate' # no WWW SF applied, old ssWW, update PS, decorrelate
#RootName = 'GHH_SS2l_r04-02_sys_20220120_decorrelate_vars' # for plot more variables
#RootName = 'GHH_SS2l_r04-02_sys_20220214' # signal PS
#RootName = 'GHH_SS2l_r04-02_sys_20220218_toy'
RootName = 'GHH_SS2l_r04-02_sys_20220406_toy_fix'
NbinsBoosted=3
NbinsResolved=5

Nbins = 4
sysType = 'overallNormHisto'
toys = 'toys' in mode or 'Toys' in mode
bkgOnly = 'bkg' in mode or 'BKG' in mode
asimov = 'asimov' in mode or 'Asimov' in mode
exclMode = 'excl' in mode or 'EXCL' in mode
limitMode = 'lim' in mode or 'LIM' in mode
rankingMode = "Ranking" in mode 
disableSignal = bkgOnly and not 'sig' in mode

# other_bkg_tmp = 'other_'
other_bkg = 'Others'
#---------------------------------------
# Logger
#---------------------------------------

log = Logger(analysis_name)
log.setLevel(INFO)  # should have no effect if -L is used

#---------------------------------------
# Flags to control which fit is executed
#---------------------------------------
useStat = True  # statistics variation of samples

#-------------------------------
# Config manager basics
#-------------------------------

# Setting the parameters of the hypothesis test
configMgr.doExclusion = True  # True=exclusion, False=discovery
if toys:
    configMgr.nTOYs=5000
    configMgr.calculatorType = 0
else:
    configMgr.calculatorType = 2
configMgr.testStatType = 3
configMgr.nPoints = 40
# configMgr.scanRange = (0, 1)

configMgr.writeXML = True
configMgr.analysisName = analysis_name
configMgr.histCacheFile = 'data/'+RootName+'_'+mode+'_'+suffix+'_'+toydata+'.root'
configMgr.outputFileName = 'results/'+RootName+'_'+mode+'_'+suffix+'_'+toydata+'_Output.root'

# activate using of background histogram cache file to speed up processes
configMgr.useCacheToTreeFallback = False  # enable the fallback to trees
# enable the use of an alternate data file
configMgr.useHistBackupCacheFile = True
# histogram templates - the data file of your previous fit, backup cache
configMgr.histBackupCacheFile = 'data/backupCacheFile_'+RootName+'.root'
configMgr.useCache = True
# Scaling calculated by outputLumi / inputLumi
configMgr.inputLumi = 138.96516  # Luminosity of input TTree after weighting
configMgr.outputLumi = 138.96516  # Luminosity required for output histograms
configMgr.setLumiUnits('fb-1')

# blind
configMgr.blindSR = False
if asimov:
    print('Using Asimov Dataset!')
    configMgr.useAsimovSet=True

if rankingMode:
    print('INJECTING SIGNAL!')
    configMgr.useSignalInBlindedData = True

#configMgr.prun = True
#configMgr.prunThreshold = 0.01
#-------------------------------------
# Now we start to build the data model
#-------------------------------------

# Systematics
if 'sys' in mode:
    # for definitions see attached systematic_objects.py
    # still work in progress
    sys_dict=getSystematicObjects()

    sysNamesChargeFlip = sys_dict['sysNamesChargeFlip']
    sysNamesVgamma = sys_dict['sysNamesVgamma']
    sysNamesNonPrompt = sys_dict['sysNamesNonPrompt']
    sysNamesCommon = sys_dict['sysNamesCommon']
    #sysNamesCommon = []
    sysNamesCommonFullSim = sys_dict['sysNamesCommonFullSim']
    sysNamesCommonFastSim = sys_dict['sysNamesCommonFastSim']
    sysNamesCommonOneSidedSym = sys_dict['sysNamesCommonOneSidedSym']
    #sysNamesCommonOneSidedSym = []
    sysNamesCommonOneSidedFullSim = sys_dict['sysNamesCommonOneSidedFullSim']
    sysNamesCommonOneSidedFastSim = sys_dict['sysNamesCommonOneSidedFastSim']
    sysNamesElectron = sys_dict['sysNamesElectron']
    sysNamesElectronFastSim = sys_dict['sysNamesElectronFastSim']
    sysNamesMuon = sys_dict['sysNamesMuon']
    sysNamesMuonOneSidedSym = sys_dict['sysNamesMuonOneSidedSym']
    sysNamesLargeRJets = sys_dict['sysNamesLargeRJets']
    sysNamesBJTEffSF = sys_dict['sysNamesBJTEffSF']
    sysNamesBJTInEffSF = sys_dict['sysNamesBJTInEffSF']
    #sysNamesLargeRJets = []
    sysNamesLargeRJetsOneSidedSym = sys_dict['sysNamesLargeRJetsOneSidedSym']
    #sysNamesLargeRJetsOneSidedSym = []
    sysNamesSmallRJets = sys_dict['sysNamesSmallRJets']
    #sysNamesSmallRJets = []
    sysNamesSmallRJetsOneSidedSym = sys_dict['sysNamesSmallRJetsOneSidedSym']
    #sysNamesSmallRJetsOneSidedSym = []
    #sysNamesTTbarTheory = sys_dict['sysNamesTTbarTheory']
    #sysNamesTTbarTheoryOneSidedSym = sys_dict['sysNamesTTbarTheoryOneSidedSym']
    #sysNamesDibosonTheory = sys_dict['sysNamesDibosonTheory']
    sysNamesWZTheoryOneSidedSym = sys_dict['sysNamesWZTheoryOneSidedSym']
    sysNamesWZTheoryTwoSidedSym = sys_dict['sysNamesWZTheoryTwoSidedSym']
    sysNamesssWWTheoryOneSidedSym = sys_dict['sysNamesssWWTheoryOneSidedSym']
    sysNamesWWWTheoryOneSidedSym = sys_dict['sysNamesWWWTheoryOneSidedSym']
    sysNamesWWWTheoryTwoSidedSym = sys_dict['sysNamesWWWTheoryTwoSidedSym']
    sysNames2ndSigTheoryOneSidedSym = sys_dict['sysNames2ndSigTheoryOneSidedSym']
    sysNames3rdSigTheoryOneSidedSym = sys_dict['sysNames3rdSigTheoryOneSidedSym']
    # do_small_sys = True
    # if do_small_sys:
    #     print("doing small")
    #     sysNamesCommon = sysNamesCommon[:1]
    #     sysNamesCommonFullSim = sysNamesCommonFullSim[:1]
    #     sysNamesCommonFastSim = sysNamesCommonFastSim[:1]
    #     sysNamesCommonOneSidedSym = sysNamesCommonOneSidedSym[:1]
    #     sysNamesCommonOneSidedFullSim = sysNamesCommonOneSidedFullSim[:1]
    #     sysNamesCommonOneSidedFastSim = sysNamesCommonOneSidedFastSim[:1]
    #     sysNamesElectron = sysNamesElectron[:1]
    #     sysNamesElectronFastSim = sysNamesElectronFastSim[:1]
    #     sysNamesMuon = sysNamesMuon[:1]
    #     sysNamesLargeRJets = sysNamesLargeRJets[:1]
    #     sysNamesLargeRJetsOneSidedSym = sysNamesLargeRJetsOneSidedSym[:1]
    #     sysNamesSmallRJets = sysNamesSmallRJets[:1]
    #     sysNamesSmallRJetsOneSidedSym = sysNamesSmallRJetsOneSidedSym[:1]
    #     sysNamesTTbarTheory = sysNamesTTbarTheory[:1]
    #     sysNamesTTbarTheoryOneSidedSym = sysNamesTTbarTheoryOneSidedSym[:1]
    #     sysNamesDibosonTheory = sysNamesDibosonTheory[:1]


    # ToDo: Figure out Fast and Full division. For now use all systematics as both
    #sysNamesCommonFastSim = sysNamesCommon
    #sysNamesCommonFullSim = sysNamesCommon
    #sysNamesCommonOneSidedFastSim = sysNamesCommonOneSidedSym
    #sysNamesCommonOneSidedFullSim = sysNamesCommonOneSidedSym
    #sysNamesElectronFastSim = sysNamesElectron
else:
    sysNamesChargeFlip = []
    sysNamesVgamma = []
    sysNamesNonPrompt = []
    sysNamesCommon = []
    sysNamesCommonFullSim = []
    sysNamesCommonFastSim = []
    sysNamesCommonOneSidedSym = []
    sysNamesCommonOneSidedFullSim = []
    sysNamesCommonOneSidedFastSim = []
    sysNamesElectron = []
    sysNamesElectronFastSim = []
    sysNamesMuon = []
    sysNamesMuonOneSidedSym = []
    sysNamesBJTEffSF = []
    sysNamesBJTInEffSF = []
    sysNamesLargeRJets = []
    sysNamesLargeRJetsOneSidedSym = []
    sysNamesSmallRJets = []
    sysNamesSmallRJetsOneSidedSym = []
    sysNamesWZTheoryOneSidedSym = []
    sysNamesWZTheoryTwoSidedSym = []
    sysNamesssWWTheoryOneSidedSym = []
    sysNamesWWWTheoryOneSidedSym = []
    sysNamesWWWTheoryTwoSidedSym = []
    sysNames2ndSigTheoryOneSidedSym = []
    sysNames3rdSigTheoryOneSidedSym = []
# Dictionnary of cuts defining channels/regions (for Tree->hist)
if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
    configMgr.cutsDict["CR3lBoostedInc"] = "1."
    configMgr.cutsDict["CR3lResolvedInc"] = "1."
    configMgr.cutsDict["ssWWResolvedInc"] = "1."
    configMgr.cutsDict["SRSS2lBoostedInc"] = "1."

if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
    configMgr.cutsDict["CR3lBoostedInc"] = "1."
    configMgr.cutsDict["CR3lResolvedInc"] = "1."
    configMgr.cutsDict["ssWWResolvedInc"] = "1."
    configMgr.cutsDict["SRSS2lResolvedInc"] = "1."
if "Inclusive" in mode:
    configMgr.cutsDict["CR3lBoostedInc"] = "1."
    configMgr.cutsDict["CR3lResolvedInc"] = "1."
    configMgr.cutsDict["ssWWResolvedInc"] = "1."
    configMgr.cutsDict["SRSS2lBoostedInc"] = "1."
    configMgr.cutsDict["SRSS2lResolvedInc"] = "1."
  #   if "ll" in mode:
	 # configMgr.cutsDict["SRSS2lBoostedee"] = "1."
	 # configMgr.cutsDict["SRSS2lBoostedemu"] = "1."
	 # configMgr.cutsDict["SRSS2lBoostedmue"] = "1."
	 # configMgr.cutsDict["SRSS2lBoostedmumu"] = "1."
	 # configMgr.cutsDict["SRSS2lResolvedee"] = "1."
	 # configMgr.cutsDict["SRSS2lResolvedemu"] = "1."
	 # configMgr.cutsDict["SRSS2lResolvedmue"] = "1."
	 # configMgr.cutsDict["SRSS2lResolvedmumu"] = "1."

if bkgOnly:
    if "DiLepton" in mode or "AllChannel" in mode or "Boosted" in mode:
        configMgr.cutsDict["VRSS2lBoostedee"] = "1."
        configMgr.cutsDict["VRSS2lBoostedemu"] = "1."
        configMgr.cutsDict["VRSS2lBoostedmue"] = "1."
        configMgr.cutsDict["VRSS2lBoostedmumu"] = "1."
    if "DiLepton" in mode or "AllChannel" in mode or "Resolved" in mode:
        configMgr.cutsDict["VRSS2lResolvedee"] = "1."
        configMgr.cutsDict["VRSS2lResolvedemu"] = "1."
        configMgr.cutsDict["VRSS2lResolvedmue"] = "1."
        configMgr.cutsDict["VRSS2lResolvedmumu"] = "1."
    if "Inclusive" in mode:
        configMgr.cutsDict["VRSS2lBoostedInc"] = "1."
        configMgr.cutsDict["VRSS2lResolvedInc"] = "1."

# Define weights - dummy here
configMgr.weights = '1.'

# nominal name of the histograms with systematic variation
configMgr.nomName = 'Nom_'

#-------------------------------------------
# List of samples and their plotting colours
#-------------------------------------------

from ROOT import kGreen, kYellow, kViolet, kBlack, kBlue, kRed, kCyan, kAzure, kOrange, kMagenta

VVVSample2l = Sample("VVV", kOrange-9)
VVVSample2l.setNormByTheory()  # scales with lumi
VVVSample2l.setStatConfig(useStat)
for sysName in sysNamesCommon:
   VVVSample2l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
for sysName in sysNamesCommonFullSim:
   VVVSample2l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
for sysName in sysNamesCommonOneSidedSym:
   VVVSample2l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
   VVVSample2l.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))

#WZSample = Sample("WZ", kMagenta-7)
#WZSample.setNormByTheory()
#WZSample.setStatConfig(useStat)
#WZSample.setNormFactor('mu_WZ', 1., 0., 5.)
#WZSample.setNormRegions([("CR3lBoostedInc", variable), ("CR3lResolvedInc", variable)])
#for sysName in sysNamesCommon:
#    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#for sysName in sysNamesCommonFullSim:
#    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
#for sysName in sysNamesCommonOneSidedSym:
#    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
#for sysName in sysNamesCommonOneSidedFullSim:
#    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
#for sysName in sysNamesWZTheoryOneSidedSym:
#    WZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

WZBoostedSample = Sample("WZBoosted", kMagenta-7)
WZBoostedSample.setNormByTheory()
WZBoostedSample.setStatConfig(useStat)
WZBoostedSample.setNormFactor('mu_WZBoosted', 1., 0., 5.)
WZBoostedSample.setNormRegions([("CR3lBoostedInc", variable)])
for sysName in sysNamesCommon:
    WZBoostedSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonFullSim:
    WZBoostedSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonOneSidedSym:
    WZBoostedSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
    WZBoostedSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

WZResolvedSample = Sample("WZResolved", kMagenta+2)
WZResolvedSample.setNormByTheory()
WZResolvedSample.setStatConfig(useStat)
WZResolvedSample.setNormFactor('mu_WZResolved', 1., 0., 5.)
WZResolvedSample.setNormRegions([("CR3lResolvedInc", variable)])
for sysName in sysNamesCommon:
    WZResolvedSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonFullSim:
    WZResolvedSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonOneSidedSym:
    WZResolvedSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
    WZResolvedSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

TopXSample = Sample("TopX",ROOT.kBlue-9)
TopXSample.setNormByTheory()
TopXSample.setStatConfig(True)
for sysName in sysNamesCommon:
   TopXSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
for sysName in sysNamesCommonFullSim:
   TopXSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
for sysName in sysNamesCommonOneSidedSym:
   TopXSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
   TopXSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))

NonPromptElSample = Sample("NonPromptEl",ROOT.kAzure+10)
NonPromptElSample.setStatConfig(True)

NonPromptMuSample = Sample("NonPromptMu",ROOT.kAzure-4)
NonPromptMuSample.setStatConfig(True)

ChargeFlipSample = Sample("ChargeFlip",ROOT.kGreen-7)
ChargeFlipSample.setStatConfig(True)

VgammaSample = Sample("PhotonConversion",ROOT.kOrange+1)
VgammaSample.setStatConfig(True)

WWWSample = Sample("WWW",ROOT.kYellow)
WWWSample.setNormByTheory()
WWWSample.setStatConfig(True)
for sysName in sysNamesCommon:
   WWWSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
for sysName in sysNamesCommonFullSim:
   WWWSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
for sysName in sysNamesCommonOneSidedSym:
   WWWSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
   WWWSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
#WWWSample.addSystematic(Systematic('WWWNorm', configMgr.weights, 1.60, 0.40, 'user', 'userHistoSys'))

ssWWSample = Sample("ssWW",ROOT.kOrange-7)
ssWWSample.setNormByTheory()
ssWWSample.setStatConfig(True)
ssWWSample.setNormFactor('mu_ssWW', 1., 0., 10.)
ssWWSample.setNormRegions([("ssWWResolvedInc", variable)])
for sysName in sysNamesCommon:
    ssWWSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonFullSim:
    ssWWSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
for sysName in sysNamesCommonOneSidedSym:
    ssWWSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
    ssWWSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

ZZSample = Sample("ZZ",ROOT.kMagenta-10)
ZZSample.setNormByTheory()
ZZSample.setStatConfig(True)
for sysName in sysNamesCommon:
   ZZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
for sysName in sysNamesCommonFullSim:
   ZZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
for sysName in sysNamesCommonOneSidedSym:
   ZZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
for sysName in sysNamesCommonOneSidedFullSim:
   ZZSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))

# Signal samples
if not disableSignal:
    sigName = suffix
    if suffix == 'default':
        sigName = 'GHH6f650f0'

    sigSample = Sample(sigName, kRed-4)
    sigSample.setNormByTheory()
    sigSample.setStatConfig(useStat)
    sigSample.setNormFactor('mu_SIG', 1., 0., 10.)
   # sigSample.addSampleSpecificWeight("1.3")

    for sysName in sysNamesCommon:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonFastSim:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesCommonOneSidedSym:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    for sysName in sysNamesCommonOneSidedFastSim:
        sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    if 'fWW' in sigName:
	 for sysName in sysNames3rdSigTheoryOneSidedSym:
	    sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    else:
	 for sysName in sysNames2ndSigTheoryOneSidedSym:  
	    sigSample.addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
            
# data
dataSample = Sample(toydata, kBlack)
dataSample.setData()


#**************
# fit
#**************
commonSamples = [dataSample, WZBoostedSample, WZResolvedSample, WWWSample, VVVSample2l, ZZSample, TopXSample, ChargeFlipSample, NonPromptElSample, NonPromptMuSample, VgammaSample, ssWWSample]

if not disableSignal:
    commonSamples += [sigSample]

# Parameters of the Measurement
measurementName = 'NormalMeasurement'
measurementLumi = 1.
if 'sys' in mode:
    measurementLumiError = 0.017  # 2015+16+17+18
else:
    measurementLumiError = 0.0001

if bkgOnly:
    fitConfig = configMgr.addFitConfig('Template_BkgOnly')
elif exclMode:
    fitConfig = configMgr.addFitConfig('Exclusion')
elif rankingMode:
    fitConfig = configMgr.addFitConfig('Ranking')

if useStat:
    fitConfig.statErrThreshold = 0.01  # values above this will be considered in the fit
else:
    fitConfig.statErrThreshold = None
fitConfig.addSamples(commonSamples)
measurement = fitConfig.addMeasurement(measurementName, measurementLumi, measurementLumiError)

if not disableSignal:
    fitConfig.setSignalSample(sigSample)
measurement.addPOI('mu_SIG')

# Regions
# region.hasB = False
# region.hasBQCD = False
# region.useOverflowBin = False

if "Boosted" in mode:
    region_3lBoostedCR_Inc = fitConfig.addChannel(variable, ['CR3lBoostedInc'], 1, 200, 3000)
    region_3lResolvedCR_Inc = fitConfig.addChannel(variable, ['CR3lResolvedInc'], 1, 200, 3000)
    region_CRssWW_Inc = fitConfig.addChannel(variable, ['ssWWResolvedInc'], 1, 200, 1000)
    region_SS2lBoostedSR_Inc = fitConfig.addChannel(variable, ['SRSS2lBoostedInc'], NbinsBoosted, 0, NbinsBoosted)

if "Resolved" in mode:
    region_3lBoostedCR_Inc = fitConfig.addChannel(variable, ['CR3lBoostedInc'], 1, 200, 3000)
    region_3lResolvedCR_Inc = fitConfig.addChannel(variable, ['CR3lResolvedInc'], 1, 200, 3000)
    region_CRssWW_Inc = fitConfig.addChannel(variable, ['ssWWResolvedInc'], 1, 200, 1000)
    region_SS2lResolvedSR_Inc = fitConfig.addChannel(variable, ['SRSS2lResolvedInc'], NbinsResolved, 0, NbinsResolved)

if "Inclusive" in mode:
    region_3lBoostedCR_Inc = fitConfig.addChannel(variable, ['CR3lBoostedInc'], 1, 200, 3000)
    region_3lResolvedCR_Inc = fitConfig.addChannel(variable, ['CR3lResolvedInc'], 1, 200, 3000)
    region_CRssWW_Inc = fitConfig.addChannel(variable, ['ssWWResolvedInc'], 1, 200, 1000)
    region_SS2lBoostedSR_Inc = fitConfig.addChannel(variable, ['SRSS2lBoostedInc'], NbinsBoosted, 0, NbinsBoosted)
    region_SS2lResolvedSR_Inc = fitConfig.addChannel(variable, ['SRSS2lResolvedInc'], NbinsResolved, 0, NbinsResolved)
    if "vars" in mode:
         region_SS2lBoostedSR_Inc_Lep1Pt  = fitConfig.addChannel('Lep1Pt', ['SRSS2lBoostedInc'], 20, 0, 600)
         region_SS2lBoostedSR_Inc_Lep2Pt  = fitConfig.addChannel('Lep2Pt', ['SRSS2lBoostedInc'], 20, 0, 400)
         region_SS2lBoostedSR_Inc_fatjet1Pt  = fitConfig.addChannel('fatjet1Pt', ['SRSS2lBoostedInc'], 20, 0, 1000)
         region_SS2lBoostedSR_Inc_MET  = fitConfig.addChannel('MET', ['SRSS2lBoostedInc'], 20, 0, 600)
         region_SS2lBoostedSR_Inc_mll  = fitConfig.addChannel('mll', ['SRSS2lBoostedInc'], 40, 0, 2000)
         region_SS2lResolvedSR_Inc_Lep1Pt = fitConfig.addChannel('Lep1Pt', ['SRSS2lResolvedInc'], 20, 0, 600)
         region_SS2lResolvedSR_Inc_Lep2Pt  = fitConfig.addChannel('Lep2Pt', ['SRSS2lResolvedInc'], 20, 0, 400)
         region_SS2lResolvedSR_Inc_jet1Pt  = fitConfig.addChannel('jet1Pt', ['SRSS2lResolvedInc'], 20, 0, 800)
         region_SS2lResolvedSR_Inc_jet2Pt  = fitConfig.addChannel('jet2Pt', ['SRSS2lResolvedInc'], 20, 0, 600)
         region_SS2lResolvedSR_Inc_MET  = fitConfig.addChannel('MET', ['SRSS2lResolvedInc'], 20, 0, 600)
         region_SS2lResolvedSR_Inc_mll  = fitConfig.addChannel('mll', ['SRSS2lResolvedInc'], 40, 0, 2000)
    if "ll" in mode:
	 region_SS2lBoostedSR_ee = fitConfig.addChannel(variable, ['SRSS2lBoostedee'], NbinsBoosted, 0, NbinsBoosted)
	 region_SS2lBoostedSR_em = fitConfig.addChannel(variable, ['SRSS2lBoostedemu'], NbinsBoosted, 0, NbinsBoosted)
	 region_SS2lBoostedSR_me = fitConfig.addChannel(variable, ['SRSS2lBoostedmue'], NbinsBoosted, 0, NbinsBoosted)
	 region_SS2lBoostedSR_mm = fitConfig.addChannel(variable, ['SRSS2lBoostedmumu'], NbinsBoosted, 0, NbinsBoosted)
	 region_SS2lResolvedSR_ee = fitConfig.addChannel(variable, ['SRSS2lResolvedee'], NbinsResolved, 0, NbinsResolved)
	 region_SS2lResolvedSR_em = fitConfig.addChannel(variable, ['SRSS2lResolvedemu'], NbinsResolved, 0, NbinsResolved)
	 region_SS2lResolvedSR_me = fitConfig.addChannel(variable, ['SRSS2lResolvedmue'], NbinsResolved, 0, NbinsResolved)
	 region_SS2lResolvedSR_mm = fitConfig.addChannel(variable, ['SRSS2lResolvedmumu'], NbinsResolved, 0, NbinsResolved)

if "bkg" in mode:
#    if "Boosted" in mode:
#        region_SS2lBoostedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lBoostedInc'], NbinsBoosted, 0, NbinsBoosted)
#    if "Resolved" in mode:
#        region_SS2lResolvedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lResolvedInc'], NbinsResolved, 0, NbinsResolved)
#    if "Inclusive" in mode:
#        region_SS2lBoostedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lBoostedInc'], NbinsBoosted, 0, NbinsBoosted)
#        region_SS2lResolvedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lResolvedInc'], NbinsResolved, 0, NbinsResolved)
#        if "ll" in mode:
#	     region_SS2lBoostedVR_ee = fitConfig.addChannel(variable, ['VRSS2lBoostedee'], NbinsBoosted, 0, NbinsBoosted)
#	     region_SS2lBoostedVR_em = fitConfig.addChannel(variable, ['VRSS2lBoostedemu'], NbinsBoosted, 0, NbinsBoosted)
#	     region_SS2lBoostedVR_me = fitConfig.addChannel(variable, ['VRSS2lBoostedmue'], NbinsBoosted, 0, NbinsBoosted)
#	     region_SS2lBoostedVR_mm = fitConfig.addChannel(variable, ['VRSS2lBoostedmumu'], NbinsBoosted, 0, NbinsBoosted)
#	     region_SS2lResolvedVR_ee = fitConfig.addChannel(variable, ['VRSS2lResolvedee'], NbinsResolved, 0, NbinsResolved)
#	     region_SS2lResolvedVR_em = fitConfig.addChannel(variable, ['VRSS2lResolvedemu'], NbinsResolved, 0, NbinsResolved)
#	     region_SS2lResolvedVR_me = fitConfig.addChannel(variable, ['VRSS2lResolvedmue'], NbinsResolved, 0, NbinsResolved)
#	     region_SS2lResolvedVR_mm = fitConfig.addChannel(variable, ['VRSS2lResolvedmumu'], NbinsResolved, 0, NbinsResolved)
    ########## equal bin width #####################
    if "Boosted" in mode:
        region_SS2lBoostedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lBoostedInc'], Nbins, 0, Nbins) # 400, 2000 GeV
    if "Resolved" in mode:
        region_SS2lResolvedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lResolvedInc'], Nbins, 0, Nbins) # 200, 1000 GeV
    if "Inclusive" in mode:
        region_SS2lBoostedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lBoostedInc'], NbinsBoosted, 0, NbinsBoosted)
        region_SS2lResolvedVR_Inc = fitConfig.addChannel(variable, ['VRSS2lResolvedInc'], NbinsResolved, 0, NbinsResolved)
        if "ll" in mode:
	     region_SS2lBoostedVR_ee = fitConfig.addChannel(variable, ['VRSS2lBoostedee'], Nbins, 0, Nbins)
	     region_SS2lBoostedVR_em = fitConfig.addChannel(variable, ['VRSS2lBoostedemu'], Nbins, 0, Nbins)
	     region_SS2lBoostedVR_me = fitConfig.addChannel(variable, ['VRSS2lBoostedmue'], Nbins, 0, Nbins)
	     region_SS2lBoostedVR_mm = fitConfig.addChannel(variable, ['VRSS2lBoostedmumu'], Nbins, 0, Nbins)
	     region_SS2lResolvedVR_ee = fitConfig.addChannel(variable, ['VRSS2lResolvedee'], Nbins, 0, Nbins)
	     region_SS2lResolvedVR_em = fitConfig.addChannel(variable, ['VRSS2lResolvedemu'], Nbins, 0, Nbins)
	     region_SS2lResolvedVR_me = fitConfig.addChannel(variable, ['VRSS2lResolvedmue'], Nbins, 0, Nbins)
	     region_SS2lResolvedVR_mm = fitConfig.addChannel(variable, ['VRSS2lResolvedmumu'], Nbins, 0, Nbins)

has_lepton = []
has_largeRjets = []
has_smallRjets = []
has_noNFs = []
has_PassWtagger = []
has_FailWtagger = []

if "Boosted" in mode:
    has_lepton += [
	  region_CRssWW_Inc,
	  region_3lResolvedCR_Inc,
	  region_3lBoostedCR_Inc,
        region_SS2lBoostedSR_Inc,
    ]
    has_largeRjets += [
	  region_3lBoostedCR_Inc,
        region_SS2lBoostedSR_Inc,
    ]
    has_smallRjets += [
	  region_CRssWW_Inc,
	  region_3lResolvedCR_Inc,
    ]
    has_noNFs += [region_SS2lBoostedSR_Inc]
    has_PassWtagger += [region_SS2lBoostedSR_Inc]
if "Resolved" in mode:
    has_lepton += [
	  region_CRssWW_Inc,
	  region_3lResolvedCR_Inc,
	  region_3lBoostedCR_Inc,
        region_SS2lResolvedSR_Inc,
    ]
    has_largeRjets += [
	    region_3lBoostedCR_Inc,
    ]
    has_smallRjets += [
	  region_CRssWW_Inc,
	  region_3lResolvedCR_Inc,
	  region_SS2lResolvedSR_Inc,
    ]
    has_noNFs += [region_SS2lResolvedSR_Inc]
if "Inclusive" in mode:
    has_lepton += [
        region_3lBoostedCR_Inc,
        region_3lResolvedCR_Inc,
        region_CRssWW_Inc,
        region_SS2lBoostedSR_Inc,
        region_SS2lResolvedSR_Inc,
    ]
    has_largeRjets += [
	  region_3lBoostedCR_Inc,
        region_SS2lBoostedSR_Inc,
    ]
    has_smallRjets += [
	  region_CRssWW_Inc,
	  region_3lResolvedCR_Inc,
	  region_SS2lResolvedSR_Inc,
    ]
    has_noNFs += [region_SS2lBoostedSR_Inc, region_SS2lResolvedSR_Inc]
    has_PassWtagger += [region_SS2lBoostedSR_Inc]
    if "vars" in mode:
        has_lepton += [region_SS2lBoostedSR_Inc_Lep1Pt, region_SS2lBoostedSR_Inc_Lep2Pt, region_SS2lBoostedSR_Inc_fatjet1Pt, region_SS2lBoostedSR_Inc_MET, region_SS2lBoostedSR_Inc_mll, region_SS2lResolvedSR_Inc_Lep1Pt, region_SS2lResolvedSR_Inc_Lep2Pt, region_SS2lResolvedSR_Inc_jet1Pt, region_SS2lResolvedSR_Inc_jet2Pt, region_SS2lResolvedSR_Inc_MET, region_SS2lResolvedSR_Inc_mll]
        has_largeRjets += [region_SS2lBoostedSR_Inc_Lep1Pt, region_SS2lBoostedSR_Inc_Lep2Pt, region_SS2lBoostedSR_Inc_fatjet1Pt, region_SS2lBoostedSR_Inc_MET, region_SS2lBoostedSR_Inc_mll]
        has_smallRjets += [region_SS2lResolvedSR_Inc_Lep1Pt, region_SS2lResolvedSR_Inc_Lep2Pt, region_SS2lResolvedSR_Inc_jet1Pt, region_SS2lResolvedSR_Inc_jet2Pt, region_SS2lResolvedSR_Inc_MET, region_SS2lResolvedSR_Inc_mll]
        has_noNFs += [region_SS2lBoostedSR_Inc_Lep1Pt, region_SS2lBoostedSR_Inc_Lep2Pt, region_SS2lBoostedSR_Inc_fatjet1Pt, region_SS2lBoostedSR_Inc_MET, region_SS2lBoostedSR_Inc_mll, region_SS2lResolvedSR_Inc_Lep1Pt, region_SS2lResolvedSR_Inc_Lep2Pt, region_SS2lResolvedSR_Inc_jet1Pt, region_SS2lResolvedSR_Inc_jet2Pt, region_SS2lResolvedSR_Inc_MET, region_SS2lResolvedSR_Inc_mll]
        has_PassWtagger += [region_SS2lBoostedSR_Inc_Lep1Pt, region_SS2lBoostedSR_Inc_Lep2Pt, region_SS2lBoostedSR_Inc_fatjet1Pt, region_SS2lBoostedSR_Inc_MET, region_SS2lBoostedSR_Inc_mll]

if "bkg" in mode:
    if "Boosted" in mode:
        has_lepton += [
            region_SS2lBoostedVR_Inc,
        ]
        has_largeRjets += [
            region_SS2lBoostedVR_Inc,
        ]
        has_noNFs += [region_SS2lBoostedVR_Inc]
        has_FailWtagger += [region_SS2lBoostedVR_Inc]
    if "Resolved" in mode:
        has_lepton += [
            region_SS2lResolvedVR_Inc,
        ]
        has_smallRjets += [
            region_SS2lResolvedVR_Inc,
        ]
        has_noNFs += [region_SS2lResolvedVR_Inc]
    if "Inclusive" in mode:
        has_lepton += [
            region_SS2lBoostedVR_Inc,
            region_SS2lResolvedVR_Inc,
        ]
        has_largeRjets += [
            region_SS2lBoostedVR_Inc,
        ]
        has_smallRjets += [
            region_SS2lResolvedVR_Inc,
        ]
        has_noNFs += [region_SS2lBoostedVR_Inc, region_SS2lResolvedVR_Inc]
        has_FailWtagger += [region_SS2lBoostedVR_Inc]

for region in has_noNFs:
   for sysName in sysNamesWZTheoryOneSidedSym:
      if 'Boosted' in sysName:
	   region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
      if 'Resolved' in sysName:
	   region.getSample('WZResolved').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
   for sysName in sysNamesWZTheoryTwoSidedSym:
      if 'Boosted' in sysName:
	   region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
      if 'Resolved' in sysName:
	   region.getSample('WZResolved').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
   for sysName in sysNamesssWWTheoryOneSidedSym:
      if 'Boosted' in sysName and 'Boosted' in region.regionString:
	   region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
      if 'Resolved' in sysName and 'Resolved' in region.regionString:
	   region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

for region in has_largeRjets:
    for sysName in sysNamesWWWTheoryOneSidedSym:
      if 'Boosted' in sysName:
	   region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
    for sysName in sysNamesWWWTheoryTwoSidedSym:
      if 'Boosted' in sysName:
	   region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesLargeRJets:
        region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesLargeRJetsOneSidedSym:
        region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
        region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
        region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

if "Inclusive" in mode or "Boosted" in mode:
    for region in has_PassWtagger:
        for sysName in sysNamesBJTEffSF:
            region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
            region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
            region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
            region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
            region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
            region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
            if not disableSignal:
                region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))

    if "bkg" in mode:
        for region in has_FailWtagger:
            for sysName in sysNamesBJTInEffSF:
                region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
                region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
                region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
                region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
                region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
                region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
                if not disableSignal:
                    region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))


for region in has_smallRjets:
    for sysName in sysNamesWWWTheoryOneSidedSym:
        if 'Resolved' in sysName:
	      region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
    for sysName in sysNamesWWWTheoryTwoSidedSym:
        if 'Resolved' in sysName:
	      region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
    for sysName in sysNamesSmallRJets:
        region.getSample('WZResolved').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesSmallRJetsOneSidedSym:
        region.getSample('WZResolved').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
        region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
        region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))

for region in has_lepton:
    for sysName in sysNamesElectron:
        if region in has_largeRjets:
            region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        if region in has_smallRjets:
            region.getSample('WZResolved').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesElectronFastSim:
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesMuon:
        if region in has_largeRjets:
            region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        if region in has_smallRjets:
            region.getSample('WZResolved').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
        region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSys'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'Sys'))
    for sysName in sysNamesMuonOneSidedSym:
        if region in has_largeRjets:
            region.getSample('WZBoosted').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
        if region in has_smallRjets:
            region.getSample('WZResolved').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
        region.getSample('ssWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
        region.getSample('VVV').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('TopX').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('WWW').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('ZZ').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        if not disableSignal:
            region.getSample(sigName).addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', sysType + 'SysOneSideSym'))
    if 'sys' in mode:
        if region != region_3lBoostedCR_Inc and region !=region_3lResolvedCR_Inc:
	     region.getSample('NonPromptEl').addSystematic(Systematic('FAKES_Electron', configMgr.weights, 1.15, 0.85, 'user', 'userHistoSys'))
	     region.getSample('NonPromptMu').addSystematic(Systematic('FAKES_Muon', configMgr.weights, 1.10, 0.90, 'user', 'userHistoSys'))
	     region.getSample('ChargeFlip').addSystematic(Systematic('ChargeFlipRate', configMgr.weights, 1.025, 0.975, 'user', 'userHistoSys'))
	     region.getSample('PhotonConversion').addSystematic(Systematic('PhotonConversionRate', configMgr.weights, 1.09, 0.91, 'user', 'userHistoSys'))
	     for sysName in sysNamesChargeFlip:
		     region.getSample('ChargeFlip').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
	     for sysName in sysNamesVgamma:
		     region.getSample('PhotonConversion').addSystematic(Systematic(sysName, 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
	     for sysName in sysNamesNonPrompt:
		     region.getSample('NonPromptEl').addSystematic(Systematic(sysName+'El', 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
		     region.getSample('NonPromptMu').addSystematic(Systematic(sysName+'Mu', 'Nom', 'dummy', 'dummy', 'tree', 'histoSysOneSideSym'))
        region.getSample('VVV').addSystematic(Systematic('SYSVVV', configMgr.weights, 1.15, 0.85, 'user', 'userHistoSys'))
        region.getSample('ZZ').addSystematic(Systematic('SYSZZ', configMgr.weights, 1.20, 0.80, 'user', 'userHistoSys'))
        region.getSample('TopX').addSystematic(Systematic('SYSTopX', configMgr.weights, 1.15, 0.85, 'user', 'userHistoSys'))
        #if not disableSignal:
	  #   region.getSample(sigName).addSystematic(Systematic('SignalAlternative', configMgr.weights, 1.05, 0.95, 'user', 'userOverallSys'))
    if 'rough' in mode:
        region.getSample('NonPromptEl').addSystematic(Systematic('FAKES_Electron', configMgr.weights, 1.15, 0.85, 'user', 'userHistoSys'))
        region.getSample('NonPromptMu').addSystematic(Systematic('FAKES_Muon', configMgr.weights, 1.10, 0.90, 'user', 'userHistoSys'))
        region.getSample('ChargeFlip').addSystematic(Systematic('ChargeFlipRate', configMgr.weights, 1.025, 0.975, 'user', 'userHistoSys'))
        region.getSample('PhotonConversion').addSystematic(Systematic('PhotonConversionRate', configMgr.weights, 1.09, 0.91, 'user', 'userHistoSys'))
        region.getSample('WZBoosted').addSystematic(Systematic('SYSWZ', configMgr.weights, 1.40, 0.60, 'user', 'userHistoSys'))
        region.getSample('WZResolved').addSystematic(Systematic('SYSWZ', configMgr.weights, 1.40, 0.60, 'user', 'userHistoSys'))
        region.getSample('ssWW').addSystematic(Systematic('SYSssWW', configMgr.weights, 1.40, 0.60, 'user', 'userHistoSys'))
        region.getSample('WWW').addSystematic(Systematic('SYSWWW', configMgr.weights, 1.30, 0.70, 'user', 'userHistoSys'))
        region.getSample('VVV').addSystematic(Systematic('SYSVVV', configMgr.weights, 1.20, 0.80, 'user', 'userHistoSys'))
        region.getSample('ZZ').addSystematic(Systematic('SYSZZ', configMgr.weights, 1.20, 0.80, 'user', 'userHistoSys'))
        region.getSample('TopX').addSystematic(Systematic('SYSTopX', configMgr.weights, 1.20, 0.80, 'user', 'userHistoSys'))
        if not disableSignal:
	     region.getSample(sigName).addSystematic(Systematic('SignalAlternative', configMgr.weights, 1.4, 0.60, 'user', 'userOverallSys'))

#######################################################
#if 'sys' in mode:
#   for region in has_largeRjets:
#	region.getSample('WWW').addSystematic(Systematic('WWWNormBoosted', configMgr.weights, 1.60, 0.40, 'user', 'userHistoSys'))
#   for region in has_smallRjets:
#	region.getSample('WWW').addSystematic(Systematic('WWWNormResolved', configMgr.weights, 1.60, 0.40, 'user', 'userHistoSys'))
#######################################################
CRs = []
CRs +=[region_3lBoostedCR_Inc, region_3lResolvedCR_Inc, region_CRssWW_Inc]
VRs = []
SRs = []
if "Boosted" in mode:
    SRs += [region_SS2lBoostedSR_Inc]
if "Resolved" in mode:
    SRs += [region_SS2lResolvedSR_Inc]
if "Inclusive" in mode:
    SRs +=[region_SS2lBoostedSR_Inc, region_SS2lResolvedSR_Inc]
    if "vars" in mode:
        VRs += [region_SS2lBoostedSR_Inc_Lep1Pt, region_SS2lBoostedSR_Inc_Lep2Pt, region_SS2lBoostedSR_Inc_fatjet1Pt, region_SS2lBoostedSR_Inc_MET, region_SS2lBoostedSR_Inc_mll, region_SS2lResolvedSR_Inc_Lep1Pt, region_SS2lResolvedSR_Inc_Lep2Pt, region_SS2lResolvedSR_Inc_jet1Pt, region_SS2lResolvedSR_Inc_jet2Pt, region_SS2lResolvedSR_Inc_MET, region_SS2lResolvedSR_Inc_mll]

if "bkg" in mode:
    #VRs = []
    if "Boosted" in mode:
        VRs += [region_SS2lBoostedVR_Inc]
    if "Resolved" in mode:
        VRs += [region_SS2lResolvedVR_Inc]
    if "Inclusive" in mode:
        VRs +=[region_SS2lBoostedVR_Inc, region_SS2lResolvedVR_Inc]


if not disableSignal:
    for r in CRs:
        r.removeSample(sigName)

#CRs:
fitConfig.addBkgConstrainChannels(CRs)
# VRs:
#if bkgOnly:
#    fitConfig.addValidationChannels(VRs)
fitConfig.addValidationChannels(VRs)
# SRs:
fitConfig.addSignalChannels(SRs)

